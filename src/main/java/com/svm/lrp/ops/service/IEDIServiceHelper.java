/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.svm.lrp.ops.service;

/**
 *
 * @author indhumathi.k
 */

import com.svm.json.SvmJsonList;
import com.svm.lrp.nfr.query.annotation.FP;
import com.svm.lrp.nfr.query.annotation.VDS;
import com.svm.lrp.nfr.query.bean.helper.Condition;
import com.svm.lrp.nfr.query.bean.helper.Operator;
import java.util.Map;

public abstract interface IEDIServiceHelper
{
  @VDS(name="getemsmovementdetail", tenant="svm")
//  public abstract EDIModel getemsmovementdetail(@FP(name="servicecode", condition=Condition.EQUALS, operator=Operator.AND) String paramString1, @FP(name="vessel", condition=Condition.EQUALS) String paramString2, @FP(name="voyage", condition=Condition.EQUALS) String paramString3, @FP(name="bound", condition=Condition.EQUALS) String paramString4, @FP(name="portcode", condition=Condition.EQUALS) String paramString5, @FP(name="depot", condition=Condition.EQUALS) String paramString6, @FP(name="mdlcode", condition=Condition.EQUALS) String paramString7, @FP(name="opsemsmovement.status", condition=Condition.EQUALS) String paramString8, @FP(name="emsmovementdetail.status", condition=Condition.EQUALS) String paramString9);
  public abstract EDIModel getemsmovementdetail(@FP(name="servicecode", condition=Condition.EQUALS, operator=Operator.AND) String paramString1, @FP(name="vessel", condition=Condition.EQUALS) String paramString2, @FP(name="voyage", condition=Condition.EQUALS) String paramString3, @FP(name="bound", condition=Condition.EQUALS) String paramString4, @FP(name="portcode", condition=Condition.EQUALS) String paramString5, @FP(name="depot", condition=Condition.EQUALS) String paramString6, @FP(name="mdlcode", condition=Condition.EQUALS) String paramString7, @FP(name="opsemsmovement.status", condition=Condition.EQUALS) String paramString8, @FP(name="emsmovementdetail.status", condition=Condition.EQUALS) String paramString9,@FP(name="opsemsmovement.movetype", condition=Condition.EQUALS) String paramString10);

  
    @VDS(name = "updatetshfctcntstatus", tenant = "svm")
    public abstract Map updatetshfctcntstatus(SvmJsonList paramSvmJsonList);

}
