package com.svm.lrp.ops.service;

import java.io.Serializable;

public class Emsmovementdetail
  implements Serializable
{

  private String containerno;

  private String crdate;

  private String documentrefno;

  private String sealnumber;
  
  private String activitydate;

  public String getContainerno()
  {
    return this.containerno;
  }

  public void setContainerno(String containerno) {
    this.containerno = containerno;
  }

  public String getCrdate() {
    return this.crdate;
  }

  public void setCrdate(String crdate) {
    this.crdate = crdate;
  }

  public String getDocumentrefno() {
    return this.documentrefno;
  }

  public void setDocumentrefno(String documentrefno) {
    this.documentrefno = documentrefno;
  }

  public String getSealnumber() {
    return this.sealnumber;
  }

  public void setSealnumber(String sealnumber) {
    this.sealnumber = sealnumber;
  }

    public String getActivitydate() {
        return activitydate;
    }

    public void setActivitydate(String activitydate) {
        this.activitydate = activitydate;
    }
  
  
}