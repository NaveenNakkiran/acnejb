package com.svm.lrp.ops.acnejb;

import com.svm.lrp.ops.acn.vo.AcnActualContainerVo;
import java.rmi.RemoteException;
import javax.ejb.EJBObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//
public abstract interface AcnsRemote {

    public String getErrorInfo() throws RemoteException;

    public String setErrorInfo(String Err) throws RemoteException;

    public Object getdetail(HashMap map, String dateformat) throws RemoteException;
//    public ActualContainerVo setLoadConfDetails(AcnActualContainerVo ldcl,AcnActualContainerVo ldl,String cmpCode,String user,String EDI,String status,String carrierCode)throws RemoteException;

    public String[][] SaveMethodEntrySrn(AcnActualContainerVo ldcl, String cmpCode, String user, String EDI, String status, String carrierCode, String Dat) throws RemoteException;

    public String[] checkForEmsUpdate(AcnActualContainerVo ldcl) throws RemoteException;

    public String getETA(String service, String vessel, String voyage, String Bound, String opspod, String opspodter, String gcid) throws RemoteException;

    public Object getContainerStatus(ArrayList cnal, ArrayList cntypeal, String port, ArrayList norflag, String plugin) throws RemoteException;

    public AcnActualContainerVo checkingMovementcode(AcnActualContainerVo lcvo) throws RemoteException;

    public Object setContainers(AcnActualContainerVo oldldvo, AcnActualContainerVo lcvo, String headers, String plugin, String Usercr, String stuffplugin, String updateforecast, String editrigger, String WCNplugin, String DGplugin) throws RemoteException;

    public Object getContainersFromEDI(String service, String vessel, String voyage, String Bound, String port, String terminal, String gcid, String lt) throws RemoteException;

    public Object validateVendorCodes(ArrayList vendorList, String agency, AcnActualContainerVo nvo, String type) throws RemoteException;

    public Object validateVendorCodesForTdn(ArrayList vendorList, String agency) throws RemoteException;

    public Object checkHaulageContractForNewVendor(AcnActualContainerVo nvo) throws RemoteException;

    public Object checkVendorInSchedules(AcnActualContainerVo nvo) throws RemoteException;

    public Object check_HlgExpRpt_Exists(AcnActualContainerVo nvo) throws RemoteException;

    public Object check_FdrExpRpt_Exists(AcnActualContainerVo nvo) throws RemoteException;

    public Object getContainerTypes(AcnActualContainerVo ctvo, String plugin) throws RemoteException;

    public Object getContainerWeight(String contno, String plugincwv) throws RemoteException;

    public Map getConfigurations(String agencycode) throws RemoteException;

    public String getConfiguration() throws RemoteException;

    public abstract List getRecords(String paramString);

    public Object updatemrns(ArrayList mrnbackupdate, String usercr) throws RemoteException;

}
