package com.svm.lrp.ops.acnejb;

import javax.ejb.CreateException;
import java.rmi.RemoteException;
import javax.ejb.EJBHome;

public interface AcnsHome extends EJBHome {

    AcnsRemote create() throws RemoteException, CreateException;
}
