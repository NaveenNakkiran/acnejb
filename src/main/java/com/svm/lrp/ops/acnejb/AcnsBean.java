/**
 * **************************************************************
 * Class : ActualContainer.java Description :
 *
 * Database Accessed : << LRPDB >> Tables Accessed : << operationdetail >>
 *
 * Author : Magesh Date : August 23, 2008, 03:40 PM
 *
 * Modification History : Author Date Reason Comment --------- ---------
 * -------- ---------
 *
 *****************************************************************
 */
package com.svm.lrp.ops.acnejb;

import com.svm.json.SvmJsonList;
import com.svm.lrp.nfr.jsf.utils.ILogger;
import com.svm.lrp.ops.acn.vo.AcnActualContainerVo;
import com.svm.lrp.ops.service.IEDIServiceHelper;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.svm.lrp.nfr.query.cbean.VDSInvoker;
import com.svm.lrp.ops.service.EDIModel;
import com.svm.lrp.ops.service.Emsmovementdetail;
import java.sql.ResultSetMetaData;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resources;
import javax.ejb.EJBException;
import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.ejb.Handle;
import javax.ejb.RemoveException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lrp.ops.service.proxy.OPSServiceProxy;
import lrp.serverutils.refs.ServerObject;
import solverminds.compress.CompressAPI;

@Stateless(name = "AcnsBean")
@Resources({
    @javax.annotation.Resource(name = "jdbc/ClientDB", mappedName = "java:/OPSDS")})
public class AcnsBean implements AcnsRemote {

    @Inject
    VDSInvoker vdsInvoker;
    IEDIServiceHelper serviceHelper;
    private javax.ejb.SessionContext context;
    private Connection conn;
    private PreparedStatement pst;
    private PreparedStatement pst1;
    private PreparedStatement pst2;
    private PreparedStatement pstwo;
    private PreparedStatement pstsave;
    private String sql = "";
    private String Err = "";
    private String pluginRRO = "";
    // private String pluginRRRO;
    private String appremarks = "";
    private String remarksappend = "";

    private ResultSet rs, rs1;
    int OperationNo;
    int RefNo;

    String usercredievent = "";

    javax.ejb.SessionContext sessionContext;

    @Inject
    ILogger logger;

    @PostConstruct
    public void init() {
        logger.info("Inside Actual Containers Init method-->");
        this.serviceHelper = ((IEDIServiceHelper) this.vdsInvoker.create(IEDIServiceHelper.class));
    }

    public AcnsBean() {

    }

    public String getConfiguration() {
        // ResultSet rs = null;
        Connection con = null;
        //  PreparedStatement pst = null;

        try {

            con = new lrp.serverutils.refs.ServerObject().getConnection();

            String sql1 = "select enablestatus from pluginconfiguration where moduleid='LDP' and pluginid='RRO' ";
            this.pst = con.prepareStatement(sql1);
            this.rs = this.pst.executeQuery();
            while (this.rs.next()) {
                pluginRRO = this.rs.getString("enableStatus");
            }
        } catch (Exception e) {
            logger.fatal(e);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e1) {
                    logger.info("--### Exception in tdnterm dispute showDetails finally blcok ###--");
                    logger.fatal(e1);
                }
            }
        }
        return pluginRRO;
    }

    public Object getdetail(HashMap map, String dateformat) {
        Object object = null;
        ArrayList list = new ArrayList();
        Connection con = null;
        try {

            con = new ServerObject().getConnection();
//            logger.info(con + "con2-->");
            SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat target = new SimpleDateFormat(dateformat);
            String SelectedFlag = map.get("SelectedFlag").toString();
            String ser = "";
            String ves = "";
            String voy = "";
            String bnd = "";
            String pod = "";
            String ter = "";
            String lt = "";
            String gcid = "";
            String bookno = "";
            String fromDate = "";
            String toDate = "";
            String agency = "";
            String includeWO = "N";
            String tmp = "";
            int ventSetting = 0;
            String ventilation = "";
            if (SelectedFlag.equalsIgnoreCase("B")) {
                bookno = map.get("BookNo").toString();
                agency = map.get("agency").toString();
            } else if ((SelectedFlag.equalsIgnoreCase("TDN-WO")) || (SelectedFlag.equalsIgnoreCase("TDN-Book"))) {
                bookno = map.get("BookNo").toString();
                agency = map.get("agency").toString();
            } else if (SelectedFlag.equalsIgnoreCase("TDN-Date")) {
                fromDate = map.get("FromDate").toString();
                toDate = map.get("ToDate").toString();
                agency = map.get("agency").toString();
                includeWO = map.get("includeWO").toString();
            } else {
                ser = map.get("service").toString();
                ves = map.get("vessel").toString();
                voy = map.get("voyage").toString();
                bnd = map.get("Bound").toString();
                pod = map.get("pod").toString();
                ter = map.get("ter").toString();
                lt = map.get("lt").toString();
                gcid = map.get("gcid").toString();
                agency = map.get("agency").toString();
                System.out.println("agency>>>>>>>>>>>>>" + agency);
            }
            ArrayList opsObjInputs2 = new ArrayList();
            Object[] opsObjInputs1 = new Object[1];
//            try {
//                String Sql = "select bl.bl_no from  tdn tn(nolock) inner join TDNDetail td(nolock) on  td.TDNNumber=tn.TDNNumber  inner join  bl(nolock) on bl.bl_no = td.bookno  where  bl.status<>'D' ";
//                logger.info("#--# For Migretion Change Sql==" + Sql);
//                this.pst = con.prepareStatement(Sql);
//                this.rs = this.pst.executeQuery();
//                while (this.rs.next()) {
//                    opsObjInputs2.add(this.rs.getString("bl_no"));
//                }
//                if (opsObjInputs2 != null) {
//                    opsObjInputs1[0] = opsObjInputs2;
//
//                    OPSServiceProxy opsPxy = new OPSServiceProxy();
//                    String keyName = "getdetail_ACC_ActualCont";
//                    Object objResult = opsPxy.getServiceDetails("Actual Containers", "getdetail_ACC_ActualCont", keyName, opsObjInputs1);
//                    Object opsObjOutputFinal = objResult;
//                    if (opsObjOutputFinal != null) {
//                        ArrayList alOutPutData = new ArrayList();
//                        alOutPutData = (ArrayList) opsObjOutputFinal;
//                        String invrefno = "";
//                        Sql = "Insert into temp_invoicemaster_ACTCont(invrefno) values(?)";
//                        this.pst = con.prepareStatement(Sql);
//                        con.setAutoCommit(false);
//                        for (int i = 0; i < alOutPutData.size(); i++) {
//                            invrefno = (String) alOutPutData.get(i);
//                            this.pst.setString(1, invrefno);
//                            this.pst.addBatch();
//                        }
//                        this.pst.executeBatch();
//                        con.setAutoCommit(true);
//                    }
//                }
//            } catch (Exception e) {
//                logger.info("Exception in Operation Module");
//                logger.fatal(e);
//            } finally {
//                if (con != null) {
//                    try {
//                        con.close();
//                    } catch (Exception e) {
//                        setErrorInfo(e.getMessage());
//                    }
//                }
//            }

//      
//            con = new ServerObject().getConnection();
            logger.info("EXEC usp_ActualContainers '" + SelectedFlag + "','" + bookno + "','" + ser + "','" + ves + "','" + voy + "','" + bnd + "','" + pod + "','" + ter + "','" + lt + "','" + gcid + "','" + fromDate + "','" + toDate + "','" + agency + "','" + includeWO + "'");
            String Sql = " EXEC usp_ActualContainers ?,?,?,?,?,?,?,?,?,?,?,?,?,?";
            this.pst = con.prepareStatement(Sql);
            this.pst.setString(1, SelectedFlag);
            this.pst.setString(2, bookno);
            this.pst.setString(3, ser);
            this.pst.setString(4, ves);
            this.pst.setString(5, voy);
            this.pst.setString(6, bnd);
            this.pst.setString(7, pod);
            this.pst.setString(8, ter);
            this.pst.setString(9, gcid);
            this.pst.setString(10, lt);
            this.pst.setString(11, fromDate);
            this.pst.setString(12, toDate);
            this.pst.setString(13, agency);
            this.pst.setString(14, includeWO);
            this.rs = this.pst.executeQuery();
            while (this.rs.next()) {
                AcnActualContainerVo lcvo = new AcnActualContainerVo();
                lcvo.soperator = this.rs.getString("Operator");
                lcvo.sacontno = this.rs.getString("ContainerNo").trim();
                lcvo.stcontno = "";
                lcvo.stopspod = "";
                lcvo.sactiveDate = "";
                lcvo.sactEqpType = this.rs.getString("EquipmentType");
                lcvo.stype = lcvo.sactEqpType;
                lcvo.smt = this.rs.getString("containerStatus");
                lcvo.sbkgno = this.rs.getString("bookNo");
                lcvo.scargotype = this.rs.getString("CargoType");
                lcvo.sorgin = this.rs.getString("originPort");
                lcvo.sblpol = this.rs.getString("blpol");
                lcvo.sblpolter = this.rs.getString("blpolTerminal");
                lcvo.sblpod = this.rs.getString("blpod");
                lcvo.sblpodter = this.rs.getString("blpodTerminal");
                lcvo.sdelivery = this.rs.getString("deliveryPort");
                lcvo.sopspol = this.rs.getString("opspol");
                lcvo.sopspolter = this.rs.getString("opspolTerminal");
                lcvo.sopspod = this.rs.getString("opspod");
                lcvo.sopspodter = this.rs.getString("opspodTerminal");
                lcvo.soptopspod = this.rs.getString("optopspod");
                lcvo.soptopspodter = this.rs.getString("optopspodTerminal");
                lcvo.svsl = this.rs.getString("VesselCode");
                lcvo.svoy = this.rs.getString("Voyage");
                lcvo.ssvc = this.rs.getString("ServiceCode");
                lcvo.sbnd = this.rs.getString("Bound");
                lcvo.stmp = this.rs.getString("Temperature");
                lcvo.soog = this.rs.getString("OOG");
                lcvo.snor = this.rs.getString("nor");
                lcvo.sremarks = this.rs.getString("Remarks");
                lcvo.soperationNum = this.rs.getString("OperationDNo");
                lcvo.sprevOperationno = this.rs.getString("prevOperation");
                lcvo.ssoc = this.rs.getString("soc");
                lcvo.spriority = this.rs.getString("priority");
                lcvo.sseal = this.rs.getString("sealno");
                lcvo.shipseal = this.rs.getString("shippersealno");
                lcvo.sotherseal = this.rs.getString("othersealno");
                lcvo.stareWt = this.rs.getString("tareWeight");
                lcvo.scargoWt = this.rs.getString("cargoWeight");
                lcvo.swt = this.rs.getString("grossWeight");
                lcvo.sweightunit = NullCheck(this.rs.getString("WeightUnit"));
                lcvo.sdgglass = this.rs.getString("DGClass");
                lcvo.sdgapproval = this.rs.getString("DGApproval");
                lcvo.stotalOOG = this.rs.getString("carriageType");
                lcvo.sccont = this.rs.getString("ccont");
                lcvo.sreftype = this.rs.getString("Reftype");
                lcvo.sisocode = this.rs.getString("isocode");
                lcvo.sloadingcondition = this.rs.getString("loadingcondition");
                lcvo.sstowagePosition = this.rs.getString("StowagePosition");
                lcvo.sblnumber = this.rs.getString("BLNumber");
                lcvo.sblType = this.rs.getString("BLType");
                lcvo.snetWeight = this.rs.getString("NetWeight");
                lcvo.sactStatus = this.rs.getString("ActStatus");
                lcvo.srowID = this.rs.getString("RowID");
                lcvo.svendor = NullCheck(this.rs.getString("Vendor"));
                lcvo.shaulagetype = NullCheck(this.rs.getString("HaulageType"));
                lcvo.sitn = NullCheck(this.rs.getString("ITN"));
                lcvo.sgateClearence = NullCheck(this.rs.getString("GateClearance"));
                lcvo.sgateDocDate = this.rs.getString("GateDocDate");
                if ((lcvo.sgateDocDate != null) && (lcvo.sgateDocDate.trim().length() != 0)) {
                    try {
                        lcvo.sgateDocDate = target.format(source.parse(lcvo.sgateDocDate));
                    } catch (Exception e) {
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                    }
                } else {
                    lcvo.sgateDocDate = NullCheck(lcvo.sgateDocDate);
                }
                lcvo.sgateInClearenceDate = this.rs.getString("GateInClearanceDate");
                if ((lcvo.sgateInClearenceDate != null) && (lcvo.sgateInClearenceDate.trim().length() != 0)) {
                    try {
                        lcvo.sgateInClearenceDate = target.format(source.parse(lcvo.sgateInClearenceDate));
                    } catch (Exception e) {
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                    }
                } else {
                    lcvo.sgateInClearenceDate = NullCheck(lcvo.sgateInClearenceDate);
                }
                ventSetting = this.rs.getInt("VentSetting");
                ventilation = this.rs.getString("VentilationPercentage");
                ventilation = ventilation == null ? "" : ventilation.toString().trim();
                if (ventSetting == 1) {
                    if ((ventilation.equalsIgnoreCase("")) || (ventilation.equalsIgnoreCase("0"))) {
                        lcvo.sventpercent = "VENT: Yes";
                    } else {
                        lcvo.sventpercent = ("VENT: Yes, " + ventilation + "(CBM)");
                    }
                } else {
                    lcvo.sventpercent = "VENT: No";
                }
                lcvo.shumidity = this.rs.getString("humidity");
                lcvo.sdrains = this.rs.getString("drains");
                lcvo.stdnnumber = this.rs.getString("TDNNumber");
                lcvo.sbookningETD = this.rs.getString("BookingETD");
                if ((lcvo.sbookningETD != null) && (lcvo.sbookningETD.trim().length() != 0)) {
                    try {
                        lcvo.sbookningETD = target.format(source.parse(lcvo.sbookningETD));
                    } catch (Exception e) {
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                    }
                } else {
                    lcvo.sbookningETD = NullCheck(lcvo.sbookningETD);
                }
                lcvo.sgateReferenceRemarks = NullCheck(this.rs.getString("GateReferenceRemarks"));
                lcvo.sapcode = NullCheck(this.rs.getString("APCode"));
                lcvo.sapname = NullCheck(this.rs.getString("APName"));
                lcvo.sblstatus = NullCheck(this.rs.getString("BLStatus"));
                lcvo.smodeOfTransport = NullCheck(this.rs.getString("GateClearanceMode"));
                lcvo.smanifestedWeight = this.rs.getString("ManifestWeight");
//                logger.info("ActualContainer Over weight Flag -" + this.rs.getString("OverWtFlag"));
                lcvo.soverWtFlag = this.rs.getString("OverWtFlag");
                lcvo.smaxGrossWt = this.rs.getString("MaxGrossWt");
                lcvo.sisDummy = this.rs.getString("isDummy");

                lcvo.sTDNRefNo = rs.getString("TDNRefNo");
                lcvo.sMultipleVendor = rs.getString("MultipleVendor");
                lcvo.sInvStatus = rs.getString("InvStatus");
                lcvo.sTdnCallid = rs.getString("TDNCallID");
                lcvo.stdnbkgno = rs.getString("TDNBookNo");
                lcvo.sNextActivity = rs.getString("NextActivity");
                lcvo.sArrDate = rs.getString("ArrDate");
                lcvo.sOpsMode = rs.getString("Mode");
                lcvo.sOpsStatus = rs.getString("OpsStatus");
                lcvo.sVGMVeriFlag = rs.getString("VGM_Source");
                lcvo.sVGMRefNumber = rs.getString("VGM_Ref_Number");
                lcvo.sVGMOrderNumber = rs.getString("VGM_order_Number");
                lcvo.sVGMObtDate = rs.getString("VGM_Obtained_Date");
                if ((lcvo.sVGMObtDate != null) && (lcvo.sVGMObtDate.trim().length() != 0)) {
                    try {
                        lcvo.sVGMObtDate = target.format(source.parse(lcvo.sVGMObtDate));
                    } catch (Exception e) {
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                    }
                } else {
                    lcvo.sVGMObtDate = NullCheck(lcvo.sVGMObtDate);
                }
                lcvo.sVGMWeight = rs.getString("VGM_Weight");
                lcvo.sVGMMeasurement = rs.getString("VGM_unit");
                lcvo.sVGMObtainedMet = rs.getString("VGM_Obtained_Method");
                lcvo.sCompanyName = rs.getString("VGM_Company_Name");
                lcvo.sNameofAuthorized = rs.getString("VGM_Authorized_Person");
                lcvo.sEDIreference = rs.getString("EDI_reference");

                lcvo.sAddress1 = rs.getString("VGM_Address1");
                lcvo.sAddress2 = rs.getString("VGM_Address2");
                lcvo.sZipCode = rs.getString("VGM_Zipcode");
                lcvo.sCity = rs.getString("VGM_City");
                lcvo.sCountry = rs.getString("VGM_Country");
                lcvo.sPhone = rs.getString("Telephone");
                lcvo.sFax = rs.getString("Fax");
                lcvo.sEmail = rs.getString("Email");
                if ("N".equalsIgnoreCase(lcvo.sccont)) {
                    lcvo.stcontno = lcvo.sacontno;
                }
                lcvo.equipmentstype = rs.getString("eqtype");
                lcvo.sOverWtApprovalFlag = rs.getString("OverWtApprovalFlag");
                lcvo.svia = rs.getString("containerupdatedvia");
                lcvo.mrnnumber = rs.getString("mrnnumber");
                list.add(lcvo);
            }
            object = list;

            String sql = "Delete from temp_invoicemaster_ACTCont";
            this.pst = con.prepareStatement(sql);
            this.pst.execute();
            logger.info("ActualContainer List Size -" + list.size());
            logger.info("---> End of getDetail method in ActualContainer");
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            if (con != null) {
                try {
                    con.close();
                    this.pst.close();
                } catch (Exception e) {
                    setErrorInfo(e.getMessage());
                }
            }
        }
//        try {
//            logger.info(con.isClosed() + "con5-->");
//        } catch (SQLException ex) {
//            logger.info("isClosed exception");
//        }
        if (list.size() > 0) {
            return new CompressAPI().compress(object);
        }
        return "No Records";
    }

    public Object getContainersFromEDI(String ser, String ves, String voy, String bnd, String port, String terminal, String gcid, String lt) {
        Object object = null;
        Connection con = null;
        try {
            Map ht = new HashMap();
            logger.info("---> Start of getContainersFromEDI");
            String allData[] = null;
            String AllDetails = "";
            Object[] opsObjOutput = null;
            Object[] opsObjInputs = new Object[8];
            opsObjInputs[0] = ser;
            opsObjInputs[1] = ves;
            opsObjInputs[2] = voy;
            opsObjInputs[3] = bnd;
            opsObjInputs[4] = port;
            opsObjInputs[5] = terminal;
            opsObjInputs[6] = gcid;
            opsObjInputs[7] = "LC";

            try {
                this.serviceHelper = ((IEDIServiceHelper) this.vdsInvoker.create(IEDIServiceHelper.class));
                logger.info("ser >> " + ser + " >> ves >> " + ves + " >> voy >> " + voy + " >> voy >> " + bnd + " >> voy >> " + port + " >> terminal >> " + terminal + "Service Helper >>>" + this.serviceHelper);
                EDIModel ems = this.serviceHelper.getemsmovementdetail(ser.trim(), ves.trim(), voy.trim(), bnd.trim(), port.trim(), terminal.trim(), "LC", "A", "A", "Search");

                if (ems.getEmsmovementdetail() != null) {
                    List al = ems.getEmsmovementdetail();
                    con = new lrp.serverutils.refs.ServerObject().getConnection();
                    String Sql = "insert into temp_Containers1_usp_getOpsEmsContainers(crdate,Bookno,Containerno,service,"
                            + "vessel,voyage,bound,SealNo,activitydate) values(cast(? as datetime),?,?,?,?,?,?,?,?)";
                    pst = con.prepareStatement(Sql, 1004, 1007);
                    for (Iterator localIterator = al.iterator(); localIterator.hasNext();) {
                        Object al1 = localIterator.next();
                        Emsmovementdetail em = (Emsmovementdetail) al1;
                        pst.setString(1, em.getCrdate());
                        pst.setString(2, em.getDocumentrefno());
                        logger.info(new StringBuilder().append("em.getDocumentrefno() >> ").append(em.getDocumentrefno()).append(" >> em.getContainerno() >> ").append(em.getContainerno()).toString());
                        pst.setString(3, em.getContainerno());
                        pst.setString(4, ser);
                        pst.setString(5, ves);
                        pst.setString(6, voy);
                        pst.setString(7, bnd);
                        pst.setString(8, em.getSealnumber() != null ? em.getSealnumber() : "");
                        pst.setString(9, em.getActivitydate());
                        pst.addBatch();
                    }
                    pst.executeBatch();

                    logger.info(" //**// Method - 1 done in Bean side Operation Module- - - - - - - - - @@@@");
                }
            } catch (Exception e) {
                logger.info(" //**// Method - 1 Exception Actual Contaienrs- - - - - - - - - @@@@" + e);
            }
            con = new ServerObject().getConnection();

            Object[] temp_Containers1 = null;
            Object[] temp_Containers2 = null;
            Object[] temp_Containers3 = null;
            Object[] temp_ContainersFinal = null;

            String Sql = " EXEC usp_getOpsEmsContainers ?,?,?,?,?,?,?,?,?";
            pst = con.prepareStatement(Sql, 1004, 1007);
            pst.setString(1, "LC");
            pst.setString(2, ser);
            pst.setString(3, ves);
            pst.setString(4, voy);
            pst.setString(5, bnd);
            pst.setString(6, port);
            pst.setString(7, terminal);
            pst.setString(8, gcid);
            pst.setString(9, lt);
            rs = pst.executeQuery();
            rs.last();
            int Rc = rs.getRow();
            AcnActualContainerVo emsvo = new AcnActualContainerVo(Rc);
            logger.info("EMS Containers count -->" + Rc);
            if (rs.getRow() > 0) {
                rs.beforeFirst();
                int i = 0;
                while (rs.next()) {
                    emsvo.bkgno[i] = rs.getString("bookNo");
                    emsvo.contno[i] = rs.getString("ContainerNo").trim();
                    emsvo.type[i] = rs.getString("EquipmentType");
                    i++;
                }
            }
            rs = null;
            AcnActualContainerVo emsvo2 = null;
            if (pst.getMoreResults()) {
                rs = pst.getResultSet();
                rs.last();
                Rc = rs.getRow();
                emsvo2 = new AcnActualContainerVo(Rc);
                logger.info("Activity Not created count -->" + Rc);
                if (rs.getRow() > 0) {
                    rs.beforeFirst();
                    int i = 0;
                    while (rs.next()) {
                        emsvo2.activityFlag[i] = rs.getString("movement");
                        i++;
                    }
                }
            }
            ht.put("emscontainer", emsvo);
            ht.put("activitynotinmaster", emsvo2);
            object = ht;
            logger.info("---> End of getContainersFromEDI ");
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            if (con != null) {
                try {
                    con.close();
                    pst.close();
                } catch (Exception e) {
                    setErrorInfo(e.getMessage());
                }
            }
        }
        return object;
    }

    public Object validateVendorCodes(ArrayList vendorList, String agency, AcnActualContainerVo nvo, String type) {
        /* CR 675 */

        Object obj = null;
        HashMap hm = new HashMap();
        Connection con = null;
        try {
            logger.info("---> Start of  validateVendorCodes");
            ArrayList VendorList = new ArrayList();
            con = new lrp.serverutils.refs.ServerObject().getConnection();
            String sql = "Select Count(Vendor_Code) As Count from VendorDetails(NoLock) Where Status='A' And Vendor_Code = ? ";
            pst = con.prepareStatement(sql);
            int rscnt = 0;
            String vendor_code = "";
            for (int i = 0; i < vendorList.size(); i++) {
                vendor_code = (String) vendorList.get(i);
                vendor_code = (vendor_code != null) ? (vendor_code.trim()) : "";
                pst.setString(1, vendor_code);
                rs = pst.executeQuery();
                while (rs.next()) {
                    rscnt = rs.getInt("Count");
                    if (rscnt == 0) {
                        VendorList.add(vendor_code);

                    }
                }
            }
            for (int i = 0; i < VendorList.size(); i++) {
                vendorList.remove(VendorList.get(i));
            }

            for (int i = 0; i < nvo.contno.length; i++) {
                logger.info("-Act container-" + nvo.getVendor() + "-" + nvo.bkgno[i] + "-" + nvo.oldcontno[i]);
            }

            logger.info("VendorMap size-->" + VendorList.size());
            hm.put("VC", VendorList);
            if (type.equalsIgnoreCase("TDN")) {
                hm.put("VCT", validateVendorCodesForTdn(vendorList, agency));
                hm.put("HlgE", check_HlgExpRpt_Exists(nvo));
                hm.put("HlgCon", checkHaulageContractForNewVendor(nvo));
            } else if (type.equalsIgnoreCase("All")) {
                hm.put("HlgE", check_HlgExpRpt_Exists(nvo));
                hm.put("FdrE", check_FdrExpRpt_Exists(nvo));
                hm.put("VenInSch", checkVendorInSchedules(nvo));
            } else {
                hm.put("FdrE", check_FdrExpRpt_Exists(nvo));
                hm.put("VenInSch", checkVendorInSchedules(nvo));
            }

            //      obj = (Object) VendorList;
            obj = (Object) hm;
            logger.info("--> End of validateVendorCodes");
        } catch (Exception E) {
            logger.info("Exception in validateVendorCodes-->" + E);
            E.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public Object validateVendorCodesForTdn(ArrayList vendorList, String agency) {
        /* CR 675 */

        Object obj = null;
        Connection con1 = null;
        try {
            logger.info("---> Start of  validateVendorCodesForTdn");
            ArrayList VendorList = new ArrayList();
            con1 = new lrp.serverutils.refs.ServerObject().getConnection();
            String sql = "Select Count(Vendor_Code) As Count from VendorDetails vnd(NoLock) "
                    + " Inner join country ctrn(nolock) on ctrn.countrycode=vnd.country "
                    + " Inner Join Agency ag on ag.countrycode = vnd.country "
                    + " LEFT Join croMappedContacts cro(nolock) ON ag.agencycode =cro.mappedAgency and cro.status='A' "
                    + " Where (cro.agencycode='" + agency + "' or ag.agencycode='" + agency + "') and vnd.status='A' "
                    + " and ctrn.status='A' and ag.status='A' and vnd.typeOfContract like '%HCN%'  And vnd.Vendor_Code = ? ";

            pst = con1.prepareStatement(sql);
            int rscnt = 0;
            String vendor_code = "";
            for (int i = 0; i < vendorList.size(); i++) {
                vendor_code = (String) vendorList.get(i);
                vendor_code = (vendor_code != null) ? (vendor_code.trim()) : "";
                pst.setString(1, vendor_code);
                rs = pst.executeQuery();
                while (rs.next()) {
                    rscnt = rs.getInt("Count");
                    if (rscnt == 0) {
                        VendorList.add(vendor_code);
                    }
                }
            }
            logger.info("VendorMap size-->" + VendorList.size());
            obj = (Object) VendorList;
            logger.info("--> End of validateVendorCodesForTdn");
        } catch (Exception E) {
            logger.info("Exception in validateVendorCodesForTdn-->" + E);
            E.printStackTrace();
        } finally {
            try {
                if (con1 != null) {
                    con1.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public Object checkHaulageContractForNewVendor(AcnActualContainerVo nvo) {
        /* CR 675 */

        Object obj = null;
        Connection con = null;
        try {
            logger.info("---> Start of  checkHaulageContractForNewVendor");
            String user = "opsac.system";
            ArrayList resList = new ArrayList();
            con = new lrp.serverutils.refs.ServerObject().getConnection();
            String insQry = " Insert Into TDNLegCharge (userid,BookNo,ContainerNo,TDNRef,Eqptype,EqpStatus,"
                    + " Mode,MoveType,ItemNo,LoadType,LoadPort,Loadterminal,DischType,DischPort,DischTerminal,RtnType,"
                    + " RtnPort,Rtnterminal,PickupDate,ReturnDate,TareWt,CargoWt,GrossWt,LoadMapping,DischMapping,"
                    + " RtnMapping,Flatbed,FlatbedCallID,FlatbedItemNo,dateup,userup,Vendorcode)"
                    + " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),?,?) ";
            PreparedStatement pstIns = con.prepareStatement(insQry);

            String sql = " Select td.BookNo,td.ContainerNo,tl.TDNRefNo,td.Eqptype,tl.chargestatus,tl.Mode,tl.TripType, "
                    + " tl.ItemNo,tl.PickUpType,tl.PickUpPort,tl.PickUpLoc,tl.CustType,tl.CustPort,tl.CustLoc,tl.RtnType, "
                    + " tl.RtnPort,tl.RtnLoc,tl.PickupDate,tl.RtnDate,td.TareWt,td.CargoWt,td.GrossWt,tdn.dateup,tdn.userup, "
                    + " tl.PickUpPort,tl.CustPort,tl.RtnPort,tl.Flatbed,tl.FlatbedCallID,tl.FlatbedItemNo,tdn.TDNType "
                    + " From TDNLeg tl(NoLock) "
                    + " Inner Join TDNDetail td(NoLock) On td.Reference = tl.Reference "
                    + " Inner Join TDN tdn(NoLock) On tdn.TDNNumber = td.TDNNumber "
                    + " Where td.ContainerNo = ? And td.BookNo = ? And tl.TDNRefNo = ? ";
            pst = con.prepareStatement(sql);

            java.util.Random Rand = new java.util.Random();
            int Randomno = Rand.nextInt(10000000) + 10;
            logger.info("Random No : " + Randomno);
            String cntr_no = "", bkg_no = "", ref_no = "";
            for (int i = 0; i < nvo.contno.length; i++) {
                cntr_no = nvo.oldcontno[i];
                //   bkg_no = nvo.bkgno[i];
                bkg_no = nvo.tdnbkgno[i];
                ref_no = nvo.tdnRefNo[i];
                logger.info("Actual cont Vendor updation " + cntr_no + "//" + bkg_no + "//" + ref_no);
                pst.setString(1, cntr_no);
                pst.setString(2, bkg_no);
                pst.setString(3, ref_no);
                rs = pst.executeQuery();
                while (rs.next()) {
                    pstIns.setInt(1, Randomno);
                    pstIns.setString(2, rs.getString("BookNo"));
                    pstIns.setString(3, rs.getString("ContainerNo"));
                    pstIns.setString(4, rs.getString("TDNRefNo"));
                    pstIns.setString(5, rs.getString("Eqptype"));
                    pstIns.setString(6, rs.getString("chargestatus"));
                    pstIns.setString(7, rs.getString("Mode"));
                    pstIns.setString(8, rs.getString("TripType"));
                    pstIns.setString(9, rs.getString("ItemNo"));

                    if (rs.getString("TripType").equalsIgnoreCase("P")) {
                        pstIns.setString(10, rs.getString("CustType")); // LoadType
                        pstIns.setString(11, rs.getString("CustPort"));
                        pstIns.setString(12, rs.getString("CustLoc"));
                        pstIns.setString(13, rs.getString("RtnType")); // Disch
                        pstIns.setString(14, rs.getString("RtnPort"));
                        pstIns.setString(15, rs.getString("RtnLoc"));
                        pstIns.setString(16, ""); // Rtn
                        pstIns.setString(17, "");
                        pstIns.setString(18, "");

                        pstIns.setString(24, rs.getString("CustPort"));
                        pstIns.setString(25, rs.getString("RtnPort"));
                        pstIns.setString(26, "");

                    } else {

                        pstIns.setString(10, rs.getString("PickUpType")); // Load
                        pstIns.setString(11, rs.getString("PickUpPort"));
                        pstIns.setString(12, rs.getString("PickUpLoc"));
                        pstIns.setString(13, rs.getString("CustType")); // Disch
                        pstIns.setString(14, rs.getString("CustPort"));
                        pstIns.setString(15, rs.getString("CustLoc"));
                        pstIns.setString(16, rs.getString("RtnType")); // Rtn
                        pstIns.setString(17, rs.getString("RtnPort"));
                        pstIns.setString(18, rs.getString("RtnLoc"));

                        pstIns.setString(24, rs.getString("PickUpPort"));
                        pstIns.setString(25, rs.getString("CustPort"));
                        pstIns.setString(26, rs.getString("RtnPort"));
                    }
                    pstIns.setString(19, rs.getString("PickupDate"));
                    pstIns.setString(20, rs.getString("RtnDate"));
                    pstIns.setString(21, rs.getString("TareWt"));
                    pstIns.setString(22, rs.getString("CargoWt"));
                    pstIns.setString(23, rs.getString("GrossWt"));

                    pstIns.setString(27, rs.getString("Flatbed"));
                    pstIns.setString(28, rs.getString("FlatbedCallID"));
                    pstIns.setString(29, rs.getString("FlatbedItemNo"));
                    pstIns.setString(30, user);
                    pstIns.setString(31, nvo.vendor[i]);
                    pstIns.addBatch();
                }
            }
            int insCount[] = pstIns.executeBatch();
            logger.info("Insert Record(s) Count : " + insCount.length);

            if (insCount.length > 0) {
                AcnActualContainerVo svo = new AcnActualContainerVo();
                ResultSet rsContract = null;
                String sConAvail = "";
                con = new lrp.serverutils.refs.ServerObject("REPORTDS").getConnection();
                sql = " EXEC usp_TDNLegChargeForNewVendor ? ";
                PreparedStatement pstChkContract = con.prepareStatement(sql);
                pstChkContract.setInt(1, Randomno);
                rsContract = pstChkContract.executeQuery();
                while (rsContract.next()) {
                    sConAvail = rsContract.getString("Availability");
                    if (sConAvail.equalsIgnoreCase("N")) {
                        svo = new AcnActualContainerVo();
                        svo.sacontno = rsContract.getString("ContainerNo");
                        svo.sbkgno = rsContract.getString("BookNo");
                        svo.sTDNRefNo = rsContract.getString("TDNRef");
                        svo.svendor = rsContract.getString("Vendor");
                        // logger.info(svo.Sacontno+"//"+svo.Sbkgno+"//"+svo.STDNRefNo+"//"+svo.SVendor);
                        resList.add(svo);
                    }
                }
            }
            logger.info("Contract List size-->" + resList.size());
            obj = (Object) resList;
            logger.info("--> End of checkHaulageContractForNewVendor");
        } catch (Exception E) {
            logger.info("Exception in checkHaulageContractForNewVendor-->" + E);
            E.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public Object checkVendorInSchedules(AcnActualContainerVo nvo) {
        /* CR 675 Validations */

        Object obj = null;
        Connection con = null;
        try {
            logger.info("---> Start of checkVendorInSchedules");
            ArrayList vNAList = new ArrayList(), conNAList = new ArrayList();
            AcnActualContainerVo svo = new AcnActualContainerVo();
            AcnActualContainerVo ncvo = new AcnActualContainerVo();
            con = new lrp.serverutils.refs.ServerObject().getConnection();
            String qry = " Select Count(Vendor_Code) As Count From FeederScheduleVendors(NoLock) "
                    + " Where Service=? "
                    + " And Vessel=? "
                    + " And Voyage=? "
                    + " And Bound=? "
                    + " And Vendor_Code=? ";
            PreparedStatement pst = con.prepareStatement(qry);

            qry = " Select Count(ContractNo) As Count from fdrcontractdetail fd(NoLock) "
                    + " Where service=? "
                    + " And usetype <>'D'  "
                    + " And status ='A' "
                    + " And approve='A' "
                    + " And vendorcode=? "
                    + " And loadPort=? "
                    + " And loadTerminal=? "
                    + " And dischport=? "
                    + " And dischterminal=? "
                    + " And termselect='Y' "
                    + " And convert(varchar(10),?,120) >= convert(varchar(10),validfrom,120) "
                    + " And convert(varchar(10),?,120) <= convert(varchar(10),validto,120) ";
            PreparedStatement pstFdrConChk = con.prepareStatement(qry);
            ResultSet rsFdrConChk = null;

            int vexists = -1, cexists = -1;
            logger.info("nvo.contno.length : " + nvo.contno.length);
            for (int i = 0; i < nvo.contno.length; i++) {
                logger.info("nvo.SOpsStatus : " + nvo.aOpsSts[i] + " // nvo.AOpsMode : " + nvo.aOpsMode[i]);
                if (nvo.aOpsSts[i].equalsIgnoreCase("Load Planned") && nvo.aOpsMode[i].equalsIgnoreCase("F")) {
                    logger.info(nvo.svc[i] + "//" + nvo.vsl[i] + "//" + nvo.voy[i] + "//" + nvo.bnd[i] + "//" + nvo.vendor[i]);
                    pst.setString(1, nvo.svc[i]);
                    pst.setString(2, nvo.vsl[i]);
                    pst.setString(3, nvo.voy[i]);
                    pst.setString(4, nvo.bnd[i]);
                    pst.setString(5, nvo.vendor[i]);
                    rs = pst.executeQuery();
                    if (rs.next()) {
                        vexists = rs.getInt("Count");
                        logger.info("vexists : " + vexists);
                        if (vexists == 0) {
                            svo = new AcnActualContainerVo();
                            svo.sbkgno = nvo.bkgno[i];
                            svo.sacontno = nvo.oldcontno[i];
                            svo.service = nvo.svc[i];
                            svo.vessel = nvo.vsl[i];
                            svo.voyage = nvo.voy[i];
                            svo.bound = nvo.bnd[i];
                            svo.svendor = nvo.vendor[i];
                            vNAList.add(svo);
                            logger.info("svo.SVendor : " + svo.svendor);
                        } else if (vexists > 0) {
                            logger.info(nvo.svc[i] + "//" + nvo.vendor[i] + "//" + nvo.opspol[i] + "//" + nvo.opspolter[i] + "//" + nvo.opspod[i] + "//" + nvo.opspodter[i] + "//" + nvo.arrDate[i]);
                            pstFdrConChk.setString(1, nvo.svc[i]);
                            pstFdrConChk.setString(2, nvo.vendor[i]);
                            pstFdrConChk.setString(3, nvo.opspol[i]);
                            pstFdrConChk.setString(4, nvo.opspolter[i]);
                            pstFdrConChk.setString(5, nvo.opspod[i]);
                            pstFdrConChk.setString(6, nvo.opspodter[i]);
                            pstFdrConChk.setString(7, nvo.arrDate[i]);
                            pstFdrConChk.setString(8, nvo.arrDate[i]);
                            rsFdrConChk = pstFdrConChk.executeQuery();
                            if (rsFdrConChk.next()) {
                                cexists = rsFdrConChk.getInt("Count");
                                logger.info("cexists : " + cexists);
                                if (cexists == 0) {
                                    ncvo = new AcnActualContainerVo();
                                    ncvo.sbkgno = nvo.bkgno[i];
                                    ncvo.sacontno = nvo.oldcontno[i];
                                    ncvo.service = nvo.svc[i];
                                    ncvo.svendor = nvo.vendor[i];
                                    conNAList.add(ncvo);
                                }
                            }
                        }
                    }
                }
            }
            HashMap resMap = new HashMap();
            resMap.put("VendorNA", vNAList);
            resMap.put("ContractNA", conNAList);
            logger.info("Vendor List size-->" + vNAList.size());
            logger.info("Contract List Size--> " + conNAList.size());
            obj = (Object) resMap;
            logger.info("--> End of checkVendorInSchedules");
        } catch (Exception E) {
            logger.info("Exception in checkVendorInSchedules-->" + E);
            E.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public Object check_HlgExpRpt_Exists(AcnActualContainerVo nvo) {
        /* CR 675 Validations */

        Object obj = null;
        Connection con1 = null;
        Object[] temp_ContainersFinal = null;
        try {
            logger.info("---> Start of check_HlgExpRpt_Exists");
            ArrayList hlgExpRpt = new ArrayList();
            AcnActualContainerVo svo = new AcnActualContainerVo();
            con1 = new lrp.serverutils.refs.ServerObject().getConnection();
//            String qry = " Select Count(HlgNo) As Count From HlgExpenseReportDetail(NoLock) "
//                    + " Where Bookno=? "
//                    + " And ContainerNo=? "
//                    + " And FromPort=? "
//                    + " And FromTerminal=? "
//                    + " And ToPort=? "
//                    + " And ToTerminal=? ";
//            PreparedStatement pst = con1.prepareStatement(qry);

            int vexists = -1;
            logger.info("nvo.contno.length : " + nvo.contno.length);
            for (int i = 0; i < nvo.contno.length; i++) {
                logger.info("nvo.SOpsStatus : " + nvo.aOpsSts[i] + " // nvo.bkgno : " + nvo.bkgno[i] + " // nvo.contno : " + nvo.oldcontno[i] + " // nvo.opspol : " + nvo.opspol[i] + " // nvo.opspolter : " + nvo.opspolter[i] + " // nvo.opspod : " + nvo.opspod[i] + " // nvo.opspodter : " + nvo.opspodter[i]);
                if (nvo.aOpsSts[i].contains("Haulage")) {
                    Object[] opsObjInputs = new Object[6];
                    opsObjInputs[0] = nvo.bkgno[i];
                    opsObjInputs[1] = nvo.oldcontno[i];
                    opsObjInputs[2] = nvo.opspol[i];
                    opsObjInputs[3] = nvo.opspolter[i];
                    opsObjInputs[4] = nvo.opspod[i];
                    opsObjInputs[5] = nvo.opspodter[i];

                    OPSServiceProxy opsPxy = new OPSServiceProxy();
                    String keyName = "getHlgExpenseReportDetail";
                    Object objResult = opsPxy.getServiceDetails("Actual Containers", "getHlgExpenseReportDetail", keyName, opsObjInputs);

                    temp_ContainersFinal = (Object[]) (Object[]) objResult;
                    vexists = (int) temp_ContainersFinal[0];
//                    pst.setString(1, nvo.bkgno[i]);
//                    pst.setString(2, nvo.oldcontno[i]);
//                    pst.setString(3, nvo.opspol[i]);
//                    pst.setString(4, nvo.opspolter[i]);
//                    pst.setString(5, nvo.opspod[i]);
//                    pst.setString(6, nvo.opspodter[i]);
//                    rs = pst.executeQuery();
//                    if (rs.next()) {
//                        vexists = rs.getInt("Count");
//                        logger.info("vexists : " + vexists);
                    if (vexists > 0) {
                        svo = new AcnActualContainerVo();
                        svo.sbkgno = nvo.bkgno[i];
                        svo.sacontno = nvo.oldcontno[i];
                        svo.sTDNRefNo = nvo.tdnRefNo[i];
                        hlgExpRpt.add(svo);
                    }
//                    }
                }
            }
            logger.info("hlgExpRpt Size : " + hlgExpRpt.size());
            HashMap resMap = new HashMap();
            resMap.put("HleExp", hlgExpRpt);
            obj = (Object) resMap;
            logger.info("--> End of check_HlgExpRpt_Exists");
        } catch (Exception E) {
            logger.info("Exception in check_HlgExpRpt_Exists-->" + E);
            E.printStackTrace();
        } finally {
            try {
                if (con1 != null) {
                    con1.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public Object check_FdrExpRpt_Exists(AcnActualContainerVo nvo) {
        /* CR 675 Validations */

        Object obj = null;
        Connection con = null;
        Object[] temp_ContainersFinal = null;
        try {
            logger.info("---> Start of check_FdrExpRpt_Exists");
            ArrayList fdrExpRpt = new ArrayList();
            AcnActualContainerVo svo = new AcnActualContainerVo();
            con = new lrp.serverutils.refs.ServerObject().getConnection();
//            String qry = " Select Count(reportNo) As Count From FdrExpenseReportDetail(NoLock) "
//                    + " Where BookNo=? "
//                    + " and ContainerNo=? "
//                    + " And OperationNo=? ";
//            PreparedStatement pst = con.prepareStatement(qry);
//
            int fexists = -1;
            logger.info("nvo.contno.length : " + nvo.contno.length);
            for (int i = 0; i < nvo.contno.length; i++) {
                logger.info("nvo.SOpsStatus : " + nvo.aOpsSts[i] + " // nvo.bkgno : " + nvo.bkgno[i] + " // nvo.contno : " + nvo.oldcontno[i] + " // nvo.OperationNum : " + nvo.operationNum[i]);
                if (nvo.aOpsSts[i].startsWith("Load")) {
//                    pst.setString(1, nvo.bkgno[i]);
//                    pst.setString(2, nvo.oldcontno[i]);
//                    pst.setString(3, nvo.operationNum[i]);
//                    rs = pst.executeQuery();
                    Object[] opsObjInputs = new Object[3];
                    opsObjInputs[0] = nvo.bkgno[i];
                    opsObjInputs[1] = nvo.oldcontno[i];
                    opsObjInputs[2] = nvo.operationNum[i];

                    OPSServiceProxy opsPxy = new OPSServiceProxy();
                    String keyName = "getFdrExpenseReportDetail";
                    Object objResult = opsPxy.getServiceDetails("Actual Containers", "getFdrExpenseReportDetail", keyName, opsObjInputs);

                    temp_ContainersFinal = (Object[]) (Object[]) objResult;
                    fexists = (int) temp_ContainersFinal[0];
//                    if (rs.next()) {
//                        fexists = rs.getInt("Count");
                    if (fexists > 0) {
                        svo = new AcnActualContainerVo();
                        svo.sbkgno = nvo.bkgno[i];
                        svo.sacontno = nvo.oldcontno[i];
                        fdrExpRpt.add(svo);
                    }
//                    }
                }
            }
            logger.info("fdrExpRpt Size : " + fdrExpRpt.size());
            HashMap resMap = new HashMap();
            resMap.put("FdrExp", fdrExpRpt);
            obj = (Object) resMap;
            logger.info("--> End of check_FdrExpRpt_Exists");
        } catch (Exception E) {
            logger.info("Exception in check_FdrExpRpt_Exists-->" + E);
            E.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public void SaveMethodEntry(AcnActualContainerVo ldcl, String cmpCode, String user, String EDI, String status, String carrierCode) {
        int d = 0;
        logger.info("Save method callingto EMS from operations For Load");
        int h = 0;
        String userData = user + ".System";
        String strQry = "";
        int Z = 0;
        int Rco = 0;
        int exc = 0;
        String[][] result = (String[][]) null;
        int mvtDtlcode = 0;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pst = null;
        SimpleDateFormat sdfTo = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        SimpleDateFormat dfTo = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        boolean blnSave = true;

        if (!status.trim().equalsIgnoreCase("Save")) {
            blnSave = false;
        }
        try {
            int EDICode = 1;
            con = new ServerObject().getConnection();

            String indexno = "select idxValue from opsmdlindex(NoLock) where idxprefix= ? and MDlname = ?";
            pst = con.prepareStatement(indexno);
            pst.setString(1, "OPSL");
            pst.setString(2, "ActualContainer");
            rs = pst.executeQuery();
            logger.info("Getting the previus values from module index for Load");

            if (rs.next()) {
                this.RefNo = rs.getInt(1);
            }

            int count = ldcl.operationNum.length;
            int TotalCount = this.RefNo + count;
            this.sql = "update opsmdlindex set idxValue = ? where idxprefix= 'OPSL' and MDlname = 'ActualContainer'";
            pst = con.prepareStatement(this.sql);
            pst.setInt(1, TotalCount);
            exc = pst.executeUpdate();
            logger.info("updateing the latest value in the module index For Load");
            String deleteQuery = "";
            PreparedStatement pstDel = null;
            if (!blnSave) {
                deleteQuery = "Delete from opsCntrMovementDetail where partnerEventId = ? ";
                pstDel = con.prepareStatement(deleteQuery);
            }

            logger.info("before inserting into the table For Load");
            if (blnSave) {
                strQry = "INSERT INTO opsCntrMovementDetail(EDICode, ContainerNo, CheckDigit,CarrierCode , PortCode,  movementCode, EquipmentType, VSLName, VSLCode, VoyCode,serviceCode,   Remarks, ActivityDate, POD,  finaldest , finalPort,Depot,condition,NextCompany,TransportMode,LeaseTerm, FromToAtCompany,DocumentType,DocumentRefNo,cmpCode,POER,CrDate,CrUsr,Bound,Owner,EDI,AgencyRefNo, partnerEventID,chgdate,chgusr,ChassisNo, userrun) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),'" + userData + "',?,?,?,?,?,getDate(),'" + userData + "',?, '" + user + "')";
            } else {
                strQry = "INSERT INTO opsCntrMovementDetail(EDICode, ContainerNo, CheckDigit,CarrierCode , PortCode,  movementCode, EquipmentType, VSLName, VSLCode, VoyCode,serviceCode,   Remarks, ActivityDate, POD,  finaldest , finalPort,Depot,condition,NextCompany,TransportMode,LeaseTerm, FromToAtCompany,DocumentType,DocumentRefNo,cmpCode,POER,CrDate,CrUsr,Bound,Owner,EDI,AgencyRefNo, partnerEventID,chgdate,chgusr,ChassisNo,userrun) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),'" + userData + "',?,'" + user + "')";
            }

            pst = con.prepareStatement(strQry);
            logger.info("Length of the records " + ldcl.operationNum.length);
            for (int i = 0; i < ldcl.operationNum.length; i++) {
                int tot = 0;

                pst.setInt(1, EDICode);
                pst.setString(2, ldcl.contno[i]);
                pst.setString(3, "Y");
                pst.setString(4, carrierCode);
                pst.setString(5, ldcl.opspol[i]);
                pst.setString(6, ldcl.mvcode[i]);
                pst.setString(7, ldcl.type[i]);

                pst.setString(8, "");
                pst.setString(9, ldcl.vsl[i]);
                pst.setString(10, ldcl.voy[i]);
                pst.setString(11, ldcl.svc[i]);

                pst.setString(12, ldcl.remarks[i]);
                pst.setString(13, ldcl.activeDate[i].trim());
                pst.setString(14, ldcl.opspod[i]);
                pst.setString(15, ldcl.delivery[i]);
                pst.setString(16, ldcl.blpod[i]);
                pst.setString(17, ldcl.opspolter[i]);

                pst.setString(18, "G");
                pst.setString(19, "");
                pst.setString(20, "");
                pst.setString(21, "");
                pst.setString(22, "");
                pst.setString(23, "BKG");
                pst.setString(24, ldcl.bkgno[i]);
                pst.setString(25, cmpCode);
                pst.setString(26, "");

                if (blnSave) {
                    pst.setString(27, ldcl.bnd[i]);
                    pst.setString(28, cmpCode);
                    pst.setString(29, EDI);
                    tot = this.RefNo + i + 1;
                    pst.setString(30, "OPSL" + tot);

                    pst.setString(31, "");
                    pst.setString(32, "");
                } else {
                    pst.setString(27, ldcl.bnd[i]);
                    pst.setString(28, cmpCode);
                    pst.setString(29, "");
                    pst.setString(30, "");
                    pst.setString(31, EDI);
                    pst.setString(32, "OPSL" + tot);
                    pst.setString(33, "");
                    pst.setString(34, "");
                }
                pst.addBatch();

                if (!blnSave) {
                    pstDel.setString(1, "");
                    pstDel.addBatch();
                }

            }

            int[] len = pst.executeBatch();

            if (len.length > 0) {
                int[] del = null;
                if (!blnSave) {
                    del = pstDel.executeBatch();
                }

                if ((blnSave) || ((!blnSave) && (del.length > 0))) {
                    logger.info("before calling sp For Load");
                    pst = con.prepareStatement("Exec opsEMSUpdationnew '" + user + "' ");
                    int x = 0;
                    x = pst.executeUpdate();
                    logger.info("Execution into EMS" + x);
                }

                logger.info("after calling sp For Load");
            }
            pst.close();
            if (pstDel != null) {
                pstDel.close();
            }
            con.close();
            logger.info("After Execution operations to EMS For Load");
        } catch (Exception e) {
            mvtDtlcode = 1;
            setErrorInfo(e.toString());
            logger.info("Exception inside saving Movemnet Entry" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
    }

    public String[] checkForEmsUpdate(AcnActualContainerVo ldcl) {
        String[] rtn = null;
        Connection con = null;
        try {
            logger.info("in the begin EMS checking Detail for Load");
            con = new ServerObject().getConnection();
            String Sql = "select Documentrefno, containerno, portcode, servicecode, vslcode, voycode, bound from cntrmovementdetail(nolock) where containerno= ? and movementcode= ? and portcode= ?  and  documentrefno = ?";
            rtn = new String[ldcl.contno.length];
            for (int i = 0; i < ldcl.contno.length; i++) {
                this.pst = con.prepareStatement(Sql, 1004, 1007);
                this.pst.setString(1, ldcl.contno[i]);
                this.pst.setString(2, ldcl.mvcode[i]);
                this.pst.setString(3, ldcl.opspol[i]);
                this.pst.setString(4, ldcl.bkgno[i]);
                this.rs = this.pst.executeQuery();
                logger.info("resulset fro fetching records from containermovement table From LoadConfirm");
                this.rs.last();
                int Rc = this.rs.getRow();

                this.rs.beforeFirst();
                if (Rc > 0) {
                    rtn[i] = "N";
                } else {
                    rtn[i] = "Y";
                }
            }

            logger.info("End of getDetail method for Checking Ems Data For Load Confirmation");
        } catch (Exception e) {
            logger.info("Exception Raised when Calling Value  of Checking  Data  with Ems Table " + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return rtn;
    }

    public String[][] SaveMethodEntrySrn(AcnActualContainerVo ldcl, String cmpCode, String user, String EDI, String status, String carrierCode, String dt) {
        int d = 0;
        logger.info("Save method callingto EMS from operations For Load");
        int h = 0;
        String userData = user + ".System";
        String strQry = "";
        int Z = 0;
        int Rco = 0;
        int exc = 0;
        String[][] result = (String[][]) null;
        int mvtDtlcode = 0;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pst = null;
        SimpleDateFormat sdfTo = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        SimpleDateFormat dfTo = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        boolean blnSave = true;

        if (!status.trim().equalsIgnoreCase("Save")) {
            blnSave = false;
        }
        try {
            int EDICode = 1;
            con = new ServerObject().getConnection();

            String indexno = "select idxValue from opsmdlindex(NoLock) where idxprefix= ? and MDlname = ?";
            pst = con.prepareStatement(indexno);
            pst.setString(1, "OPSL");
            pst.setString(2, "ActualContainer");
            rs = pst.executeQuery();
            logger.info("Getting the previus values from module index for Load");

            if (rs.next()) {
                this.RefNo = rs.getInt(1);
            }

            int count = ldcl.operationNum.length;
            int TotalCount = this.RefNo + count;
            this.sql = "update opsmdlindex set idxValue = ? where idxprefix= 'OPSL' and MDlname = 'ActualContainer'";
            pst = con.prepareStatement(this.sql);
            pst.setInt(1, TotalCount);
            exc = pst.executeUpdate();
            logger.info("updateing the latest value in the module index For Load");
            String deleteQuery = "";
            PreparedStatement pstDel = null;
            if (!blnSave) {
                deleteQuery = "Delete from opsCntrMovementDetail where partnerEventId = ? ";
                pstDel = con.prepareStatement(deleteQuery);
            }

            logger.info("before inserting into the table For Load");
            if (blnSave) {
                strQry = "INSERT INTO opsCntrMovementDetail(EDICode, ContainerNo, CheckDigit,CarrierCode , PortCode,  movementCode, EquipmentType, VSLName, VSLCode, VoyCode,serviceCode,   Remarks, ActivityDate, POD,  finaldest , finalPort,Depot,condition,NextCompany,TransportMode,LeaseTerm, FromToAtCompany,DocumentType,DocumentRefNo,cmpCode,POER,CrDate,CrUsr,Bound,Owner,EDI,AgencyRefNo,partnerEventID,chgdate,chgusr,ChassisNo, userrun) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),'" + userData + "',?,?,?,?,?,getDate(),'" + userData + "',?, '" + user + "')";
            } else {
                strQry = "INSERT INTO opsCntrMovementDetail(EDICode, ContainerNo, CheckDigit,CarrierCode , PortCode,  movementCode, EquipmentType, VSLName, VSLCode, VoyCode,serviceCode,   Remarks, ActivityDate, POD,  finaldest , finalPort,Depot,condition,NextCompany,TransportMode,LeaseTerm, FromToAtCompany,DocumentType,DocumentRefNo,cmpCode,POER,CrDate,CrUsr,Bound,Owner,EDI,AgencyRefNo,partnerEventID,chgdate,chgusr,ChassisNo,userrun) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,getDate(),'" + userData + "',?,'" + user + "')";
            }

            pst = con.prepareStatement(strQry);
            logger.info("Length of the records " + ldcl.operationNum.length);
            for (int i = 0; i < ldcl.operationNum.length; i++) {
                int tot = 0;

                pst.setInt(1, EDICode);
                pst.setString(2, ldcl.contno[i]);
                pst.setString(3, "Y");
                pst.setString(4, carrierCode);
                pst.setString(5, ldcl.opspol[i]);
                pst.setString(6, ldcl.mvcode[i]);
                pst.setString(7, ldcl.type[i]);

                pst.setString(8, ldcl.remarks[i]);
                pst.setString(9, ldcl.vsl[i]);
                pst.setString(10, ldcl.voy[i]);
                pst.setString(11, ldcl.svc[i]);

                pst.setString(12, "");
                pst.setString(13, dt);
                pst.setString(14, ldcl.opspod[i]);
                pst.setString(15, ldcl.delivery[i]);
                pst.setString(16, ldcl.blpod[i]);
                pst.setString(17, ldcl.opspolter[i]);

                pst.setString(18, "G");
                pst.setString(19, "");
                pst.setString(20, "");
                pst.setString(21, "");
                pst.setString(22, "");
                pst.setString(23, "BKG");
                pst.setString(24, ldcl.bkgno[i]);
                pst.setString(25, cmpCode);
                pst.setString(26, "");

                if (blnSave) {
                    pst.setString(27, ldcl.bnd[i]);
                    pst.setString(28, cmpCode);
                    pst.setString(29, EDI);
                    tot = this.RefNo + i + 1;
                    pst.setString(30, "OPSL" + tot);
                    logger.info("AgenceReferencenumber" + ldcl.bnd[i]);
                    pst.setString(31, "");
                    pst.setString(32, "");
                } else {
                    pst.setString(27, ldcl.bnd[i]);
                    pst.setString(28, cmpCode);
                    pst.setString(29, "");
                    pst.setString(30, "");
                    pst.setString(31, EDI);
                    pst.setString(32, "OPSL" + tot);
                    pst.setString(33, "");
                    pst.setString(34, "");
                }
                pst.addBatch();

                if (!blnSave) {
                    pstDel.setString(1, "");
                    pstDel.addBatch();
                }

            }

            int[] len = pst.executeBatch();

            if (len.length > 0) {
                int[] del = null;
                if (!blnSave) {
                    del = pstDel.executeBatch();
                }

                if ((blnSave) || ((!blnSave) && (del.length > 0))) {
                    logger.info("before calling sp For Load");
                    pst = con.prepareStatement("Exec opsEMSUpdationnew '" + user + "' ");
                    int x = 0;
                    x = pst.executeUpdate();
                }

                logger.info("after calling sp For Load");
            }
            pst.close();
            if (pstDel != null) {
                pstDel.close();
            }
            con.close();
            logger.info("After Execution operations to EMS For Load");
        } catch (Exception e) {
            mvtDtlcode = 1;
            setErrorInfo(e.toString());
            logger.info("Exception inside saving Movemnet Entry" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return result;
    }

    public String getErrorInfo() {
        return this.Err;
    }

    public String setErrorInfo(String Err) {
        this.Err = Err;
        return Err;
    }

    public String getETA(String ser, String ves, String voy, String bnd, String pod, String ter, String gcid) {
        String etaetd = "";
        Connection con = null;
        try {
            logger.info("in the begin og get ETA   ");

            con = new ServerObject().getConnection();
            String Sql = "Select top 1 arrdate,depdate from allschedules(nolock) where service=? and vessel=? and voyage=? and bound=? and port=? and terminal=? and  groupcallid=?";

            this.pst = con.prepareStatement(Sql);
            this.pst.setString(1, ser);
            this.pst.setString(2, ves);
            this.pst.setString(3, voy);
            this.pst.setString(4, bnd);
            this.pst.setString(5, pod);
            this.pst.setString(6, ter);
            this.pst.setString(7, gcid);

            this.rs = this.pst.executeQuery();
            this.rs.next();
            etaetd = this.rs.getString("arrdate") + "//" + this.rs.getString("depdate");

            logger.info("ETA and ETD-->" + etaetd);
            logger.info("End of getETA method for ActualContainer");
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            if (con != null) {
                try {
                    con.close();
                    this.pst.close();
                } catch (Exception e) {
                    setErrorInfo(e.getMessage());
                }
            }
        }
        return etaetd;
    }

    public Object getContainerStatus(ArrayList cnnoal, ArrayList cntypeal, String port, ArrayList norflag, String pluginRRRO) {
        Object object = null;
        HashMap ContainerTypeMap = new HashMap();
        Connection con = null;
        String nor = "";
        ArrayList Equiptype = new ArrayList();
        //     pluginRRRO = getConfiguration();
        try {
            logger.info("-->in the begin of getContainerStatus ");

            con = new ServerObject().getConnection();
            if ("true".equalsIgnoreCase(pluginRRRO)) {
                sql = "select equiptype from equipments where type='r' and status='A'";
                pst = con.prepareStatement(sql);
                rs = pst.executeQuery();
                while (rs.next()) {
                    Equiptype.add(rs.getString("equiptype"));

                }
                Calendar calValue = new GregorianCalendar();
                calValue.setTime(new Date());

                long tmStamp = calValue.getTimeInMillis();

                logger.info("User id for inventory-->" + String.valueOf(tmStamp).trim());

                String sql = "INSERT INTO OperationInventory (USerID,Containerno,Type,Count) values(?,?,?,?)";
                this.pst = con.prepareStatement(sql);

                int contcount = 0;
                logger.info("cnnoal-->" + cnnoal.size());
                for (int i = 0; i < cnnoal.size(); i++) {
                    this.pst.setString(1, String.valueOf(tmStamp));
                    this.pst.setString(2, cnnoal.get(i).toString());
                    this.pst.setString(3, cntypeal.get(i).toString());
                    if (Equiptype.contains(cntypeal.get(i).toString().trim())) {
                        pst.setString(4, norflag.get(i).toString().trim());
                    } else {
                        pst.setString(4, "");
                    }
                    this.pst.addBatch();
                }
                this.pst.executeBatch();

                String sql1 = "EXEC usp_OpsCheckinginventory ? ";
                pst = con.prepareStatement(sql1);

                pst.setString(1, String.valueOf(tmStamp));

                rs = pst.executeQuery();

                int count = 0;
                while (this.rs.next()) {
                    ContainerTypeMap.put(this.rs.getString("Containerno"), this.rs.getString("MappedType"));
                    count++;
                }
                logger.info("Containers in Inventory -->" + count);

            } else {
                Calendar calValue = new GregorianCalendar();
                calValue.setTime(new Date());

                long tmStamp = calValue.getTimeInMillis();

                logger.info("User id for inventory-->" + String.valueOf(tmStamp).trim());

                String sql = "INSERT INTO OperationInventory (USerID,Containerno,Type,Count) values(?,?,?,?)";
                this.pst = con.prepareStatement(sql);

                int contcount = 0;
                logger.info("cnnoal-->" + cnnoal.size());
                for (int i = 0; i < cnnoal.size(); i++) {
                    this.pst.setString(1, String.valueOf(tmStamp));
                    this.pst.setString(2, cnnoal.get(i).toString());
                    this.pst.setString(3, cntypeal.get(i).toString());
                    this.pst.setString(4, "N");
                    this.pst.addBatch();
                }
                this.pst.executeBatch();

                String sql1 = "EXEC usp_OpsCheckinginventory ? ";
                pst = con.prepareStatement(sql1);

                pst.setString(1, String.valueOf(tmStamp));

                rs = pst.executeQuery();

                int count = 0;
                while (this.rs.next()) {
                    ContainerTypeMap.put(this.rs.getString("Containerno"), this.rs.getString("MappedType"));
                    count++;
                }
                logger.info("Containers in Inventory -->" + count);
            }
            logger.info("-->in the end of getContainerStatus  ");

            object = ContainerTypeMap;
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }

        if (ContainerTypeMap.size() > 0) {
            return new CompressAPI().compress(object);
        }

        return "No Records";
    }

    public AcnActualContainerVo checkingMovementcode(AcnActualContainerVo cmcvo) {
        AcnActualContainerVo invalidconvo = null;
        Connection con = null;
        try {
            logger.info("----->ActualContainer-inside the checkingMovementcode");

            con = new ServerObject().getConnection();
            logger.info("cmcvo length-->" + cmcvo.bkgno.length);

            Random Rand = new Random();
            int Randomno = Rand.nextInt(10000) + 10;

            logger.info("User ID is -->" + Randomno);

            String bookno = "";
            String contno = "";
            String opspol = "";
            String reftype = "";
            String mt = "";
            String status = "";
            int tot = 0;

            this.sql = "INSERT INTO OperationMovementCode (USerID,Bookno,Containerno,Port,Status,Count) values(?,?,?,?,?,?)";
            this.pst = con.prepareStatement(this.sql);

            for (int i = 0; i < cmcvo.bkgno.length; i++) {
                bookno = cmcvo.bkgno[i].trim();
                contno = cmcvo.contno[i].trim();
                opspol = cmcvo.opspol[i].trim();
                reftype = cmcvo.reftype[i].trim();
                mt = cmcvo.mt[i].trim();

                if ((reftype.equalsIgnoreCase("W")) || (mt.equalsIgnoreCase("M"))) {
                    status = "E";
                } else {
                    status = "L";
                }

                this.pst.setInt(1, Randomno);
                this.pst.setString(2, bookno);
                this.pst.setString(3, contno);
                this.pst.setString(4, opspol);
                this.pst.setString(5, status);
                this.pst.setString(6, "N");

                this.pst.addBatch();
            }
            this.pst.executeBatch();

            this.sql = "EXEC usp_checkingmovementcode ?";
            this.pst = con.prepareStatement(this.sql, 1004, 1007);
            this.pst.setInt(1, Randomno);
            this.rs = this.pst.executeQuery();

            this.rs.last();
            int Rc = this.rs.getRow();
            invalidconvo = new AcnActualContainerVo(Rc);
            logger.info("Invalid movement code count-->" + Rc);

            if (this.rs.getRow() > 0) {
                this.rs.beforeFirst();
                int t = 0;
                while (this.rs.next()) {
                    invalidconvo.bkgno[t] = this.rs.getString("bookno");
                    invalidconvo.contno[t] = this.rs.getString("containerno");
                    t++;
                }
            }

            logger.info("----->ActualContainer End of set checkingMovementcode ");
        } catch (SQLException e) {
            logger.fatal(e);
            logger.info("Exception in Operation Module");
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }

        return invalidconvo;
    }

    public Object setContainers(AcnActualContainerVo oldlcvo, AcnActualContainerVo lcvo, String headers, String pluginRRRO, String Usercr, String stuffplugin, String updateforecast, String editrigger, String WCNplugin, String pluginDGVupdate) {
        Object object = null;
        HashMap Exceptionmap = new HashMap();
        Connection con = null;
        usercredievent = Usercr;
        try {
            logger.info("###################################");

            //  pluginRRRO = getConfiguration();
            System.out.println("pluginRRO>>>>>>>>>>>>>>>>" + pluginRRRO);

            logger.info("Updation on : " + lcvo.updationOn);
            con = new ServerObject().getConnection();
            String sql = " select count(booking.book_no) from Booking(Nolock)  inner join  booking_cargo (Nolock) ON booking.book_no = booking_Cargo.book_no  where booking.status in ('D','C')  and Booking.servicecode=?  and Booking.vesselcode=?  and Booking.voyagecode=?  and Booking.bound=?  and booking_cargo.eqpid=? ";
            this.pst1 = con.prepareStatement(sql);

            sql = " SELECT count(bookno) FROM operationdetail(Nolock)  where servicecode=?  and vesselcode=?  and voyage=?  and bound=?  and containerno=?  and prevoperation=0  and opsclose='0' and servicecode<>'' ";
            this.pst2 = con.prepareStatement(sql);

            String qry3 = "select count(mnrrefno) from Emptyworeqmaster MS(NOLOCK)\n"
                    + "INNER JOIN EMPTYWOREQDETAIL DT(NOLOCK) ON DT.REFERENCENO = MS.REFERENCENO \n"
                    + "INNER JOIN EMPTYWOREQCARGO CT(NOLOCK) ON CT.bookno = MS.mnrrefno and CT.status='A'\n"
                    + "WHERE MS.STATUS ='A'  AND DT.ITEMAPPROVAL='A' and\n"
                    + "MS.SERVICE=? and MS.VSLCODE=? and MS.VOYCODE = ? and MS.BOUND=? and MS.mnrrefno <> ? and \n"
                    + "CT.eqpid=? and MS.fmloc=?  and MS.fmtml=? and  (MS.SERVICE <> '' and MS.SERVICE is not null)";

            this.pstwo = con.prepareStatement(qry3);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set eqpid=?,eqptypeid=?,isdummy='N',movetype='ACN'  where cid = @cid";

            PreparedStatement pstbk_cargo_C = con.prepareStatement(sql);

            sql = "select  cid into #tmpopr from tpy_booking_cargo (nolock) inner join tpy_booking (nolock) on tpy_booking.book_no=tpy_booking_cargo.book_no where  tpy_booking_cargo.masterbookno=? and tpy_booking_cargo.eqpid=? Update tpy_booking_cargo set eqpid=?,eqptypeid=?,isdummy='N',movetype='ACN'  from #tmpopr  where tpy_booking_cargo.cid =  #tmpopr.cid  and   tpy_booking_cargo.status='A'";
            PreparedStatement psttpybk_cargo_C = con.prepareStatement(sql);

            sql = " update nd set  containerno=?,eqptype=? from ndgmaster nm(nolock) inner join ndgdetail nd(nolock) on nm.dgno=nd.dgno where partdgrefno=? and containerno=? and nm.status not in ('R','C')";
            PreparedStatement pstndg_C = con.prepareStatement(sql);

            sql = " update nd set  containerno=? from ndgmaster nm(nolock) inner join DGValidationResponse nd(nolock) on nm.dgno=nd.dgno where partdgrefno=? and containerno=? and nm.status not in ('R','C') and nd.status <> 'V'";
            PreparedStatement pstndg_CDGValidationResponse = con.prepareStatement(sql);

            sql = " update nd set  containerno=? from ndgmaster nm(nolock) inner join DGValidationQ nd(nolock) on nm.dgno=nd.dgno where partdgrefno=? and containerno=? and nm.status not in ('R','C')";
            PreparedStatement pstndg_CDGValidationQ = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update bc set tareWeight = case when (cp.tareWt is not Null AND cp.tareWt>0) then cp.tareWt else stm.tarekg end,  grossWeight = (case when (cp.tareWt is not Null  AND cp.tareWt>0) then cp.tareWt else stm.tarekg end)+cargoWeight from booking_cargo bc  inner join Equipmentpassport cp(NoLock) ON bc.eqpId = cp.Containerno  inner join emsEquipmentMaster stm (NoLock) ON stm.equiptype = bc.eqpTypeId  where cid = @cid and cp.status='A' and stm.Status='A' ";
            PreparedStatement pstbk_cargo_tareWt = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update bc set tareWeight = case when (cp.tareWt is not Null AND cp.tareWt>0) then (case when bc.weightunit ='K' then cp.tareWt when bc.weightunit ='T' then cp.tareWt/1000 when bc.weightunit ='P' then cp.tareWt*2.204  else '' end)  else  (case when bc.weightunit ='K' then stm.tarekg when bc.weightunit ='T' then stm.tarekg/1000 when bc.weightunit ='P' then stm.tarekg*2.204  else '' end) end,  grossWeight = (case when (cp.tareWt is not Null AND cp.tareWt>0) then (case when bc.weightunit ='K' then cp.tareWt when bc.weightunit ='T' then cp.tareWt/1000 when bc.weightunit ='P' then cp.tareWt*2.204  else '' end)  else  (case when bc.weightunit ='K' then stm.tarekg when bc.weightunit ='T' then stm.tarekg/1000 when bc.weightunit ='P' then stm.tarekg*2.204  else '' end) end)+cargoWeight from booking_cargo bc  inner join Equipmentpassport cp(NoLock) ON bc.eqpId = cp.Containerno  inner join emsEquipmentMaster stm (NoLock) ON stm.equiptype = bc.eqpTypeId  where cid = @cid and cp.status='A' and stm.Status='A'";
            PreparedStatement pstbk_cargo_tareWtGWF = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set cargoweight=?,grossweight=tareWeight+?,weightunit='K' where cid = @cid";
            PreparedStatement pstbk_cargo_S = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set cargoweight=?,grossweight=tareWeight+?,weightunit=? where cid = @cid";
            PreparedStatement pstbk_cargo_SGWF = con.prepareStatement(sql);

            sql = "select count(cid) as  count  from booking_commodity (nolock)  where book_no=? and eqpid=? ";
            PreparedStatement pstbk_com_CWcount = con.prepareStatement(sql);

            sql = "select cid into #tmpopr from booking_commodity (nolock)  where book_no=? and eqpid=?  UPDATE booking_commodity SET itemweight=? from #tmpopr  where booking_commodity.cid = #tmpopr.cid ";
            PreparedStatement pstbk_com_CW = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set seal1No=? where cid = @cid";
            PreparedStatement pstbk_cargo_S1 = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set seal2No=? where cid = @cid";
            PreparedStatement pstbk_cargo_S2 = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set otherseal=? where cid = @cid";
            PreparedStatement pstbk_cargo_S3 = con.prepareStatement(sql);

            sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set remarks=? where cid = @cid";
            PreparedStatement pstbk_cargo_V = con.prepareStatement(sql);

            sql = " UPDATE operationdetail set stowageposition=? where operationdno =? ";
            PreparedStatement pstbk_cargo_SO = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from booking_commodity (nolock)  where book_no=? and eqpid=?  UPDATE booking_commodity SET eqpid=? from #tmpopr  where booking_commodity.cid = #tmpopr.cid ";
            PreparedStatement pstbk_com_C = con.prepareStatement(sql);

            sql = " select tpy_booking_commodity.cid  into #tmpopr from tpy_booking_commodity (nolock)  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=tpy_booking_commodity.book_no where tpy_booking_cargo.masterbookno=? and tpy_booking_commodity.eqpid=? and tpy_booking_cargo.status='A'  UPDATE tpy_booking_commodity SET eqpid=? from #tmpopr  where tpy_booking_commodity.cid = #tmpopr.cid ";
            PreparedStatement psttpybk_com_C = con.prepareStatement(sql);

            sql = " select id into #tmpopr from TPY_booking_imo (nolock)  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=TPY_booking_imo.book_no where tpy_booking_cargo.masterbookno=? and TPY_booking_imo.eqpid=? and tpy_booking_cargo.status='A'  UPDATE TPY_booking_imo SET eqpid=?,eqpType=? from #tmpopr  where TPY_booking_imo.id = #tmpopr.id";
            PreparedStatement psttpybk_imo_C = con.prepareStatement(sql);

            sql = "select TPY_booking_chargenew.cid into #tmpopr from TPY_booking_chargenew (nolock)  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=TPY_booking_chargenew.book_no where tpy_booking_cargo.masterbookno=? and TPY_booking_chargenew.eqpid=? and tpy_booking_cargo.status='A'   UPDATE TPY_booking_chargenew SET eqpid=?,eqptypeid=? from #tmpopr  where TPY_booking_chargenew.cid = #tmpopr.cid ";
            PreparedStatement psttpybk_charge_C = con.prepareStatement(sql);

            sql = "select TPY_booking_contacts.cid into #tmpopr from TPY_booking_contacts (nolock)  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=TPY_booking_contacts.book_no where tpy_booking_cargo.masterbookno=? and TPY_booking_contacts.eqpid=? and tpy_booking_cargo.status='A'   UPDATE TPY_booking_contacts SET eqpid=? from #tmpopr  where TPY_booking_contacts.cid = #tmpopr.cid  ";
            PreparedStatement psttpybk_contact_C = con.prepareStatement(sql);

            sql = " select id into #tmpopr from booking_imo (nolock)  where book_no=? and eqpid=?  UPDATE booking_imo SET eqpid=?,eqptype=? from #tmpopr  where booking_imo.id = #tmpopr.id ";
            PreparedStatement pstbk_imo_C = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from booking_haulage (nolock)  where book_no=? and eqpid=?  UPDATE booking_haulage SET eqpid=?,eqptypeid=? from #tmpopr  where booking_haulage.cid = #tmpopr.cid ";
            PreparedStatement pstbk_haulage_C = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from booking_charge (nolock)  where book_no=? and eqpid=?  UPDATE booking_charge SET eqpid=?,eqptypeid=? from #tmpopr  where booking_charge.cid = #tmpopr.cid ";
            PreparedStatement pstbk_charge_C = con.prepareStatement(sql);

            sql = "select cid into #tmpopr from booking_contacts (nolock)  where book_no=? and eqpid=?  UPDATE booking_contacts SET eqpid=? from #tmpopr  where booking_contacts.cid = #tmpopr.cid ";
            PreparedStatement pstbk_contact_C = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no  and bl.book_no=?  and bl_cargo.eqpid=?  UPDATE bl_cargo SET eqpid=?,eqptypeid=? from #tmpopr  where bl_cargo.cid = #tmpopr.cid ";

            PreparedStatement pstbl_cargo_C = con.prepareStatement(sql);

            sql = " select tpy_bl_cargo.cid into #tmpopr from tpy_bl_cargo (nolock)  inner join tpy_bl (nolock) on tpy_bl.bl_no=tpy_bl_cargo.bl_no  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=tpy_bl.book_no where tpy_booking_cargo.masterbookno=? and tpy_bl_cargo.eqpid=? and tpy_bl_cargo.status='A'  UPDATE tpy_bl_cargo SET eqpid=?,eqptypeid=? from #tmpopr  where tpy_bl_cargo.cid = #tmpopr.cid";
            PreparedStatement psttpybl_cargo_C = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no   where bl.book_no=? and bl_cargo.eqpid=?  Update bc set tareWeight = case when (cp.tareWt is not Null and cp.tareWt<>'' and cp.tareWt>0) then cp.tareWt else stm.tarekg end,  grossWeight = ( case when (cp.tareWt is not Null and cp.tareWt<>'' and cp.tareWt>0) then cp.tareWt else stm.tarekg end)+cargoWeight,  ActualContainerFlag = (case when(( case when (cp.tareWt is not Null and cp.tareWt<>'' and cp.tareWt>0) then cp.tareWt else stm.tarekg end)+cargoWeight) <> grossWeight then 'W' else bc.ActualContainerFlag end ) from #tmpopr  inner join bl_cargo bc(nolock) ON #tmpopr.cid = bc.cid  inner join Equipmentpassport cp(NoLock) ON bc.eqpId = cp.Containerno  inner join emsEquipmentMaster stm (NoLock) ON stm.equiptype = bc.eqpTypeId  where cp.status='A' and stm.status='A' ";
            PreparedStatement pstbl_cargo_tareWt = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no  into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no   where bl.book_no=? and bl_cargo.eqpid=?  Update bc set tareWeight = case when (cp.tareWt is not Null and cp.tareWt<>'' and cp.tareWt>0) then (case when bc.weightunit ='K' then cp.tareWt when bc.weightunit ='T' then cp.tareWt/1000 when bc.weightunit ='P' then cp.tareWt * 2.204  else '' end)  else (case when bc.weightunit ='K' then stm.tarekg when bc.weightunit ='T' then stm.tarekg/1000 when bc.weightunit ='P' then stm.tarekg * 2.204  else '' end) end, grossWeight = (case when (cp.tareWt is not Null AND cp.tareWt>0) then (case when bc.weightunit ='K' then cp.tareWt when bc.weightunit ='T' then cp.tareWt/1000 when bc.weightunit ='P' then cp.tareWt*2.204  else '' end)  else (case when bc.weightunit ='K' then stm.tarekg when bc.weightunit ='T' then stm.tarekg/1000 when bc.weightunit ='P' then stm.tarekg*2.204  else '' end) end)+cargoWeight, ActualContainerFlag = (case when((case when (cp.tareWt is not Null AND cp.tareWt>0) then (case when bc.weightunit ='K' then cp.tareWt when bc.weightunit ='T' then cp.tareWt/1000 when bc.weightunit ='P' then cp.tareWt*2.204  else '' end)  else (case when bc.weightunit ='K' then stm.tarekg when bc.weightunit ='T' then stm.tarekg/1000 when bc.weightunit ='P' then stm.tarekg*2.204  else '' end) end)+cargoWeight) <> grossWeight then 'W' else bc.ActualContainerFlag end ) from #tmpopr  inner join bl_cargo bc(nolock) ON #tmpopr.cid = bc.cid  inner join Equipmentpassport cp(NoLock) ON bc.eqpId = cp.Containerno  inner join emsEquipmentMaster stm (NoLock) ON stm.equiptype = bc.eqpTypeId  where cp.status='A' and stm.status='A'  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_cargo_tareWtGWF = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from bl_commodity (nolock)  inner join bl (nolock) on bl.bl_no=bl_commodity.bl_no  and bl.book_no=?  and bl_commodity.eqpid=?  UPDATE bl_commodity SET eqpid=? from #tmpopr  where bl_commodity.cid = #tmpopr.cid ";
            PreparedStatement pstbl_com_C = con.prepareStatement(sql);

            sql = "select tpy_bl_commodity.cid into #tmpopr from tpy_bl_commodity (nolock)  inner join tpy_bl (nolock) on tpy_bl.bl_no=tpy_bl_commodity.bl_no  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=tpy_bl.book_no where tpy_booking_cargo.masterbookno=? and tpy_bl_commodity.eqpid=? and tpy_booking_cargo.status='A'   UPDATE tpy_bl_commodity SET eqpid=? from #tmpopr  where tpy_bl_commodity.cid = #tmpopr.cid  ";
            PreparedStatement psttpybl_com_C = con.prepareStatement(sql);

            sql = " select id into #tmpopr from bl_imo (nolock)  inner join bl (nolock) on bl.bl_no=bl_imo.bl_no  and bl.book_no=?  and bl_imo.eqpid=?  UPDATE bl_imo SET eqpid=?,eqptype=? from #tmpopr  where bl_imo.id = #tmpopr.id ";
            PreparedStatement pstbl_imo_C = con.prepareStatement(sql);

            sql = "  select id into #tmpopr from tpy_bl_imo (nolock)  inner join tpy_bl (nolock) on tpy_bl.bl_no=tpy_bl_imo.bl_no  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=tpy_bl.book_no where tpy_booking_cargo.masterbookno=? and tpy_bl_imo.eqpid=? and tpy_booking_cargo.status='A'   UPDATE tpy_bl_imo SET eqpid=?,eqptype=? from #tmpopr  where tpy_bl_imo.id = #tmpopr.id";
            PreparedStatement psttpybl_imo_C = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from bl_contacts (nolock)  inner join bl (nolock) on bl.bl_no=bl_contacts.bl_no  and bl.book_no=?  and bl_contacts.eqpid=?  UPDATE bl_contacts SET eqpid=? from #tmpopr  where bl_contacts.cid = #tmpopr.cid ";
            PreparedStatement pstbl_contact_C = con.prepareStatement(sql);

            sql = "  select tpy_bl_contacts.cid into #tmpopr from tpy_bl_contacts (nolock)  inner join tpy_bl (nolock) on tpy_bl.bl_no=tpy_bl_contacts.bl_no  inner join tpy_booking_cargo (nolock) on tpy_booking_cargo.book_no=tpy_bl.book_no where tpy_booking_cargo.masterbookno=? and tpy_bl_contacts.eqpid=? and tpy_booking_cargo.status='A'   UPDATE tpy_bl_contacts SET eqpid=? from #tmpopr  where tpy_bl_contacts.cid = #tmpopr.cid ";
            PreparedStatement psttpybl_contact_C = con.prepareStatement(sql);

            sql = "select operationdno into #tmpopr from operationdetail (nolock)  where bookno=?  and Containerno=?  UPDATE operationdetail set containerno=?,equipmenttype=?,ischanged=? from #tmpopr  where operationdetail.operationdno = #tmpopr.operationdno ";
            PreparedStatement pstops = con.prepareStatement(sql);

            sql = "UPDATE operationdetail set stowageposition=? where operationdno =?";
            PreparedStatement pstopssp = con.prepareStatement(sql);

            sql = "select id into #tmpopr from tdndetail(Nolock)  where bookno=? and containerno=?  UPDATE tdndetail set containerno=?,eqptype=? from #tmpopr  Where tdndetail.id=#tmpopr.id ";
            PreparedStatement psttdn = con.prepareStatement(sql);

            sql = "if object_id('tempdb..#tmpopr') is not null drop table #tmpopr";
            PreparedStatement psttmpdrop = con.prepareStatement(sql);

            sql = "if object_id('tempdb..#tmpopr') is not null drop table #tmpopr";
            PreparedStatement psttmpdrop1 = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl_cargo.loadingcondition='FF' and bl.status='D'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.cargoweight=?,blc.grossweight=blc.tareweight+?,blc.weightunit='K',blc.ActualContainerFlag='W' from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.cargoweight<>?) ";
            PreparedStatement pstbl_update_cb = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl_cargo.loadingcondition='FF' and bl.status='D'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.cargoweight=?,blc.grossweight=blc.tareweight+?,blc.ActualContainerFlag='W',blc.weightunit=? from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.cargoweight<>?)  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_update_cbGWF = con.prepareStatement(sql);

            sql = " select Count(cid) as blcount  from bl_commodity (nolock)  inner join bl (nolock) on bl.bl_no=bl_commodity.bl_no  and bl.book_no=?  and bl_commodity.eqpid=?  and bl.status='D'";
            PreparedStatement pstbl_com_CWcount = con.prepareStatement(sql);

            sql = " select cid into #tmpopr from bl_commodity (nolock)  inner join bl (nolock) on bl.bl_no=bl_commodity.bl_no  and bl.book_no=?  and bl_commodity.eqpid=?  and bl.status='D'  Select blc.cid,blc.itemweight into #TmpBLC from #tmpopr (Nolock)  INNER JOIN  bl_commodity blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  UPDATE bl_commodity SET itemweight=? from #TmpBLC  where bl_commodity.cid = #TmpBLC.cid and (#TmpBLC.itemweight<>?) Drop Table #TmpBLC ";
            PreparedStatement pstbl_com_CW = con.prepareStatement(sql);

            if ("true".equalsIgnoreCase(pluginRRRO)) {
                sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl_cargo.loadingcondition='FF' and bl.status ='D'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.seal1No=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.seal1No<>?)   UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            } else {
                sql = " select cid into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl_cargo.loadingcondition='FF' and bl.status ='D'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.seal1No=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.seal1No<>?)  ";
            }
            PreparedStatement pstbl_update_cb1 = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl_cargo.loadingcondition='FF' and bl.status ='D'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.seal2no=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.seal2no<>?)  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_update_cb2 = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl_cargo.loadingcondition='FF' and bl.status ='D'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.otherseals=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.otherseals<>?)  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_update_cb3 = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl.status ='C'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.seal1No=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.seal1No<>?)  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_update_cb1confirm1 = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl.status ='C'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.seal2no=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.seal2no<>?)  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_update_cb1confirm2 = con.prepareStatement(sql);

            sql = " select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no and bl.book_no=?  and bl_cargo.eqpid=? and bl.status ='C'  Select blc.cid,blc.seal1no,blc.seal2no,blc.otherseals,blc.tareweight,blc.cargoweight,blc.grossweight into #TmpBL from #tmpopr (Nolock)  INNER JOIN  bl_cargo blc(nolock) on blc.cid=#tmpopr.cid  where blc.cid = #tmpopr.cid  update blc set blc.otherseals=?,blc.ActualContainerFlag = (Select case when blc.ActualContainerFlag='W' then 'W' else 'Y' end) from #TmpBL  INNER JOIN bl_cargo blc(nolock) on blc.cid=#TmpBL.cid where blc.cid = #TmpBL.cid and (#TmpBL.otherseals<>?)  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
            PreparedStatement pstbl_update_cb1confirm3 = con.prepareStatement(sql);

            sql = " select id into #tmpopr from transrework_cargo(nolock)  where transbook_no=?  and reworkeqpid=?  Update transrework_cargo set reworkeqpid=?,eqptypeid=? from #tmpopr  where transrework_cargo.id = #tmpopr.id ";
            PreparedStatement psttransrework_cargo = con.prepareStatement(sql);

            sql = " select id into #tmpopr from transrework_commodity(nolock)  where  transbook_no=? and reworkeqpid=?  UPDATE transrework_commodity SET reworkeqpid=?,eqptypeid=? from #tmpopr  where transrework_commodity.id = #tmpopr.id ";
            PreparedStatement psttransrework_commodity = con.prepareStatement(sql);

            sql = " Update booking_reference set refval=? where book_no=? and refid='ITN' ";
            PreparedStatement pstbooking_reference_update = con.prepareStatement(sql);

            sql = " Create Table #tmpBL(bl_no varchar(30))  Insert into #tmpBL(bl_no) Select distinct bl.bl_no from bl (nolock)  inner join bl_reference blref(nolock) on bl.bl_no=blref.bl_no  where bl.bl_no=blref.bl_no and bl.status<>'V' and bl.book_no=?  and bl.systemBLType not in ('LCL BL','Rework BL')  Update bl_reference set refval=? from #tmpBL  where bl_reference.bl_no=#tmpBL.bl_no and refid='ITN' ";
            PreparedStatement pstbl_reference_update = con.prepareStatement(sql);

            sql = " if object_id('tempdb..#tmpBL') is not null Drop Table #tmpBL ";
            PreparedStatement pstbl_droptmp = con.prepareStatement(sql);

            sql = " Create Table #tmpFCNBL(bl_no varchar(30),FCNNo varchar(30))  Insert into #tmpFCNBL(bl_no,FCNNo) Select distinct fcn_bl.bl_no,fcn_bl.FCNNo from fcn_bl(nolock)  inner join fcn_bl_reference blref(nolock) on fcn_bl.bl_no=blref.bl_no  where fcn_bl.status<>'V' and fcn_bl.book_no=?  and fcn_bl.systemBLType not in ('LCL BL','Rework BL')  Update fcn_bl_reference set refval=? from #tmpFCNBL  where fcn_bl_reference.bl_no=#tmpFCNBL.bl_no and fcn_bl_reference.FCNNO=#tmpFCNBL.FCNNo and refid='ITN' ";

            PreparedStatement pstfcn_bl_reference_update = con.prepareStatement(sql);

            sql = " if object_id('tempdb..#tmpFCNBL') is not null Drop Table #tmpFCNBL ";
            PreparedStatement pstfcnbl_droptmp = con.prepareStatement(sql);

            sql = " Create Table #tmpBk(book_no varchar(30),refid varchar(15))  Insert into #tmpBk(book_no,refid) select distinct bk.book_no,'ITN' as refid from booking bk(nolock) where book_no=?  insert into booking_reference (refid, refval, isedited, book_no)  Select 'ITN',?,'N',#tmpbk.book_no from #tmpbk  inner join reference ref(nolock) on #tmpBk.refid = ref.refid  where ref.status='A'   drop table #tmpbk ";

            PreparedStatement pstbooking_reference_insert = con.prepareStatement(sql);

            sql = " Create Table #tmpBL(bl_no varchar(30),book_no varchar(30),refid varchar(15))  Insert into #tmpBL(bl_no,book_no,refid) Select distinct bl.bl_no,bl.book_no,'ITN' from bl(nolock)  where bl.status<>'V' and bl.book_No=?  and bl.systemBLType not in ('LCL BL','Rework BL')  insert into bl_reference (refid, refval, bl_no) select ref.refid,?,#tmpBL.bl_no from #tmpBL  inner join reference ref(nolock) on #tmpBL.refid = ref.refid  where ref.status='A'  Drop Table #tmpBL ";

            PreparedStatement pstbl_reference_insert = con.prepareStatement(sql);

            sql = " Create Table #tmpFCN(FCNNo varchar(30),bl_no varchar(30),book_no varchar(30),refid varchar(15))  Insert into #tmpFCN(FCNNo,bl_no,book_no,refid) Select fcn_bl.FCNNo,fcn_bl.bl_no,fcn_bl.book_no,'ITN' from fcn_bl(nolock)  where fcn_bl.status<>'V' and fcn_bl.book_No=?  and fcn_bl.systemBLType not in ('LCL BL','Rework BL')  insert into fcn_bl_reference (refid, refval, bl_no,FCNNo,isEdited)  select ref.refid,?,#tmpFCN.bl_no,#tmpFCN.FCNNo,'N' from #tmpFCN  inner join reference ref(nolock) on #tmpFCN.refid = ref.refid  where ref.status='A'  Drop Table #tmpFCN ";

            PreparedStatement pstfcn_bl_reference_insert = con.prepareStatement(sql);

            sql = " Update OperationDetail set GateClearance=?, GateDocDate=?, GateInClearanceDate=?, GateReferenceRemarks=?, GateClearanceMode=? where OperationDNo=? ";
            PreparedStatement pstops_GateInfo = con.prepareStatement(sql);

            sql = " Update booking_cargo set GateClearance=?, GateDocDate=?, GateInClearanceDate=?, GateReferenceRemarks=?, GateClearanceMode=? where book_no=? and eqpId=? ";
            PreparedStatement pstBkgCargo_GateInfo = con.prepareStatement(sql);

            sql = " select blc.cid into #tmpchargeid from bl_chargenew blc(nolock)  inner join bl (nolock) on bl.bl_no=blc.bl_no  inner join bl_cargo bc (nolock) on bl.bl_no=bc.bl_no  where bl.book_no=? and bc.eqpId=? and blc.autoRated in ('A','O','E')  and bl.status='D' and bc.loadingCondition='FF' and bc.ActualContainerFlag='W' and bl.sbltype not in ('PP','PM')  delete from bl_chargenew from #tmpchargeid where bl_chargenew.cid=#tmpchargeid.cid  Drop table #tmpchargeid ";

            PreparedStatement pstbl_remove_AutoRatedCharges = con.prepareStatement(sql);

            /* CR 675 Changes Start - Vendor Updation in Operations Start by Silambarasan P on 06/09/2013 */

 /* -- Operations and TDNLeg and TDNCharge - Vendor Updation -- */
            // Booking            
            sql = " Select od.OperationDNo Into #tmpops From OperationDetail od(NoLock) "
                    + " Where od.TDNRefNo=? And od.BookNo=? And od.ContainerNo=? "
                    + " Update od Set VendorCode = ? From OperationDetail od(NoLock) "
                    + " Inner Join #tmpops t1 On t1.OperationDNo = od.OperationDNo "
                    + " Update tl Set VendorCode = ? From TDNLeg tl(NoLock) Where TDNRefNo = ? "
                    + " Delete From TDNCharge Where TDNRefNo = ?  And ChargeType='A' ";
            PreparedStatement pstUpVendorTDN = con.prepareStatement(sql);

            sql = " if object_id('tempdb..#tmpops') is not null Drop Table #tmpops ";
            PreparedStatement psttmpdrop_ops = con.prepareStatement(sql);

            // Workorder            
            sql = " Select tl.id,tl.TDNRefNo into #tdnleg From TDNLeg tl(NoLock) "
                    + " Inner Join TDNDetail td(NoLock) On tl.Reference = td.Reference "
                    + " Where td.BookNo=? "
                    + " Update tl Set VendorCode=? From TDNLeg tl(NoLock) " // TDN Leg
                    + " Inner Join #tdnleg tt On tl.id=tt.id "
                    + " Delete tc From TDNCharge tc(NoLock) " // TDN Charge
                    + " Inner Join #tdnleg tl On tl.TDNRefNo = tc.TDNRefNo  "
                    + " Where tc.ChargeType='A' "
                    + " Select OperationDNo into #tmpops From OperationDetail(NoLock) " // Operations
                    + " Where BookNo=? And Opspol = ? "
                    + " Update od Set VendorCode=? From OperationDetail od(NoLock) "
                    + " Inner Join #tmpops t1 On t1.OperationDNo = od.OperationDNo "
                    + " Update EmptyWOReqMaster Set VendorCode=? Where MNRRefNo=? and nature<>'M' " // Workorder Master
                    + " Update em Set em.Vendor=vd.Vendor_Name From EmptyWOReqMaster em(Nolock) "
                    + " Inner Join VendorDetails vd(NoLock) On vd.vendor_code = em.vendorcode "
                    + " Where em.MNRRefNo=? And vd.Status='A' and nature<>'M' ";
            PreparedStatement pstUpVendorWOTDN = con.prepareStatement(sql);

            sql = " if object_id('tempdb..#tdnleg') is not null Drop Table #tdnleg ";
            PreparedStatement psttmpdrop_tl = con.prepareStatement(sql);

            /* -- Operations Load Planning (Local / TS) - Vendor Updation -- */
            // Booking
            sql = " Select OperationDNo Into #tmpops From OperationDetail(NoLock) "
                    + " Where (OperationDNo = ? Or PrevOperation = ?) "
                    + " Update od Set VendorCode=? From OperationDetail od(NoLock) "
                    + " Inner Join #tmpops On #tmpops.OperationDNo = od.OperationDNo ";
            PreparedStatement pstUp_LPVendor = con.prepareStatement(sql);

            sql = " Select OperationDNo Into #tmpopsfc From OperationDetail(NoLock) "
                    + " Where (OperationDNo = ? Or PrevOperation = ?) "
                    + " Update od set contractNo=?, Duration=?, Basis=? From OperationDetail od(NoLock) "
                    + " Inner Join #tmpopsfc On #tmpopsfc.OperationDNo = od.OperationDNo ";
            PreparedStatement pstUp_fdrCnt = con.prepareStatement(sql);

            // WorkOrder
            sql = " Select OperationDNo into #tmpops From OperationDetail(NoLock) "
                    + " Where BookNo=? And Opspol = ? "
                    + " Update od Set VendorCode=? From OperationDetail od(NoLock) "
                    + " Inner Join #tmpops t1 On t1.OperationDNo = od.OperationDNo ";
            PreparedStatement pstUp_LPVendorWO = con.prepareStatement(sql);

            sql = " Update EmptyWOReqMaster Set VendorCode=? Where MNRRefNo=? and nature<>'M' "
                    + " Update em Set em.Vendor=vd.Vendor_Name From EmptyWOReqMaster em(Nolock) "
                    + " Inner Join VendorDetails vd(NoLock) On vd.vendor_code = em.vendorcode "
                    + " Where em.MNRRefNo=? And vd.Status='A' and nature<>'M' ";
            PreparedStatement pstUp_VendorWO = con.prepareStatement(sql);

            sql = " Select OperationDNo Into #tmpopsfc From OperationDetail(NoLock) "
                    + " Where BookNo=? And Opspol = ? "
                    + " Update od set contractNo=?, Duration=?, Basis=? From OperationDetail od(NoLock) "
                    + " Inner Join #tmpopsfc On #tmpopsfc.OperationDNo = od.OperationDNo ";
            PreparedStatement pstUp_fdrCntWO = con.prepareStatement(sql);

            sql = " Select distinct contractno into #tmpc from fdrcontractdetail fd(NoLock) "
                    + " Where service=? "
                    + " And usetype <>'D' "
                    + " And status ='A' "
                    + " And approve='A' "
                    + " And vendorcode=? "
                    + " And loadPort=? "
                    + " And loadTerminal=? "
                    + " And dischport=? "
                    + " And dischterminal=? "
                    + " And termselect='Y' "
                    + " And convert(varchar(10),?,120) >= convert(varchar(10),validfrom,120) "
                    + " And convert(varchar(10),?,120) <= convert(varchar(10),validto,120) "
                    + " Select Count(contractno) as Count From #tmpc ";
            PreparedStatement pstChk_fdrCnt = con.prepareStatement(sql);

            sql = " Select distinct contractno,termtype as duration,usetype as basis "
                    + " From fdrcontractdetail fd(NoLock) "
                    + " Where service=? "
                    + " And usetype <>'D' "
                    + " And status ='A' "
                    + " And approve='A' "
                    + " And vendorcode=? "
                    + " And loadPort=? "
                    + " And loadTerminal=? "
                    + " And dischport=? "
                    + " And dischterminal=? "
                    + " And termselect='Y' "
                    + " And convert(varchar(10),?,120) >= convert(varchar(10),validfrom,120) "
                    + " And convert(varchar(10),?,120) <= convert(varchar(10),validto,120) "
                    + " Group By contractno, eqp,termtype, usetype , loadport, loadterminal, "
                    + " dischport, dischterminal,vendorcode, service "
                    + " Having Count(contractno)=1 ";
            PreparedStatement pstget_fdrcon = con.prepareStatement(sql);

            sql = "declare @id varchar(20) select @id=id from  emptyworeqcargo(Nolock) Where bookno=? and eqpid=? and status<>'V'  Update emptyworeqcargo set eqpid=?,isdummy='N',movetype='ACN' Where id=@id";

            PreparedStatement updateemptyworeqcargo = con.prepareStatement(sql);

            sql = " if object_id('tempdb..#tmpc') is not null Drop Table #tmpc ";
            PreparedStatement psttmpdrop_fdrv = con.prepareStatement(sql);

            sql = " if object_id('tempdb..#tmpopsfc') is not null Drop Table #tmpopsfc ";
            PreparedStatement psttmpdrop_opsfc = con.prepareStatement(sql);

            int transreworkcargocnt = 0;
            int transreworkcommoditycnt = 0;
            int bkgreferenceitncnt = 0;
            int blreferenceitncnt = 0;
            int mcnreferenceitncnt = 0;
            int opsgatereferencecnt = 0;
            int pcount = 0;
            int pcountwo = 0;
            int pcount1 = 0;
            int totalcount = 0;
            boolean stowagealonests = true;
            ResultSet blcargors = null;
            int blcount = 0;

            String OldContainerNo = "";
            String NewContainerNo = "";
            String BookNo = "";
            String reftype = "";
            ArrayList updateforecastlist = new ArrayList();
            ArrayList triggeredilist = new ArrayList();

            HashMap WoActEqpMap = new HashMap();
            HashMap WoNewEqpMap = new HashMap();

            boolean woexist = false;
            int bkgcargocnt = 0;
            int tpybkgcargocnt = 0;
            int bkgndgcnt = 0;
            int bkgndgcntDGValidationResponse = 0;
            int bkgndgcntDGValidationQ = 0;
            int bkgcargocnt1 = 0;
            int bkgcommoditycnt = 0;
            int tpybkgcommoditycnt = 0;
            int bkgimocnt = 0;
            int tpybkgimocnt = 0;

            int bkgchargecnt = 0;
            int bkgcontactscnt = 0;
            int tpybkgchargecnt = 0;
            int tpybkgcontactscnt = 0;

            int blgcargocnt = 0;
            int tpyblgcargocnt = 0;
            int blcargoupdate = 0;
            int blcargoupdate1 = 0;
            int blcargoupdatec = 0;
            int blgcommoditycnt = 0;
            int blgimocnt = 0;
            int blgcontactscnt = 0;
            int tpyblgcommoditycnt = 0;
            int tpyblgimocnt = 0;
            int tpyblgcontactscnt = 0;
            int updateforecast1count = 0;
            int updateforecast2count = 0;
            int bookinguncargo = 0;
            int bluncargo = 0;
            int rorodet = 0;
            int stuffbbrorodetail = 0;
            int opsupdatecnt = 0;
            int updatecount = 0;
            int tdnupdatecnt = 0;
            int opsstowageupcount = 0;
            int alreadyplannedinbk = 0;
            int alreadyplannedinops = 0;
            int alreadyplannedinbklocal = 0;
            int alreadyplannedinopslocal = 0;
            int tdnvendorcnt = 0;

            ArrayList distBkList = new ArrayList();
            ArrayList dubWOList = new ArrayList();

            int remove_overweightcharges = 0;
            int ops_ven_cnt = 0;
            int fdr_con_upcnt = 0;
            int cexists = 0;
            int Comcidcount = 0;
            int blComcidcount = 0;
            ResultSet bkgget_comcidcount = null;
            ResultSet blget_comcidcount = null;
            int ops_ven_cnt_gbl = 0;
            ResultSet rsFdr_ConChk = null;

            ResultSet rsGet_fdrcon = null;

            for (int i = 0; i < lcvo.contno.length; i++) {
                logger.info("Stowage Position Check in bean : " + lcvo.stowagePosition[i] + "actbookno" + lcvo.bkgno[i] + "lcvo.oldcontno[i]" + lcvo.oldcontno[i] + "lcvo.contno[i]" + lcvo.contno[i]);
                alreadyplannedinbklocal = 0;
                alreadyplannedinopslocal = 0;
                NewContainerNo = lcvo.contno[i];
                NewContainerNo = NewContainerNo != null ? NewContainerNo.trim() : "";

                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                    logger.info("-----ActualContainer Start of Containers-----");
                    try {
                        pcount = 0;
                        pcountwo = 0;
                        OldContainerNo = lcvo.oldcontno[i];
                        OldContainerNo = OldContainerNo != null ? OldContainerNo.trim() : "";

                        BookNo = lcvo.bkgno[i];
                        BookNo = BookNo != null ? BookNo.trim() : "";

                        reftype = lcvo.reftype[i];
                        reftype = reftype != null ? reftype.trim() : "";

                        boolean opsflag = true;
                        boolean saveflag = true;
                        con.setAutoCommit(false);

                        if ((!NewContainerNo.equalsIgnoreCase(OldContainerNo)) && !(NewContainerNo.equalsIgnoreCase(""))) {

                            if (reftype.equalsIgnoreCase("B")) {
                                this.pst1.setString(1, lcvo.svc[i]);
                                this.pst1.setString(2, lcvo.vsl[i]);
                                this.pst1.setString(3, lcvo.voy[i]);
                                this.pst1.setString(4, lcvo.bnd[i]);
                                this.pst1.setString(5, lcvo.contno[i]);

                                this.rs = this.pst1.executeQuery();
                                this.rs.next();
                                pcount = this.rs.getInt(1);

                                if ("true".equalsIgnoreCase(WCNplugin)) {
                                    this.pstwo.setString(1, lcvo.svc[i]);
                                    this.pstwo.setString(2, lcvo.vsl[i]);
                                    this.pstwo.setString(3, lcvo.voy[i]);
                                    this.pstwo.setString(4, lcvo.bnd[i]);
                                    this.pstwo.setString(5, BookNo);
                                    this.pstwo.setString(6, lcvo.contno[i]);
                                    this.pstwo.setString(7, lcvo.opspol[i]);
                                    this.pstwo.setString(8, lcvo.opspolter[i]);
                                    this.rs = this.pstwo.executeQuery();
                                    this.rs.next();
                                    pcountwo = this.rs.getInt(1);
                                }
                                this.logger.info("workorder-Container Already Planned pcountwo-->" + pcountwo);

                                if (pcount < 1 && pcountwo < 1) {
                                    this.pst2.setString(1, lcvo.svc[i]);
                                    this.pst2.setString(2, lcvo.vsl[i]);
                                    this.pst2.setString(3, lcvo.voy[i]);
                                    this.pst2.setString(4, lcvo.bnd[i]);
                                    this.pst2.setString(5, lcvo.contno[i]);
                                    this.rs = this.pst2.executeQuery();
                                    this.rs.next();
                                    pcount1 = this.rs.getInt(1);

                                    logger.info("Operation-Container Already Planned-->" + pcount1);

                                    if (pcount1 < 1) {
                                        pstbk_cargo_C.setString(1, BookNo);
                                        pstbk_cargo_C.setString(2, OldContainerNo);
                                        pstbk_cargo_C.setString(3, NewContainerNo);
                                        pstbk_cargo_C.setString(4, lcvo.type[i]);

                                        bkgcargocnt += pstbk_cargo_C.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {

                                            psttpybk_cargo_C.setString(1, BookNo);
                                            psttpybk_cargo_C.setString(2, OldContainerNo);
                                            psttpybk_cargo_C.setString(3, NewContainerNo);
                                            psttpybk_cargo_C.setString(4, lcvo.type[i]);
                                            tpybkgcargocnt += psttpybk_cargo_C.executeUpdate();
                                            logger.info("psttpybk_cargo_C query executed >>>>>>>>>>>>>>>" + tpybkgcargocnt);
                                            psttmpdrop.executeUpdate();
                                        }
                                        pstndg_C.setString(1, NewContainerNo);
                                        pstndg_C.setString(2, lcvo.type[i]);
                                        pstndg_C.setString(3, BookNo);
                                        pstndg_C.setString(4, OldContainerNo);
                                        bkgndgcnt += pstndg_C.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginDGVupdate)) {
                                            pstndg_CDGValidationResponse.setString(1, NewContainerNo);
                                            pstndg_CDGValidationResponse.setString(2, BookNo);
                                            pstndg_CDGValidationResponse.setString(3, OldContainerNo);
                                            bkgndgcntDGValidationResponse += pstndg_CDGValidationResponse.executeUpdate();

                                            pstndg_CDGValidationQ.setString(1, NewContainerNo);
                                            pstndg_CDGValidationQ.setString(2, BookNo);
                                            pstndg_CDGValidationQ.setString(3, OldContainerNo);
                                            bkgndgcntDGValidationQ += pstndg_CDGValidationQ.executeUpdate();
                                        }

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            pstbk_cargo_tareWtGWF.setString(1, BookNo);
                                            pstbk_cargo_tareWtGWF.setString(2, NewContainerNo);
                                            bkgcargocnt += pstbk_cargo_tareWtGWF.executeUpdate();
                                            //  logger.info("haulage query started>>>>>>>>>>>>>>>");
                                            pstbk_haulage_C.setString(1, BookNo);
                                            pstbk_haulage_C.setString(2, OldContainerNo);
                                            pstbk_haulage_C.setString(3, NewContainerNo);
                                            pstbk_haulage_C.setString(4, lcvo.type[i]);
                                            bkgimocnt += pstbk_haulage_C.executeUpdate();
                                            //    logger.info("haulage query executed >>>>>>>>>>>>>>>" + bkgimocnt);
                                            psttmpdrop.executeUpdate();

                                        } else {

                                            pstbk_cargo_tareWt.setString(1, BookNo);
                                            pstbk_cargo_tareWt.setString(2, NewContainerNo);
                                            bkgcargocnt += pstbk_cargo_tareWt.executeUpdate();
                                        }

                                        pstbk_com_C.setString(1, BookNo);
                                        pstbk_com_C.setString(2, OldContainerNo);
                                        pstbk_com_C.setString(3, NewContainerNo);
                                        bkgcommoditycnt += pstbk_com_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            psttpybk_com_C.setString(1, BookNo);
                                            psttpybk_com_C.setString(2, OldContainerNo);
                                            psttpybk_com_C.setString(3, NewContainerNo);
                                            tpybkgcommoditycnt += psttpybk_com_C.executeUpdate();
                                            logger.info("psttpybk_com_C query executed >>>>>>>>>>>>>>>" + tpybkgcommoditycnt);
                                            psttmpdrop.executeUpdate();

                                        }

                                        pstbk_imo_C.setString(1, BookNo);
                                        pstbk_imo_C.setString(2, OldContainerNo);
                                        pstbk_imo_C.setString(3, NewContainerNo);
                                        pstbk_imo_C.setString(4, lcvo.type[i]);
                                        bkgimocnt += pstbk_imo_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            psttpybk_imo_C.setString(1, BookNo);
                                            psttpybk_imo_C.setString(2, OldContainerNo);
                                            psttpybk_imo_C.setString(3, NewContainerNo);
                                            psttpybk_imo_C.setString(4, lcvo.type[i]);
                                            tpybkgimocnt += psttpybk_imo_C.executeUpdate();
                                            logger.info("psttpybk_imo_C query executed >>>>>>>>>>>>>>>" + tpybkgimocnt);
                                            psttmpdrop.executeUpdate();

                                        }

                                        pstbk_charge_C.setString(1, BookNo);
                                        pstbk_charge_C.setString(2, OldContainerNo);
                                        pstbk_charge_C.setString(3, NewContainerNo);
                                        pstbk_charge_C.setString(4, lcvo.type[i]);
                                        bkgchargecnt += pstbk_charge_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            psttpybk_charge_C.setString(1, BookNo);
                                            psttpybk_charge_C.setString(2, OldContainerNo);
                                            psttpybk_charge_C.setString(3, NewContainerNo);
                                            psttpybk_charge_C.setString(4, lcvo.type[i]);
                                            tpybkgchargecnt += psttpybk_charge_C.executeUpdate();
                                            psttmpdrop.executeUpdate();
                                        }

                                        pstbk_contact_C.setString(1, BookNo);
                                        pstbk_contact_C.setString(2, OldContainerNo);
                                        pstbk_contact_C.setString(3, NewContainerNo);
                                        bkgcontactscnt += pstbk_contact_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            psttpybk_contact_C.setString(1, BookNo);
                                            psttpybk_contact_C.setString(2, OldContainerNo);
                                            psttpybk_contact_C.setString(3, NewContainerNo);
                                            tpybkgcontactscnt += psttpybk_contact_C.executeUpdate();
                                            psttmpdrop.executeUpdate();

                                        }

                                        pstbl_cargo_C.setString(1, BookNo);
                                        pstbl_cargo_C.setString(2, OldContainerNo);
                                        pstbl_cargo_C.setString(3, NewContainerNo);
                                        pstbl_cargo_C.setString(4, lcvo.type[i]);

                                        blgcargocnt += pstbl_cargo_C.executeUpdate();
                                        psttmpdrop.executeUpdate();
                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            psttpybl_cargo_C.setString(1, BookNo);
                                            psttpybl_cargo_C.setString(2, OldContainerNo);
                                            psttpybl_cargo_C.setString(3, NewContainerNo);
                                            psttpybl_cargo_C.setString(4, lcvo.type[i]);

                                            tpyblgcargocnt += psttpybl_cargo_C.executeUpdate();

                                            logger.info("psttpybl_cargo_C query executed >>>>>>>>>>>>>>>" + tpyblgcargocnt);
                                            psttmpdrop.executeUpdate();

                                            pstbl_cargo_tareWtGWF.setString(1, BookNo);
                                            pstbl_cargo_tareWtGWF.setString(2, NewContainerNo);
                                            blgcargocnt += pstbl_cargo_tareWtGWF.executeUpdate();
                                            psttmpdrop.executeUpdate();
                                        } else {
                                            pstbl_cargo_tareWt.setString(1, BookNo);
                                            pstbl_cargo_tareWt.setString(2, NewContainerNo);
                                            blgcargocnt += pstbl_cargo_tareWt.executeUpdate();
                                            psttmpdrop.executeUpdate();
                                        }
                                        pstbl_com_C.setString(1, BookNo);
                                        pstbl_com_C.setString(2, OldContainerNo);
                                        pstbl_com_C.setString(3, NewContainerNo);
                                        blgcommoditycnt += pstbl_com_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        pstbl_imo_C.setString(1, BookNo);
                                        pstbl_imo_C.setString(2, OldContainerNo);
                                        pstbl_imo_C.setString(3, NewContainerNo);
                                        pstbl_imo_C.setString(4, lcvo.type[i]);
                                        blgimocnt += pstbl_imo_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        pstbl_contact_C.setString(1, BookNo);
                                        pstbl_contact_C.setString(2, OldContainerNo);
                                        pstbl_contact_C.setString(3, NewContainerNo);
                                        blgcontactscnt += pstbl_contact_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        if ("true".equalsIgnoreCase(pluginRRRO)) {
                                            psttpybl_com_C.setString(1, BookNo);
                                            psttpybl_com_C.setString(2, OldContainerNo);
                                            psttpybl_com_C.setString(3, NewContainerNo);
                                            tpyblgcommoditycnt += psttpybl_com_C.executeUpdate();
                                            psttmpdrop.executeUpdate();

                                            psttpybl_imo_C.setString(1, BookNo);
                                            psttpybl_imo_C.setString(2, OldContainerNo);
                                            psttpybl_imo_C.setString(3, NewContainerNo);
                                            psttpybl_imo_C.setString(4, lcvo.type[i]);
                                            tpyblgimocnt += psttpybl_imo_C.executeUpdate();
                                            psttmpdrop.executeUpdate();

                                            psttpybl_contact_C.setString(1, BookNo);
                                            psttpybl_contact_C.setString(2, OldContainerNo);
                                            psttpybl_contact_C.setString(3, NewContainerNo);
                                            tpyblgcontactscnt += psttpybl_contact_C.executeUpdate();
                                            psttmpdrop.executeUpdate();

                                        }

                                        pstops.setString(1, BookNo);
                                        pstops.setString(2, OldContainerNo);
                                        pstops.setString(3, NewContainerNo);
                                        pstops.setString(4, lcvo.type[i]);
                                        pstops.setString(5, "Y");
                                        opsupdatecnt += pstops.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        psttdn.setString(1, BookNo);
                                        psttdn.setString(2, OldContainerNo);
                                        psttdn.setString(3, NewContainerNo);
                                        psttdn.setString(4, lcvo.type[i]);
                                        tdnupdatecnt += psttdn.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        psttransrework_cargo.setString(1, BookNo);
                                        psttransrework_cargo.setString(2, OldContainerNo);
                                        psttransrework_cargo.setString(3, NewContainerNo);
                                        psttransrework_cargo.setString(4, lcvo.type[i]);

                                        transreworkcargocnt += psttransrework_cargo.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        psttransrework_commodity.setString(1, BookNo);
                                        psttransrework_commodity.setString(2, OldContainerNo);
                                        psttransrework_commodity.setString(3, NewContainerNo);
                                        psttransrework_commodity.setString(4, lcvo.type[i]);
                                        transreworkcommoditycnt += psttransrework_commodity.executeUpdate();
                                        psttmpdrop.executeUpdate();
                                    } else {
                                        saveflag = false;
                                        Exceptionmap.put(lcvo.rowID[i], "P");
                                        alreadyplannedinops++;
                                        alreadyplannedinopslocal++;
                                    }
                                } else {
                                    saveflag = false;
                                    Exceptionmap.put(lcvo.rowID[i], "P");
                                    alreadyplannedinbk++;
                                    alreadyplannedinbklocal++;
                                }

                            } else if (reftype.equalsIgnoreCase("W")) {
                                this.pst2.setString(1, lcvo.svc[i]);
                                this.pst2.setString(2, lcvo.vsl[i]);
                                this.pst2.setString(3, lcvo.voy[i]);
                                this.pst2.setString(4, lcvo.bnd[i]);
                                this.pst2.setString(5, lcvo.contno[i]);
                                this.rs = this.pst2.executeQuery();
                                this.rs.next();
                                pcount = this.rs.getInt(1);

                                if ("true".equalsIgnoreCase(WCNplugin)) {
                                    this.pstwo.setString(1, lcvo.svc[i]);
                                    this.pstwo.setString(2, lcvo.vsl[i]);
                                    this.pstwo.setString(3, lcvo.voy[i]);
                                    this.pstwo.setString(4, lcvo.bnd[i]);
                                    this.pstwo.setString(5, BookNo);
                                    this.pstwo.setString(6, lcvo.contno[i]);
                                    this.pstwo.setString(7, lcvo.opspol[i]);
                                    this.pstwo.setString(8, lcvo.opspolter[i]);
                                    this.rs = this.pstwo.executeQuery();
                                    this.rs.next();
                                    pcountwo = this.rs.getInt(1);
                                }
                                this.logger.info("workorder-Container Already Planned pcountwo-->" + pcountwo);

                                if (pcount < 1 && pcountwo < 1) {
                                    pstbl_cargo_C.setString(1, BookNo);
                                    pstbl_cargo_C.setString(2, OldContainerNo);
                                    pstbl_cargo_C.setString(3, NewContainerNo);
                                    pstbl_cargo_C.setString(4, lcvo.type[i]);

                                    blgcargocnt += pstbl_cargo_C.executeUpdate();
                                    psttmpdrop.executeUpdate();
                                    if ("true".equalsIgnoreCase(pluginRRRO)) {

                                        psttpybl_cargo_C.setString(1, BookNo);
                                        psttpybl_cargo_C.setString(2, OldContainerNo);
                                        psttpybl_cargo_C.setString(3, NewContainerNo);
                                        psttpybl_cargo_C.setString(4, lcvo.type[i]);

                                        tpyblgcargocnt += psttpybl_cargo_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        pstbl_cargo_tareWtGWF.setString(1, BookNo);
                                        pstbl_cargo_tareWtGWF.setString(2, NewContainerNo);
                                        blgcargocnt += pstbl_cargo_tareWtGWF.executeUpdate();
                                        psttmpdrop.executeUpdate();
                                    } else {

                                        pstbl_cargo_tareWt.setString(1, BookNo);
                                        pstbl_cargo_tareWt.setString(2, NewContainerNo);
                                        blgcargocnt += pstbl_cargo_tareWt.executeUpdate();
                                        psttmpdrop.executeUpdate();
                                    }

                                    pstbl_com_C.setString(1, BookNo);
                                    pstbl_com_C.setString(2, OldContainerNo);
                                    pstbl_com_C.setString(3, NewContainerNo);
                                    blgcommoditycnt += pstbl_com_C.executeUpdate();
                                    psttmpdrop.executeUpdate();

                                    pstbl_imo_C.setString(1, BookNo);
                                    pstbl_imo_C.setString(2, OldContainerNo);
                                    pstbl_imo_C.setString(3, NewContainerNo);
                                    pstbl_imo_C.setString(4, lcvo.type[i]);
                                    blgimocnt += pstbl_imo_C.executeUpdate();
                                    psttmpdrop.executeUpdate();

                                    pstbl_contact_C.setString(1, BookNo);
                                    pstbl_contact_C.setString(2, OldContainerNo);
                                    pstbl_contact_C.setString(3, NewContainerNo);
                                    blgcontactscnt += pstbl_contact_C.executeUpdate();
                                    psttmpdrop.executeUpdate();

                                    if ("true".equalsIgnoreCase(pluginRRRO)) {
                                        psttpybl_com_C.setString(1, BookNo);
                                        psttpybl_com_C.setString(2, OldContainerNo);
                                        psttpybl_com_C.setString(3, NewContainerNo);
                                        tpyblgcommoditycnt += psttpybl_com_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        psttpybl_imo_C.setString(1, BookNo);
                                        psttpybl_imo_C.setString(2, OldContainerNo);
                                        psttpybl_imo_C.setString(3, NewContainerNo);
                                        psttpybl_imo_C.setString(4, lcvo.type[i]);
                                        tpyblgimocnt += psttpybl_imo_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                        psttpybl_contact_C.setString(1, BookNo);
                                        psttpybl_contact_C.setString(2, OldContainerNo);
                                        psttpybl_contact_C.setString(3, NewContainerNo);
                                        tpyblgcontactscnt += psttpybl_contact_C.executeUpdate();
                                        psttmpdrop.executeUpdate();

                                    }

                                    pstops.setString(1, BookNo);
                                    pstops.setString(2, OldContainerNo);
                                    pstops.setString(3, NewContainerNo);
                                    pstops.setString(4, lcvo.type[i]);
                                    pstops.setString(5, "Y");
                                    opsupdatecnt += pstops.executeUpdate();
                                    psttmpdrop.executeUpdate();

                                    if ("true".equalsIgnoreCase(WCNplugin)) {
                                        updateemptyworeqcargo.setString(1, BookNo);
                                        updateemptyworeqcargo.setString(2, OldContainerNo);
                                        updateemptyworeqcargo.setString(3, NewContainerNo);
                                        updatecount += updateemptyworeqcargo.executeUpdate();
                                    }

                                    psttdn.setString(1, BookNo);
                                    psttdn.setString(2, OldContainerNo);
                                    psttdn.setString(3, NewContainerNo);
                                    psttdn.setString(4, lcvo.type[i]);
                                    tdnupdatecnt += psttdn.executeUpdate();
                                    psttmpdrop.executeUpdate();
                                } else {
                                    saveflag = false;
                                    Exceptionmap.put(lcvo.rowID[i], "P");
                                    alreadyplannedinops++;
                                }
                            }

                            if (opsflag == true) {
                                if ((saveflag) && (reftype.equalsIgnoreCase("W")) && (!lcvo.actEqpType[i].equalsIgnoreCase(lcvo.type[i]))) {
                                    woexist = true;
                                    int woactcount = 1;
                                    if (WoActEqpMap.get(BookNo + "~" + lcvo.actEqpType[i]) != null) {
                                        woactcount = Integer.parseInt(WoActEqpMap.get(BookNo + "~" + lcvo.actEqpType[i]).toString());
                                        woactcount++;
                                    }
                                    WoActEqpMap.put(BookNo + "~" + lcvo.actEqpType[i], String.valueOf(woactcount));
                                    int wonewcount = 1;
                                    if (WoNewEqpMap.get(BookNo + "~" + lcvo.type[i]) != null) {
                                        wonewcount = Integer.parseInt(WoNewEqpMap.get(BookNo + "~" + lcvo.type[i]).toString());
                                        wonewcount++;
                                    }
                                    WoNewEqpMap.put(BookNo + "~" + lcvo.type[i], String.valueOf(wonewcount));
                                }
                                con.commit();
                                totalcount++;
                            }
                        }
                    } catch (SQLException E) {
                        E.printStackTrace();
                        logger.info("-->Duplicate ContaierNo exception handled");
                        logger.info("Error code-->" + E.getErrorCode());
                        if (E.getErrorCode() == 2601) {
                            Exceptionmap.put(lcvo.rowID[i], "D");
                        } else if (E.getErrorCode() == 2627) {
                            Exceptionmap.put(lcvo.rowID[i], "D");
                        } else {
                            Exceptionmap.put(lcvo.rowID[i], "Others");
                        }
                        try {
                            logger.info("-->Roll back Start for SQLException");

                            con.rollback();
                            logger.info("-->Roll back End in SQLException");
                        } catch (Exception e2) {
                            logger.info("Exception in roll back at SQLException-->" + e2);
                            e2.printStackTrace();
                        }
                    } catch (Exception e1) {
                        logger.info("Exception in Containers -->" + e1);
                        e1.printStackTrace();
                        Exceptionmap.put(lcvo.rowID[i], "Others");
                        try {
                            logger.info("-->Roll back Start for Exception");

                            con.rollback();
                            logger.info("-->Roll back End in Exception ");
                        } catch (Exception e2) {
                            logger.info("Exception in roll back at Exception-->" + e2);
                            e2.printStackTrace();
                        }
                    }
                    logger.info("-----ActualContainer End of Containers-----");
                }

                if (lcvo.updationOn.contains("S") && !lcvo.updationOn.contains("SO")) {
                    try {
                        con.setAutoCommit(false);
                        logger.info("-----ActualContainer Start of Stowage/Weight/Seal-----");

                        if ("true".equalsIgnoreCase(pluginRRRO)) {

                            pstbk_cargo_SGWF.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_cargo_SGWF.setString(2, NewContainerNo);
                            } else {
                                pstbk_cargo_SGWF.setString(2, lcvo.oldcontno[i]);
                            }

                            pstbk_cargo_SGWF.setString(3, lcvo.cargoWt[i]);
                            pstbk_cargo_SGWF.setString(4, lcvo.cargoWt[i]);
                            pstbk_cargo_SGWF.setString(5, lcvo.weightunit[i]);
                            bkgcargocnt += pstbk_cargo_SGWF.executeUpdate();

                            pstbk_com_CWcount.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_com_CWcount.setString(2, NewContainerNo);
                            } else {
                                pstbk_com_CWcount.setString(2, lcvo.oldcontno[i]);
                            }
                            bkgget_comcidcount = pstbk_com_CWcount.executeQuery();
                            if (bkgget_comcidcount.next()) {
                                Comcidcount = bkgget_comcidcount.getInt("count");
                                if (Comcidcount == 1) {
                                    pstbk_com_CW.setString(1, lcvo.bkgno[i]);
                                    if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                        pstbk_com_CW.setString(2, NewContainerNo);
                                    } else {
                                        pstbk_com_CW.setString(2, lcvo.oldcontno[i]);
                                    }

                                    pstbk_com_CW.setString(3, lcvo.cargoWt[i]);
                                    bkgcargocnt += pstbk_com_CW.executeUpdate();

                                }
                            }

                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbk_cargo_S1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S1.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S1.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S1.setString(3, lcvo.seal[i]);
                                logger.info("lcvo.carrierseal bk only : " + lcvo.seal[i]);

                                bkgcargocnt1 += pstbk_cargo_S1.executeUpdate();
                            }
                            if (lcvo.shipperseal[i] != null && lcvo.shipperseal[i].length() > 0) {
                                pstbk_cargo_S2.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S2.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S2.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S2.setString(3, lcvo.shipperseal[i]);
                                logger.info("lcvo.shipperseal bk only : " + lcvo.shipperseal[i]);

                                bkgcargocnt1 += pstbk_cargo_S2.executeUpdate();
                            }
                            if (lcvo.otherseal[i] != null && lcvo.otherseal[i].length() > 0) {
                                pstbk_cargo_S3.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S3.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S3.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S3.setString(3, lcvo.otherseal[i]);
                                logger.info("lcvo.otherseal bk only : " + lcvo.otherseal[i]);

                                bkgcargocnt1 += pstbk_cargo_S3.executeUpdate();
                            }
                        } else {

                            pstbk_cargo_S.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_cargo_S.setString(2, NewContainerNo);
                            } else {
                                pstbk_cargo_S.setString(2, lcvo.oldcontno[i]);
                            }

                            pstbk_cargo_S.setString(3, lcvo.cargoWt[i]);
                            pstbk_cargo_S.setString(4, lcvo.cargoWt[i]);
                            bkgcargocnt += pstbk_cargo_S.executeUpdate();
                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbk_cargo_S1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S1.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S1.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S1.setString(3, lcvo.seal[i]);
                                logger.info("lcvo.seal bk only : " + lcvo.seal[i]);

                                bkgcargocnt1 += pstbk_cargo_S1.executeUpdate();
                            }

                        }
                        logger.info("bkgcargocnt1 : " + bkgcargocnt1);
                        logger.info("bkgndgcnt : " + bkgndgcnt);
                        logger.info("bkgndgcntDGValidationResponse : " + bkgndgcntDGValidationResponse);
                        logger.info("bkgndgcntDGValidationQ : " + bkgndgcntDGValidationQ);

                        pstopssp.setString(1, lcvo.stowagePosition[i]);
                        pstopssp.setString(2, lcvo.operationNum[i]);
                        logger.info("Stowage Position Check in bean : " + lcvo.stowagePosition[i]);
                        logger.info("operationNum Check in bean : " + lcvo.operationNum[i]);
                        opsstowageupcount += pstopssp.executeUpdate();
                        con.commit();
                        logger.info("-----ActualContainer End of Stowage/Weight/Seal-----");
                    } catch (Exception e) {
                        stowagealonests = false;
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                        con.rollback();
                        logger.info("----Roll Back Called---");
                    }

                }

                if (lcvo.updationOn.contains("V")) { // Vendor Updation Revised For CR-675 Changes.
                    try {
                        logger.info("----- ActualContainer Start of Vendor -----");
                        con.setAutoCommit(false);

                        logger.info("Check the updateby : " + lcvo.updateby);
                        if (lcvo.updateby.equalsIgnoreCase("TDN")) {
                            logger.info("Check the reftype : " + lcvo.reftype[i]);
                            // UpdasUpdateBytion in TDN, Operations and TDNCharge
                            logger.info("Check the bkgno : " + lcvo.bkgno[i]);
                            if (lcvo.reftype[i].equalsIgnoreCase("B")) {
                                pstUpVendorTDN.setString(1, lcvo.tdnRefNo[i]);
                                pstUpVendorTDN.setString(2, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstUpVendorTDN.setString(3, NewContainerNo);
                                } else {
                                    pstUpVendorTDN.setString(3, lcvo.oldcontno[i]);
                                }
                                logger.info("Check the tdnRefNo : " + lcvo.tdnRefNo[i]);
                                logger.info("Check the vendor : " + lcvo.vendor[i]);
                                pstUpVendorTDN.setString(4, lcvo.vendor[i]);
                                pstUpVendorTDN.setString(5, lcvo.vendor[i]);
                                pstUpVendorTDN.setString(6, lcvo.tdnRefNo[i]);
                                pstUpVendorTDN.setString(7, lcvo.tdnRefNo[i]);
                                tdnvendorcnt = pstUpVendorTDN.executeUpdate();
                                logger.info("Check the tdnvendorcnt : " + tdnvendorcnt);
                                psttmpdrop_ops.executeUpdate();
                            } else if (lcvo.reftype[i].equalsIgnoreCase("W")) {
                                if (!dubWOList.contains(lcvo.bkgno[i])) {
                                    dubWOList.add(lcvo.bkgno[i]);
                                    pstUpVendorWOTDN.setString(1, lcvo.bkgno[i]);
                                    pstUpVendorWOTDN.setString(2, lcvo.vendor[i]);
                                    pstUpVendorWOTDN.setString(3, lcvo.bkgno[i]);
                                    pstUpVendorWOTDN.setString(4, lcvo.opspol[i]);
                                    pstUpVendorWOTDN.setString(5, lcvo.vendor[i]);
                                    pstUpVendorWOTDN.setString(6, lcvo.vendor[i]);
                                    pstUpVendorWOTDN.setString(7, lcvo.bkgno[i]);
                                    pstUpVendorWOTDN.setString(8, lcvo.bkgno[i]);
                                    tdnvendorcnt = pstUpVendorWOTDN.executeUpdate();
                                    psttmpdrop_tl.executeUpdate();
                                    psttmpdrop_ops.executeUpdate();
                                }
                            }
                            tdnvendorcnt++;
                        } else if (lcvo.updateby.equalsIgnoreCase("LP") || lcvo.updateby.equalsIgnoreCase("TS")) {
                            // Operations               
                            if (lcvo.reftype[i].equalsIgnoreCase("B")) {
                                pstUp_LPVendor.setString(1, lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(2, lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(3, lcvo.vendor[i]);
                                ops_ven_cnt = pstUp_LPVendor.executeUpdate();
                                psttmpdrop_ops.executeUpdate();
                                logger.info("ops_ven_cnt : " + ops_ven_cnt);
                                if (ops_ven_cnt == 1) {
                                    ops_ven_cnt_gbl++;
                                    // Fdr Contract Terms Validation
                                    pstChk_fdrCnt.setString(1, lcvo.svc[i]);
                                    pstChk_fdrCnt.setString(2, lcvo.vendor[i]);
                                    pstChk_fdrCnt.setString(3, lcvo.opspol[i]);
                                    pstChk_fdrCnt.setString(4, lcvo.opspolter[i]);
                                    pstChk_fdrCnt.setString(5, lcvo.opspod[i]);
                                    pstChk_fdrCnt.setString(6, lcvo.opspodter[i]);
                                    pstChk_fdrCnt.setString(7, lcvo.arrDate[i]);
                                    pstChk_fdrCnt.setString(8, lcvo.arrDate[i]);
                                    rsFdr_ConChk = pstChk_fdrCnt.executeQuery();
                                    psttmpdrop_fdrv.executeUpdate();
                                    if (rsFdr_ConChk.next()) {
                                        cexists = rsFdr_ConChk.getInt("Count");
                                        logger.info("contract exists : " + cexists);
                                        if (cexists == 1) {
                                            pstget_fdrcon.setString(1, lcvo.svc[i]);
                                            pstget_fdrcon.setString(2, lcvo.vendor[i]);
                                            pstget_fdrcon.setString(3, lcvo.opspol[i]);
                                            pstget_fdrcon.setString(4, lcvo.opspolter[i]);
                                            pstget_fdrcon.setString(5, lcvo.opspod[i]);
                                            pstget_fdrcon.setString(6, lcvo.opspodter[i]);
                                            pstget_fdrcon.setString(7, lcvo.arrDate[i]);
                                            pstget_fdrcon.setString(8, lcvo.arrDate[i]);
                                            rsGet_fdrcon = pstget_fdrcon.executeQuery();
                                            if (rsGet_fdrcon.next()) { // Fdr Contract Terms Updation
                                                pstUp_fdrCnt.setString(1, lcvo.operationNum[i]);
                                                pstUp_fdrCnt.setString(2, lcvo.operationNum[i]);
                                                pstUp_fdrCnt.setString(3, rsGet_fdrcon.getString("contractno"));
                                                pstUp_fdrCnt.setString(4, rsGet_fdrcon.getString("duration"));
                                                pstUp_fdrCnt.setString(5, rsGet_fdrcon.getString("basis"));
                                                fdr_con_upcnt = pstUp_fdrCnt.executeUpdate();
                                                logger.info("Operations Feeder Contract Terms Update Count : " + fdr_con_upcnt);
                                                psttmpdrop_opsfc.executeUpdate();
                                            }
                                        } else {
                                            pstUp_fdrCnt.setString(1, lcvo.operationNum[i]);
                                            pstUp_fdrCnt.setString(2, lcvo.operationNum[i]);
                                            pstUp_fdrCnt.setString(3, "");
                                            pstUp_fdrCnt.setString(4, "");
                                            pstUp_fdrCnt.setString(5, "");
                                            fdr_con_upcnt = pstUp_fdrCnt.executeUpdate();
                                            logger.info("Operations Feeder Contract Terms Empty 1 Count : " + fdr_con_upcnt);
                                            psttmpdrop_opsfc.executeUpdate();
                                        }
                                    }
                                }
                            } else if (lcvo.reftype[i].equalsIgnoreCase("W")) {
                                if (!dubWOList.contains(lcvo.bkgno[i])) {
                                    dubWOList.add(lcvo.bkgno[i]);
                                    pstUp_LPVendorWO.setString(1, lcvo.bkgno[i]);
                                    pstUp_LPVendorWO.setString(2, lcvo.opspol[i]);
                                    pstUp_LPVendorWO.setString(3, lcvo.vendor[i]);
                                    ops_ven_cnt = pstUp_LPVendorWO.executeUpdate();
                                    psttmpdrop_ops.executeUpdate();
                                    logger.info("CargoType : " + lcvo.cargotype[i]);
                                    logger.info("ops_ven_cnt : " + ops_ven_cnt);
                                    if (ops_ven_cnt >= 1) {
                                        if (lcvo.cargotype[i].equalsIgnoreCase("Local")) {
                                            pstUp_VendorWO.setString(1, lcvo.vendor[i]);
                                            pstUp_VendorWO.setString(2, lcvo.bkgno[i]);
                                            pstUp_VendorWO.setString(3, lcvo.bkgno[i]);
                                            int woupcnt = pstUp_VendorWO.executeUpdate();
                                            logger.info("Vendor Update in WorkOrder : " + lcvo.bkgno[i] + " Count : " + woupcnt);
                                        }
                                        ops_ven_cnt_gbl++;
                                        // Fdr Contract Terms Validation
                                        pstChk_fdrCnt.setString(1, lcvo.svc[i]);
                                        pstChk_fdrCnt.setString(2, lcvo.vendor[i]);
                                        pstChk_fdrCnt.setString(3, lcvo.opspol[i]);
                                        pstChk_fdrCnt.setString(4, lcvo.opspolter[i]);
                                        pstChk_fdrCnt.setString(5, lcvo.opspod[i]);
                                        pstChk_fdrCnt.setString(6, lcvo.opspodter[i]);
                                        pstChk_fdrCnt.setString(7, lcvo.arrDate[i]);
                                        pstChk_fdrCnt.setString(8, lcvo.arrDate[i]);
                                        rsFdr_ConChk = pstChk_fdrCnt.executeQuery();
                                        psttmpdrop_fdrv.executeUpdate();
                                        if (rsFdr_ConChk.next()) {
                                            cexists = rsFdr_ConChk.getInt("Count");
                                            logger.info("contract exists : " + cexists);
                                            if (cexists == 1) {
                                                pstget_fdrcon.setString(1, lcvo.svc[i]);
                                                pstget_fdrcon.setString(2, lcvo.vendor[i]);
                                                pstget_fdrcon.setString(3, lcvo.opspol[i]);
                                                pstget_fdrcon.setString(4, lcvo.opspolter[i]);
                                                pstget_fdrcon.setString(5, lcvo.opspod[i]);
                                                pstget_fdrcon.setString(6, lcvo.opspodter[i]);
                                                pstget_fdrcon.setString(7, lcvo.arrDate[i]);
                                                pstget_fdrcon.setString(8, lcvo.arrDate[i]);
                                                rsGet_fdrcon = pstget_fdrcon.executeQuery();
                                                if (rsGet_fdrcon.next()) { // Fdr Contract Terms Updation
                                                    pstUp_fdrCntWO.setString(1, lcvo.bkgno[i]);
                                                    pstUp_fdrCntWO.setString(2, lcvo.opspol[i]);
                                                    pstUp_fdrCntWO.setString(3, rsGet_fdrcon.getString("contractno"));
                                                    pstUp_fdrCntWO.setString(4, rsGet_fdrcon.getString("duration"));
                                                    pstUp_fdrCntWO.setString(5, rsGet_fdrcon.getString("basis"));
                                                    fdr_con_upcnt = pstUp_fdrCntWO.executeUpdate();
                                                    logger.info("Operations(WO) Feeder Contract Terms Update Count : " + fdr_con_upcnt);
                                                    psttmpdrop_opsfc.executeUpdate();
                                                }
                                            } else {
                                                pstUp_fdrCntWO.setString(1, lcvo.bkgno[i]);
                                                pstUp_fdrCntWO.setString(2, lcvo.opspol[i]);
                                                pstUp_fdrCntWO.setString(3, "");
                                                pstUp_fdrCntWO.setString(4, "");
                                                pstUp_fdrCntWO.setString(5, "");
                                                fdr_con_upcnt = pstUp_fdrCntWO.executeUpdate();
                                                logger.info("Operations(WO) Feeder Contract Terms Empty 1 Count : " + fdr_con_upcnt);
                                                psttmpdrop_opsfc.executeUpdate();
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (lcvo.updateby.equalsIgnoreCase("B")) {
                            logger.info("Vendor Update For Un Planned / Booking Records - " + lcvo.bkgno[i]);
                            // if (lcvo.Haulagetype[i].equalsIgnoreCase("M")) {
                            pstbk_cargo_V.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_cargo_V.setString(2, NewContainerNo);
                            } else {
                                pstbk_cargo_V.setString(2, lcvo.oldcontno[i]);
                            }
                            pstbk_cargo_V.setString(3, lcvo.vendor[i]);
                            bkgcargocnt += pstbk_cargo_V.executeUpdate();
                            // }
                        } else if (lcvo.updateby.equalsIgnoreCase("A")) {
                            // Repeat the above validations
                            logger.info("Check the Operation No : " + lcvo.operationNum[i]);
                            if (lcvo.operationNum[i].equalsIgnoreCase("0")) { // Un Planned Containers
                                logger.info("Vendor Update For Un Planned Records - " + lcvo.bkgno[i]);
                                pstbk_cargo_V.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_V.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_V.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_V.setString(3, lcvo.vendor[i]);
                                bkgcargocnt += pstbk_cargo_V.executeUpdate();
                            } else if (lcvo.aOpsSts[i].equalsIgnoreCase("Load Planned")) { // Operations Vendor & Feeder Contract Terms Update
                                logger.info("Vendor Update For Load Planned Records - " + lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(1, lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(2, lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(3, lcvo.vendor[i]);
                                ops_ven_cnt += pstUp_LPVendor.executeUpdate();
                                psttmpdrop_ops.executeUpdate();
                                logger.info("ops_ven_cnt : " + ops_ven_cnt);
                                if (ops_ven_cnt == 1) {
                                    ops_ven_cnt_gbl++;
                                    // Fdr Contract Terms Validation
                                    pstChk_fdrCnt.setString(1, lcvo.svc[i]);
                                    pstChk_fdrCnt.setString(2, lcvo.vendor[i]);
                                    pstChk_fdrCnt.setString(3, lcvo.opspol[i]);
                                    pstChk_fdrCnt.setString(4, lcvo.opspolter[i]);
                                    pstChk_fdrCnt.setString(5, lcvo.opspod[i]);
                                    pstChk_fdrCnt.setString(6, lcvo.opspodter[i]);
                                    pstChk_fdrCnt.setString(7, lcvo.arrDate[i]);
                                    pstChk_fdrCnt.setString(8, lcvo.arrDate[i]);
                                    rsFdr_ConChk = pstChk_fdrCnt.executeQuery();
                                    psttmpdrop_fdrv.executeUpdate();
                                    if (rsFdr_ConChk.next()) {
                                        cexists = rsFdr_ConChk.getInt("Count");
                                        logger.info("contract exists : " + cexists);
                                        if (cexists == 1) {
                                            pstget_fdrcon.setString(1, lcvo.svc[i]);
                                            pstget_fdrcon.setString(2, lcvo.vendor[i]);
                                            pstget_fdrcon.setString(3, lcvo.opspol[i]);
                                            pstget_fdrcon.setString(4, lcvo.opspolter[i]);
                                            pstget_fdrcon.setString(5, lcvo.opspod[i]);
                                            pstget_fdrcon.setString(6, lcvo.opspodter[i]);
                                            pstget_fdrcon.setString(7, lcvo.arrDate[i]);
                                            pstget_fdrcon.setString(8, lcvo.arrDate[i]);
                                            rsGet_fdrcon = pstget_fdrcon.executeQuery();
                                            if (rsGet_fdrcon.next()) { // Fdr Contract Terms Updation
                                                pstUp_fdrCnt.setString(1, lcvo.operationNum[i]);
                                                pstUp_fdrCnt.setString(2, lcvo.operationNum[i]);
                                                pstUp_fdrCnt.setString(3, rsGet_fdrcon.getString("contractno"));
                                                pstUp_fdrCnt.setString(4, rsGet_fdrcon.getString("duration"));
                                                pstUp_fdrCnt.setString(5, rsGet_fdrcon.getString("basis"));
                                                fdr_con_upcnt = pstUp_fdrCnt.executeUpdate();
                                                logger.info("Operations Feeder Contract Terms Update Count : " + fdr_con_upcnt);
                                                psttmpdrop_opsfc.executeUpdate();
                                            }
                                        } else {
                                            pstUp_fdrCnt.setString(1, lcvo.operationNum[i]);
                                            pstUp_fdrCnt.setString(2, lcvo.operationNum[i]);
                                            pstUp_fdrCnt.setString(3, "");
                                            pstUp_fdrCnt.setString(4, "");
                                            pstUp_fdrCnt.setString(5, "");
                                            fdr_con_upcnt = pstUp_fdrCnt.executeUpdate();
                                            logger.info("Operations Feeder Contract Terms Empty 1 Count : " + fdr_con_upcnt);
                                            psttmpdrop_opsfc.executeUpdate();
                                        }
                                    }
                                }
                            } else if (lcvo.aOpsSts[i].contains("Haulage")) { // Operations Vendor Alone
                                logger.info("Vendor Update For Haulage Planned / Confirmed Records - " + lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(1, lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(2, lcvo.operationNum[i]);
                                pstUp_LPVendor.setString(3, lcvo.vendor[i]);
                                ops_ven_cnt = pstUp_LPVendor.executeUpdate();
                                ops_ven_cnt_gbl++;
                                psttmpdrop_ops.executeUpdate();
                                logger.info("ops_ven_cnt : " + ops_ven_cnt);
                            }
                        }
                        con.commit();
                        logger.info("----- ActualContainer End of Vendor -----");
                    } catch (Exception e) {
                        stowagealonests = false;
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                        con.rollback();
                        logger.info("----Roll Back Called---");
                    }
                }
                if (lcvo.updationOn.contains("B")) {
                    try {
                        logger.info("----- ActualContainer Start of Update BL -----");
                        con.setAutoCommit(false);

                        if ("true".equalsIgnoreCase(pluginRRRO)) {

                            pstbk_cargo_SGWF.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_cargo_SGWF.setString(2, NewContainerNo);
                            } else {
                                pstbk_cargo_SGWF.setString(2, lcvo.oldcontno[i]);
                            }

                            pstbk_cargo_SGWF.setString(3, lcvo.cargoWt[i]);
                            pstbk_cargo_SGWF.setString(4, lcvo.cargoWt[i]);
                            pstbk_cargo_SGWF.setString(5, lcvo.weightunit[i]);
                            bkgcargocnt += pstbk_cargo_SGWF.executeUpdate();

                            pstbk_com_CWcount.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_com_CWcount.setString(2, NewContainerNo);
                            } else {
                                pstbk_com_CWcount.setString(2, lcvo.oldcontno[i]);
                            }
                            bkgget_comcidcount = pstbk_com_CWcount.executeQuery();
                            if (bkgget_comcidcount.next()) {
                                Comcidcount = bkgget_comcidcount.getInt("count");
                                if (Comcidcount == 1) {
                                    pstbk_com_CW.setString(1, lcvo.bkgno[i]);
                                    if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                        pstbk_com_CW.setString(2, NewContainerNo);
                                    } else {
                                        pstbk_com_CW.setString(2, lcvo.oldcontno[i]);
                                    }

                                    pstbk_com_CW.setString(3, lcvo.cargoWt[i]);
                                    bkgcargocnt += pstbk_com_CW.executeUpdate();

                                }
                            }

                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbk_cargo_S1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S1.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S1.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S1.setString(3, lcvo.seal[i]);
                                logger.info("lcvo.carrierseal bk only : " + lcvo.seal[i]);

                                bkgcargocnt1 += pstbk_cargo_S1.executeUpdate();
                            }
                            if (lcvo.shipperseal[i] != null && lcvo.shipperseal[i].length() > 0) {
                                pstbk_cargo_S2.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S2.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S2.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S2.setString(3, lcvo.shipperseal[i]);
                                logger.info("lcvo.shipperseal bk only : " + lcvo.shipperseal[i]);

                                bkgcargocnt1 += pstbk_cargo_S2.executeUpdate();
                            }
                            if (lcvo.otherseal[i] != null && lcvo.otherseal[i].length() > 0) {
                                pstbk_cargo_S3.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S3.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S3.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S3.setString(3, lcvo.otherseal[i]);
                                logger.info("lcvo.otherseal bk only : " + lcvo.otherseal[i]);

                                bkgcargocnt1 += pstbk_cargo_S3.executeUpdate();
                            }
                        } else {

                            pstbk_cargo_S.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbk_cargo_S.setString(2, NewContainerNo);
                            } else {
                                pstbk_cargo_S.setString(2, lcvo.oldcontno[i]);
                            }

                            pstbk_cargo_S.setString(3, lcvo.cargoWt[i]);
                            pstbk_cargo_S.setString(4, lcvo.cargoWt[i]);
                            bkgcargocnt += pstbk_cargo_S.executeUpdate();
                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbk_cargo_S1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbk_cargo_S1.setString(2, NewContainerNo);
                                } else {
                                    pstbk_cargo_S1.setString(2, lcvo.oldcontno[i]);
                                }
                                pstbk_cargo_S1.setString(3, lcvo.seal[i]);
                                logger.info("lcvo.seal bk only : " + lcvo.seal[i]);

                                bkgcargocnt1 += pstbk_cargo_S1.executeUpdate();
                            }

                        }

                        if ("true".equalsIgnoreCase(pluginRRRO)) {

                            pstbl_update_cbGWF.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbl_update_cbGWF.setString(2, NewContainerNo);
                            } else {
                                pstbl_update_cbGWF.setString(2, lcvo.oldcontno[i]);
                            }

                            pstbl_update_cbGWF.setString(3, lcvo.cargoWt[i]);
                            pstbl_update_cbGWF.setString(4, lcvo.cargoWt[i]);
                            pstbl_update_cbGWF.setString(5, lcvo.weightunit[i]);
                            pstbl_update_cbGWF.setString(6, lcvo.cargoWt[i]);

                            blcargoupdate += pstbl_update_cbGWF.executeUpdate();

                            pstbl_com_CWcount.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbl_com_CWcount.setString(2, NewContainerNo);
                            } else {
                                pstbl_com_CWcount.setString(2, lcvo.oldcontno[i]);
                            }
                            blget_comcidcount = pstbl_com_CWcount.executeQuery();
                            if (blget_comcidcount.next()) {
                                blComcidcount = blget_comcidcount.getInt("blcount");
                                if (blComcidcount == 1) {

                                    pstbl_com_CW.setString(1, lcvo.bkgno[i]);
                                    if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                        pstbl_com_CW.setString(2, NewContainerNo);
                                    } else {
                                        pstbl_com_CW.setString(2, lcvo.oldcontno[i]);
                                    }

                                    pstbl_com_CW.setString(3, lcvo.cargoWt[i]);
                                    pstbl_com_CW.setString(4, lcvo.cargoWt[i]);
                                    blcargoupdate += pstbl_com_CW.executeUpdate();
                                }
                            }

                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbl_update_cb1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb1.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb1.setString(2, lcvo.oldcontno[i]);
                                }
                                // logger.info("lcvo.carrierseal : " + lcvo.seal[i]);
                                pstbl_update_cb1.setString(3, lcvo.seal[i]);
                                pstbl_update_cb1.setString(4, lcvo.seal[i]);
                                blcargoupdate1 += pstbl_update_cb1.executeUpdate();
                            }
                            if (lcvo.shipperseal[i] != null && lcvo.shipperseal[i].length() > 0) {
                                pstbl_update_cb2.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb2.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb2.setString(2, lcvo.oldcontno[i]);
                                }
                                //  logger.info("lcvo.shipperseal : " + lcvo.shipperseal[i]);
                                pstbl_update_cb2.setString(3, lcvo.shipperseal[i]);
                                pstbl_update_cb2.setString(4, lcvo.shipperseal[i]);
                                blcargoupdate1 += pstbl_update_cb2.executeUpdate();
                            }
                            if (lcvo.otherseal[i] != null && lcvo.otherseal[i].length() > 0) {
                                pstbl_update_cb3.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb3.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb3.setString(2, lcvo.oldcontno[i]);
                                }
                                // logger.info("lcvo.otherseal : " + lcvo.otherseal[i]);
                                pstbl_update_cb3.setString(3, lcvo.otherseal[i]);
                                pstbl_update_cb3.setString(4, lcvo.otherseal[i]);
                                blcargoupdate1 += pstbl_update_cb3.executeUpdate();
                            }

                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbl_update_cb1confirm1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb1confirm1.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb1confirm1.setString(2, lcvo.oldcontno[i]);
                                }
                                //   logger.info("lcvo.carrierseal : " + lcvo.seal[i]);
                                pstbl_update_cb1confirm1.setString(3, lcvo.seal[i]);
                                pstbl_update_cb1confirm1.setString(4, lcvo.seal[i]);
                                blcargoupdatec += pstbl_update_cb1confirm1.executeUpdate();
                                // this.logger.info("Confirmed BL carrierseal Updated Count                    --> " + blcargoupdatec);
                            }
                            if (lcvo.shipperseal[i] != null && lcvo.shipperseal[i].length() > 0) {
                                pstbl_update_cb1confirm2.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb1confirm2.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb1confirm2.setString(2, lcvo.oldcontno[i]);
                                }
                                // logger.info("lcvo.shipperseal confirmed: " + lcvo.shipperseal[i]);
                                pstbl_update_cb1confirm2.setString(3, lcvo.shipperseal[i]);
                                pstbl_update_cb1confirm2.setString(4, lcvo.shipperseal[i]);
                                blcargoupdatec += pstbl_update_cb1confirm2.executeUpdate();
                                // this.logger.info("Confirmed BL shipperseal Updated Count                    --> " + blcargoupdatec);
                            }
                            if (lcvo.otherseal[i] != null && lcvo.otherseal[i].length() > 0) {
                                pstbl_update_cb1confirm3.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb1confirm3.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb1confirm3.setString(2, lcvo.oldcontno[i]);
                                }
                                //  logger.info("lcvo.otherseal : " + lcvo.otherseal[i]);
                                pstbl_update_cb1confirm3.setString(3, lcvo.otherseal[i]);
                                pstbl_update_cb1confirm3.setString(4, lcvo.otherseal[i]);
                                blcargoupdatec += pstbl_update_cb1confirm3.executeUpdate();
                                //  this.logger.info("Confirmed BL otherseal Updated Count                    --> " + blcargoupdatec);
                            }
                        } else {

                            pstbl_update_cb.setString(1, lcvo.bkgno[i]);
                            if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                pstbl_update_cb.setString(2, NewContainerNo);
                            } else {
                                pstbl_update_cb.setString(2, lcvo.oldcontno[i]);
                            }

                            pstbl_update_cb.setString(3, lcvo.cargoWt[i]);
                            pstbl_update_cb.setString(4, lcvo.cargoWt[i]);
                            pstbl_update_cb.setString(5, lcvo.cargoWt[i]);

                            blcargoupdate += pstbl_update_cb.executeUpdate();
                            if (lcvo.seal[i] != null && lcvo.seal[i].length() > 0) {
                                pstbl_update_cb1.setString(1, lcvo.bkgno[i]);
                                if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                                    pstbl_update_cb1.setString(2, NewContainerNo);
                                } else {
                                    pstbl_update_cb1.setString(2, lcvo.oldcontno[i]);
                                }
                                //   logger.info("lcvo.seal : " + lcvo.seal[i]);
                                pstbl_update_cb1.setString(3, lcvo.seal[i]);
                                pstbl_update_cb1.setString(4, lcvo.seal[i]);
                                blcargoupdate1 += pstbl_update_cb1.executeUpdate();
                            }

                        }

                        psttmpdrop.executeUpdate();
                        psttmpdrop1.executeUpdate();
                        logger.info("bkgcargocnt1 : " + bkgcargocnt1);
                        logger.info("blcargoupdatec : " + blcargoupdatec);
                        logger.info("blcargoupdate1 : " + blcargoupdate1);

                        pstopssp.setString(1, lcvo.stowagePosition[i]);
                        pstopssp.setString(2, lcvo.operationNum[i]);
                        logger.info("Stowage Position : " + lcvo.stowagePosition[i]);
                        logger.info("operationNum : " + lcvo.operationNum[i]);
                        opsstowageupcount += pstopssp.executeUpdate();

                        pstbl_remove_AutoRatedCharges.setString(1, lcvo.bkgno[i]);
                        pstbl_remove_AutoRatedCharges.setString(2, lcvo.oldcontno[i]);
                        remove_overweightcharges += pstbl_remove_AutoRatedCharges.executeUpdate();

                        logger.info("----- ActualContainer End of Update BL -----");
                        con.commit();
                    } catch (Exception e) {
                        stowagealonests = false;
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                        con.rollback();
                        logger.info("----Roll Back Called---");
                    }
                }
                if (lcvo.updationOn.contains("SO")) {
                    logger.info("----- ActualContainer Start of Stowage Only -----");
                    try {
                        con.setAutoCommit(false);

                        pstbk_cargo_SO.setString(1, lcvo.stowagePosition[i]);
                        pstbk_cargo_SO.setString(2, lcvo.operationNum[i]);
                        opsstowageupcount += pstbk_cargo_SO.executeUpdate();
                        con.commit();
                    } catch (Exception e) {
                        stowagealonests = false;
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                        con.rollback();
                        logger.info("----Roll Back Called---");
                    }
                    logger.info("----- ActualContainer End of Stowage Only -----");
                }

                if (lcvo.updationOn.contains("I")) {
                    logger.info("----- ActualContainer Start of ITN Reference -----");
                    try {
                        con.setAutoCommit(false);

                        if (!distBkList.contains(lcvo.bkgno[i].trim())) {
                            int bkupcnt = 0;
                            int blupcnt = 0;
                            int fcnupcnt = 0;
                            if ((lcvo.itn[i] != null) && (!lcvo.itn[i].toString().trim().equalsIgnoreCase(""))) {
                                pstbooking_reference_update.setString(1, lcvo.itn[i]);
                                pstbooking_reference_update.setString(2, lcvo.bkgno[i]);
                                bkupcnt = pstbooking_reference_update.executeUpdate();
                                bkgreferenceitncnt += bkupcnt;
                                logger.info("bkupcnt : " + bkupcnt);
                                if (bkupcnt == 0) {
                                    pstbooking_reference_insert.setString(1, lcvo.bkgno[i]);
                                    pstbooking_reference_insert.setString(2, lcvo.itn[i]);
                                    bkupcnt = pstbooking_reference_insert.executeUpdate();
                                    bkgreferenceitncnt++;
                                }

                                pstbl_reference_update.setString(1, lcvo.bkgno[i]);
                                pstbl_reference_update.setString(2, lcvo.itn[i]);
                                blupcnt = pstbl_reference_update.executeUpdate();
                                blreferenceitncnt += blupcnt;
                                pstbl_droptmp.executeUpdate();
                                logger.info("blupcnt : " + blupcnt);
                                if (blupcnt == 0) {
                                    pstbl_reference_insert.setString(1, lcvo.bkgno[i]);
                                    pstbl_reference_insert.setString(2, lcvo.itn[i]);
                                    blupcnt = pstbl_reference_insert.executeUpdate();
                                    blreferenceitncnt++;
                                }

                                pstfcn_bl_reference_update.setString(1, lcvo.bkgno[i]);
                                pstfcn_bl_reference_update.setString(2, lcvo.itn[i]);
                                fcnupcnt = pstfcn_bl_reference_update.executeUpdate();
                                mcnreferenceitncnt += fcnupcnt;
                                pstfcnbl_droptmp.executeUpdate();
                                logger.info("fcnupcnt : " + fcnupcnt);
                                if (fcnupcnt == 0) {
                                    pstfcn_bl_reference_insert.setString(1, lcvo.bkgno[i]);
                                    pstfcn_bl_reference_insert.setString(2, lcvo.itn[i]);
                                    fcnupcnt = pstfcn_bl_reference_insert.executeUpdate();
                                    mcnreferenceitncnt += fcnupcnt;
                                }
                                distBkList.add(lcvo.bkgno[i].trim());
                            }
                            bkgreferenceitncnt++;
                            con.commit();
                        }
                    } catch (Exception e) {
                        stowagealonests = false;
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                        con.rollback();
                        logger.info("----Roll Back Called---");
                    }
                }

                if (lcvo.updationOn.contains("G")) {
                    logger.info("----- ActualContainer Start of Gate Reference -----");
                    try {
                        con.setAutoCommit(false);

                        int opsGateCnt = 0;
                        logger.info("llcvo.ModeOfTransport[r] : " + lcvo.modeOfTransport[i].toString());
                        String mode = lcvo.modeOfTransport[i];
                        if ("Truck".equalsIgnoreCase(mode)) {
                            mode = "T";
                        } else if ("Rail".equalsIgnoreCase(mode)) {
                            mode = "R";
                        } else if ("Barge".equalsIgnoreCase(mode)) {
                            mode = "B";
                        }

                        String gateClear = lcvo.gateClearence[i];
                        if ("Yes".equalsIgnoreCase(gateClear)) {
                            gateClear = "Y";
                        } else if ("No".equalsIgnoreCase(gateClear)) {
                            gateClear = "N";
                        }
                        pstops_GateInfo.setString(1, gateClear);
                        pstops_GateInfo.setString(2, lcvo.gateDocDate[i]);
                        pstops_GateInfo.setString(3, lcvo.gateInClearenceDate[i]);
                        pstops_GateInfo.setString(4, lcvo.gateReferenceRemarks[i]);
                        pstops_GateInfo.setString(5, mode);
                        pstops_GateInfo.setString(6, lcvo.operationNum[i]);
                        opsGateCnt = pstops_GateInfo.executeUpdate();
                        logger.info("Operations Gate Clearance Update : " + opsGateCnt);

                        pstBkgCargo_GateInfo.setString(1, gateClear);
                        pstBkgCargo_GateInfo.setString(2, lcvo.gateDocDate[i]);
                        pstBkgCargo_GateInfo.setString(3, lcvo.gateInClearenceDate[i]);
                        pstBkgCargo_GateInfo.setString(4, lcvo.gateReferenceRemarks[i]);
                        pstBkgCargo_GateInfo.setString(5, mode);
                        pstBkgCargo_GateInfo.setString(6, lcvo.bkgno[i]);

                        if (lcvo.updationOn.contains("C") && !(NewContainerNo.equalsIgnoreCase(""))) {
                            logger.info("Terminal Container No : " + NewContainerNo);
                            pstBkgCargo_GateInfo.setString(7, NewContainerNo);
                        } else {
                            pstBkgCargo_GateInfo.setString(7, lcvo.oldcontno[i]);
                        }
                        opsGateCnt = pstBkgCargo_GateInfo.executeUpdate();
                        logger.info("Booking Cargo Gate Clearance Update : " + opsGateCnt);
                        opsgatereferencecnt += opsGateCnt;
                        con.commit();
                    } catch (Exception e) {
                        stowagealonests = false;
                        logger.info("Exception in Operation Module");
                        logger.fatal(e);
                        con.rollback();
                        logger.info("----Roll Back Called---");
                    }
                    logger.info("----- ActualContainer Start of Gate Reference -----");
                }

                try {

                    if (alreadyplannedinbklocal == 0 && alreadyplannedinopslocal == 0) {

                        String sNSizeactType = lcvo.actEqpType[i];
                        sNSizeactType = sNSizeactType != null ? sNSizeactType.trim() : "";
                        String sNSizeType = lcvo.type[i];
                        sNSizeType = sNSizeType != null ? sNSizeType.trim() : "";
                        String oldnor = lcvo.nor[i];
                        oldnor = oldnor != null ? oldnor.trim() : "";

                        System.out.println("sNSizeType>>>>>>>>>>>>>>>>" + sNSizeType);
                        System.out.println("sNSizeactType>>>>>>>>>>>>>>>>" + sNSizeactType);
                        System.out.println("oldnor>>>>>>>>>>>>>>>>" + oldnor);

                        if ("true".equalsIgnoreCase(pluginRRRO)) {

                            if ((!NewContainerNo.equalsIgnoreCase(OldContainerNo)) && !(NewContainerNo.equalsIgnoreCase(""))) {
                                int pstbk_cargo_TYPECount = 0;
                                int pstbl_cargo_TYPECount = 0;
                                int psteqptypebkCount = 0;
                                int psteqptypeblCount = 0;

                                StringBuilder sb = new StringBuilder();
                                StringBuilder sb1 = new StringBuilder();
                                if (!sNSizeType.equalsIgnoreCase(sNSizeactType)) {
                                    String Oldcontainertype = "";
                                    String Newcontainertype = "";

                                    String sqlEquipType = "select type from equipments(nolock) where EquipType =?";
                                    PreparedStatement psttype = con.prepareStatement(sqlEquipType);
                                    psttype.setString(1, sNSizeactType);
                                    ResultSet rstype = psttype.executeQuery();
                                    if (rstype.next()) {
                                        Oldcontainertype = rstype.getString("type");
                                        Oldcontainertype = (Oldcontainertype != null) ? Oldcontainertype : "";
                                        logger.info("Oldcontainertype>>>>>>>>>>>>>>>" + Oldcontainertype);
                                    }
                                    PreparedStatement pstnewtype = con.prepareStatement(sqlEquipType);
                                    pstnewtype.setString(1, sNSizeType);
                                    ResultSet rsnewtype = pstnewtype.executeQuery();
                                    while (rsnewtype.next()) {
                                        Newcontainertype = rsnewtype.getString("type");
                                        Newcontainertype = (Newcontainertype != null) ? Newcontainertype : "";
                                        logger.info("Newcontainertype>>>>>>>>>>>>>>>" + Newcontainertype);
                                    }

                                    this.sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set rate_eqptypeid=?,rate_nor=? where cid = @cid";
                                    PreparedStatement psteqptypebk = con.prepareStatement(this.sql);
                                    this.sql = "select cid,bl.bl_no into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no  and bl.book_no=?  and bl_cargo.eqpid=?  UPDATE bl_cargo set rate_eqptypeid=?,rate_nor=?  from #tmpopr  where bl_cargo.cid = #tmpopr.cid    UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no Drop table #tmpopr ";
                                    PreparedStatement psteqptypebl = con.prepareStatement(this.sql);
                                    psteqptypebk.setString(1, BookNo);
                                    psteqptypebk.setString(2, NewContainerNo);
                                    psteqptypebk.setString(3, sNSizeactType);
                                    psteqptypebk.setString(4, oldnor);
                                    psteqptypebkCount += psteqptypebk.executeUpdate();
                                    psteqptypebl.setString(1, BookNo);
                                    psteqptypebl.setString(2, NewContainerNo);
                                    psteqptypebl.setString(3, sNSizeactType);
                                    psteqptypebl.setString(4, oldnor);
                                    psteqptypeblCount += psteqptypebl.executeUpdate();
                                    this.logger.info("Rate_equipbk updated  Count                 --> " + psteqptypebkCount);
                                    this.logger.info("Rate_equipbl updated  Count                 --> " + psteqptypeblCount);

                                    if (!(Oldcontainertype.equals(Newcontainertype))) {

                                        if ("R".equalsIgnoreCase(Oldcontainertype)) {
                                            this.sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set nor='N',type='N' where cid = @cid";

                                            PreparedStatement pstbk_cargo_TYPE = con.prepareStatement(this.sql);

                                            this.sql = "select cid into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no  and bl.book_no=?  and bl_cargo.eqpid=?  UPDATE bl_cargo SET nor='N',type='N' from #tmpopr  where bl_cargo.cid = #tmpopr.cid   Drop table #tmpopr ";

                                            PreparedStatement pstbl_cargo_TYPE = con.prepareStatement(this.sql);

                                            pstbk_cargo_TYPE.setString(1, BookNo);
                                            pstbk_cargo_TYPE.setString(2, NewContainerNo);
                                            pstbk_cargo_TYPECount += pstbk_cargo_TYPE.executeUpdate();

                                            pstbl_cargo_TYPE.setString(1, BookNo);
                                            pstbl_cargo_TYPE.setString(2, NewContainerNo);
                                            pstbl_cargo_TYPECount += pstbl_cargo_TYPE.executeUpdate();

                                        } else if ("R".equalsIgnoreCase(Newcontainertype)) {

                                            this.sql = " declare @cid varchar(20)  select @cid = cid from booking_cargo (nolock)  where  book_no=? and eqpid=?  Update booking_cargo set nor='Y',type='R' where cid = @cid";

                                            PreparedStatement pstbk_cargo_TYPE = con.prepareStatement(this.sql);

                                            this.sql = "select cid into #tmpopr from bl_cargo (nolock)  inner join bl (nolock) on bl.bl_no=bl_cargo.bl_no  and bl.book_no=?  and bl_cargo.eqpid=?  UPDATE bl_cargo SET nor='Y',type='R' from #tmpopr  where bl_cargo.cid = #tmpopr.cid   Drop table #tmpopr ";

                                            PreparedStatement pstbl_cargo_TYPE = con.prepareStatement(this.sql);

                                            pstbk_cargo_TYPE.setString(1, BookNo);
                                            pstbk_cargo_TYPE.setString(2, NewContainerNo);
                                            pstbk_cargo_TYPECount += pstbk_cargo_TYPE.executeUpdate();

                                            pstbl_cargo_TYPE.setString(1, BookNo);
                                            pstbl_cargo_TYPE.setString(2, NewContainerNo);
                                            pstbl_cargo_TYPECount += pstbl_cargo_TYPE.executeUpdate();

                                        }

                                        this.logger.info("BL nor ,type Updated Count                    --> " + pstbl_cargo_TYPECount);
                                        this.logger.info("BK nor ,type Updated Count                    --> " + pstbk_cargo_TYPECount);

                                    }

                                    PreparedStatement pstRmks = con.prepareStatement("select remarks from booking (nolock) where book_no =? ");
                                    pstRmks.setString(1, BookNo);
                                    ResultSet rsRmks = pstRmks.executeQuery();
                                    while (rsRmks.next()) {

                                        sb1.append(rsRmks.getString("remarks"));
                                    }
                                    PreparedStatement pst25 = con.prepareStatement("update booking set remarks=? where book_no =? ");
                                    System.out.println("Username>>>>>>>>>>>" + headers);

                                    String breplceRemarks = headers + "substitute " + sNSizeactType + "(" + OldContainerNo + ")" + " with " + sNSizeType + "(" + NewContainerNo + ")" + " via Actual containers";
                                    System.out.println("Final Remarks : " + breplceRemarks);

                                    if ("null".equalsIgnoreCase(sb1.toString())) {

                                        sb1.append(breplceRemarks);
                                    } else {

                                        sb1.append("\n\n").append(breplceRemarks);
                                    }

                                    remarksappend = sb1.toString();

                                    int r7 = 0;

                                    pst25.setString(1, remarksappend);
                                    pst25.setString(2, BookNo);
                                    r7 = pst25.executeUpdate();

                                    System.out.println("in booking remarks " + r7);

                                }

                                this.sql = "INSERT INTO OPSFORCEDDISCHARGE([OldDischCallId],[BLPODTerminal],[BookNo],[ContainerNo],[UserCr],[DateCr],[flag],[EquipmentType],[vinno]) VALUES(?,?,?,?,?,getDate(),'ACN',?,?)";

                                PreparedStatement psttracking = con.prepareStatement(this.sql);
                                psttracking.setString(1, OldContainerNo);
                                psttracking.setString(2, sNSizeactType);
                                psttracking.setString(3, BookNo);
                                psttracking.setString(4, NewContainerNo);
                                psttracking.setString(5, Usercr);
                                psttracking.setString(6, sNSizeType);
                                if (sNSizeType.equalsIgnoreCase(sNSizeactType)) {
                                    psttracking.setString(7, "SAME EQPMENTTYPE");
                                } else {
                                    psttracking.setString(7, "DIFFERENT EQPMENTTYPE");
                                }

                                psttracking.executeUpdate();

                            }

                        }

                        if ("true".equalsIgnoreCase(stuffplugin)) {

                            this.sql = " select bud.id into #tmpopr from booking_uncargodetail  bud(nolock) inner join  stuffbbrorodetail sd (nolock) on sd.bookingno=bud.book_no and sd.eqpid = bud.eqpid and sd.eqptypeid = bud.eqptypeid and sd.vinno = bud.vinno and sd.status<>'V' where sd.stuffbook_no=? and bud.eqpid=? and bud.eqptypeid=?   UPDATE booking_uncargodetail SET eqpid=?,eqptypeid=?, movetype='ACN',supportunit='C' from #tmpopr  where booking_uncargodetail.id = #tmpopr.id";
                            PreparedStatement updatebookinguncargodetail = con.prepareStatement(this.sql);
                            updatebookinguncargodetail.setString(1, BookNo);
                            updatebookinguncargodetail.setString(2, OldContainerNo);
                            updatebookinguncargodetail.setString(3, sNSizeactType);
                            updatebookinguncargodetail.setString(4, NewContainerNo);
                            updatebookinguncargodetail.setString(5, sNSizeType);
                            bookinguncargo += updatebookinguncargodetail.executeUpdate();
                            psttmpdrop.executeUpdate();

                            this.sql = " select bld.id,bl.bl_no into #tmpopr from bl_uncargodetail(nolock) bld inner join bl (nolock)  on bld.bl_no =bl.bl_no  inner join  stuffbbrorodetail sd (nolock) on sd.bookingno=bl.book_no and sd.eqpid = bld.eqpid and sd.vinno = bld.vinno and sd.status<>'V'\n"
                                    + " where sd.stuffbook_no=? and bld.eqpid=? and bld.eqptypeid=?  Update bl_uncargodetail set eqpid=? , eqptypeid=? ,movetype='ACN',supportunit='C' from #tmpopr  where bl_uncargodetail.id = #tmpopr.id  UPDATE bl SET isopsupdate='Y' from #tmpopr  where bl.bl_no = #tmpopr.bl_no";
                            PreparedStatement updatebluncargodetail = con.prepareStatement(this.sql);
                            updatebluncargodetail.setString(1, BookNo);
                            updatebluncargodetail.setString(2, OldContainerNo);
                            updatebluncargodetail.setString(3, sNSizeactType);
                            updatebluncargodetail.setString(4, NewContainerNo);
                            updatebluncargodetail.setString(5, sNSizeType);
                            bluncargo += updatebluncargodetail.executeUpdate();
                            psttmpdrop.executeUpdate();

                            this.sql = " select distinct rd.operationdno as id into #tmpopr from rorooperationdetail  rd (nolock) inner join  stuffbbrorodetail sd (nolock) on sd.bookingno=rd.bookno and sd.eqpid = rd.containerno and sd.eqptypeid = rd.equipmenttype and sd.vinno = rd.vinno and sd.status<>'V' where sd.stuffbook_no=? and rd.containerno=? and rd.equipmenttype=?   UPDATE rorooperationdetail SET containerno=?,equipmenttype=?,supportunit='Container' from #tmpopr  where rorooperationdetail .operationdno = #tmpopr.id";
                            PreparedStatement updaterorodet = con.prepareStatement(this.sql);
                            updaterorodet.setString(1, BookNo);
                            updaterorodet.setString(2, OldContainerNo);
                            updaterorodet.setString(3, sNSizeactType);
                            updaterorodet.setString(4, NewContainerNo);
                            updaterorodet.setString(5, sNSizeType);
                            rorodet += updaterorodet.executeUpdate();
                            psttmpdrop.executeUpdate();

                            this.sql = " select id into #tmpopr from stuffbbrorodetail(nolock)  where stuffbook_no=?  and eqpid=? and eqptypeid=?  Update stuffbbrorodetail set eqpid=? , eqptypeid=?,isdummy='N' from #tmpopr  where stuffbbrorodetail.id = #tmpopr.id ";
                            PreparedStatement updatestuffdetail = con.prepareStatement(this.sql);
                            updatestuffdetail.setString(1, BookNo);
                            updatestuffdetail.setString(2, OldContainerNo);
                            updatestuffdetail.setString(3, sNSizeactType);
                            updatestuffdetail.setString(4, NewContainerNo);
                            updatestuffdetail.setString(5, sNSizeType);
                            stuffbbrorodetail += updatestuffdetail.executeUpdate();
                            psttmpdrop.executeUpdate();

                            logger.info("stuffbbrorodetail eqp update count:" + stuffbbrorodetail + "  ~  if bookinguncargo update count:" + bookinguncargo + "  ~  if bluncargo update count:" + bluncargo);
                            logger.info("  ~  if rorotable update count:" + rorodet);
                            //      logger.info("stuffbbrorodetail diff eqp update count:" + stuffbbrorodetail + "  ~  if bookinguncargo update count:" + bookinguncargo);
                        }
                        if ("Y".equalsIgnoreCase(editrigger)) {
                            if ((!NewContainerNo.equalsIgnoreCase(OldContainerNo)) && !(NewContainerNo.equalsIgnoreCase(""))) {

                                if (!triggeredilist.contains(BookNo)) {
                                    triggeredilist.add(BookNo);
                                }
                            }
                        }
                        if ("true".equalsIgnoreCase(updateforecast)) {

                            updateforecastlist.add(BookNo + "~" + OldContainerNo + "~" + NewContainerNo + "~" + lcvo.type[i]);

                        }

                    }
                    con.commit();

                } catch (SQLException E) {
                    E.printStackTrace();
                }

            }

            if (woexist) {
                logger.info("-------WORK ORDER UPATION ------------------");

                Calendar calValue = new GregorianCalendar();
                calValue.setTime(new Date());
                long tmStamp = calValue.getTimeInMillis();

                logger.info("User Id -" + tmStamp);
                logger.info("WoActEqpMap -" + WoActEqpMap);
                logger.info("WoNewEqpMap -" + WoNewEqpMap);

                con.setAutoCommit(false);

                String woeqpsql = "INSERT INTO OpsWoEqpUpdation (Userid,wono,eqptype,Count,Action,Usercr,Triggereddate) VALUES (?,?,?,?,?,?,getdate())";
                PreparedStatement pstwoeqp = con.prepareStatement(woeqpsql);

                String woacteqpstr = "";
                String[] woacteqp = new String[2];
                String worefno = "";

                int opswoeqpinsertcount = 0;
                int quantity = 0;

                Iterator woactit = WoActEqpMap.keySet().iterator();

                while (woactit.hasNext()) {
                    woacteqpstr = woactit.next().toString();
                    woacteqp = woacteqpstr.split("~");
                    quantity = Integer.parseInt(WoActEqpMap.get(woacteqpstr).toString());
                    pstwoeqp.setString(1, String.valueOf(tmStamp));
                    pstwoeqp.setString(2, woacteqp[0]);
                    pstwoeqp.setString(3, woacteqp[1]);
                    pstwoeqp.setString(4, String.valueOf(quantity));
                    pstwoeqp.setString(5, "U");
                    pstwoeqp.setString(6, lcvo.user);
                    opswoeqpinsertcount += pstwoeqp.executeUpdate();
                }

                logger.info(" -- OpsWOEqp Insert for U count -" + opswoeqpinsertcount);

                opswoeqpinsertcount = 0;
                woactit = WoNewEqpMap.keySet().iterator();
                while (woactit.hasNext()) {
                    woacteqpstr = woactit.next().toString();
                    woacteqp = woacteqpstr.split("~");
                    quantity = Integer.parseInt(WoNewEqpMap.get(woacteqpstr).toString());
                    pstwoeqp.setString(1, String.valueOf(tmStamp));
                    pstwoeqp.setString(2, woacteqp[0]);
                    pstwoeqp.setString(3, woacteqp[1]);
                    pstwoeqp.setString(4, String.valueOf(quantity));
                    pstwoeqp.setString(5, "I");
                    pstwoeqp.setString(6, lcvo.user);
                    opswoeqpinsertcount += pstwoeqp.executeUpdate();
                }
                logger.info(" -- OpsWOEqp Insert for U count -" + opswoeqpinsertcount);
                con.commit();
                logger.info("-----END OF WORK ORDER UPATION ------------------");
            }

            logger.info("Already planned in booking          -->" + alreadyplannedinbk);
            logger.info("Already planned in operatn          -->" + alreadyplannedinops);
            logger.info("Updation in bookin_cargo            -->" + bkgcargocnt);
            logger.info("Updation in ops_ven_cnt             -->" + ops_ven_cnt);
            logger.info("Updation in tdnvendor_cnt           -->" + tdnvendorcnt);
            logger.info("Updation in booking_commodity       -->" + bkgcommoditycnt);
            logger.info("Updation in booking_imo             -->" + bkgimocnt);
            logger.info("Updation in booking_charge          -->" + bkgchargecnt);
            logger.info("Updation in booking_contacts        -->" + bkgcontactscnt);
            logger.info("Updation in bl_cargo                -->" + blgcargocnt);
            logger.info("Updation in bl_commodity            -->" + blgcommoditycnt);
            logger.info("Updation in bl_imo                  -->" + blgimocnt);
            logger.info("Updation in bl_contacts             -->" + blgcontactscnt);
            logger.info("Updation in operationdetail         -->" + opsupdatecnt);
            logger.info("Updation in Ops stowage pos         -->" + opsstowageupcount);
            logger.info("Updation in TdnDetail               -->" + tdnupdatecnt);
            logger.info("Updation in TransRework Cargo       -->" + transreworkcargocnt);
            logger.info("Updation in TransRework Commodity   -->" + transreworkcommoditycnt);
            logger.info("Updation in ITN Booking Reference   -->" + bkgreferenceitncnt);
            logger.info("Updation in Ops Gate ReferenceInfo  -->" + opsgatereferencecnt);
            logger.info("updateforecastlist size             --> " + updateforecastlist.size());
            logger.info("triggeredilist size             --> " + triggeredilist.size());

            if (((bkgcargocnt > 0) || (bkgcommoditycnt > 0) || (tdnvendorcnt > 0) || (ops_ven_cnt > 0) || (bkgimocnt > 0) || (bkgchargecnt > 0) || (bkgcontactscnt > 0) || (opsupdatecnt > 0) || (opsstowageupcount > 0) || (bkgreferenceitncnt > 0) || (opsgatereferencecnt > 0)) && (stowagealonests)) {
                Exceptionmap.put("ReturnString", "Yes");
            } else {
                Exceptionmap.put("ReturnString", "No");
            }

            if (updateforecastlist.size() > 0) {

                getupdateforecastlist(updateforecastlist);
            }
            if (triggeredilist.size() > 0) {

                triggeredievent(triggeredilist);
            }

            object = Exceptionmap;

            logger.info("----->ActualContainer End of Containers ");
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }

        if (Exceptionmap.size() > 0) {
            return new CompressAPI().compress(object);
        }

        return "No Records";
    }

    public Object updatemrns(ArrayList mrnbackupdate, String usercr) {
        Object object = null;
        HashMap Exceptionmap = new HashMap();
        Connection con = null;

        try {
            logger.info("#################updatemrns##################" + mrnbackupdate.size() + "usercr" + usercr);

            con = new ServerObject().getConnection();

            String Sqlinsert = "INSERT INTO ops_mrncapture (blno,bookNo,mrnNo,usercr,datecr) values(?,?,?,?,getdate())";
            PreparedStatement pstinsertmrn = con.prepareStatement(Sqlinsert);

            String Sqlupdate = "Update ops_mrncapture set mrnno=?,userup=?,dateup =getdate() Where blno=? and (status <> 'V' or status is null) ";
            PreparedStatement updatestuffdetail = con.prepareStatement(Sqlupdate);

            String sqlmrn = "Select Count(blno) As Count from ops_mrncapture(NoLock) "
                    + "Where blno=? and (status <> 'V' or status is null)";
            pst = con.prepareStatement(sqlmrn);
            int rscnt;
            int rscnt1 = 0;
            for (int i = 0; i < mrnbackupdate.size(); i++) {
                StringTokenizer st1 = new StringTokenizer(checkNull((String) mrnbackupdate.get(i)), "~");

                while (st1.hasMoreTokens()) {
                    rscnt = 0;
                    String blno = st1.nextToken().trim();
                    String mrnno = st1.nextToken().trim();
                    String bookno = st1.nextToken().trim();
                    pst.setString(1, blno);
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        rscnt = rs.getInt("Count");
                        if (rscnt == 0) {
                            pstinsertmrn.setString(1, blno);
                            pstinsertmrn.setString(2, bookno);
                            pstinsertmrn.setString(3, mrnno);
                            pstinsertmrn.setString(4, usercr);
                            pstinsertmrn.addBatch();

                        } else {

                            // updatestuffdetail.setString(1, bookno);
                            updatestuffdetail.setString(1, mrnno);
                            updatestuffdetail.setString(2, usercr);
                            updatestuffdetail.setString(3, blno);
                            rscnt1 += updatestuffdetail.executeUpdate();

                        }
                    }

                }

            }
            int opsmrncapturedcount[] = pstinsertmrn.executeBatch();

            logger.info("Updation in opsmrncapturedcount  -->" + opsmrncapturedcount.length + "---rscnt1----->" + rscnt1);

            if ((opsmrncapturedcount.length > 0) || (rscnt1 > 0)) {
                Exceptionmap.put("ReturnString", "Yes");
            } else {
                Exceptionmap.put("ReturnString", "No");
            }

            object = Exceptionmap;

            logger.info("----->ActualContainer End of updatemrns ");
        } catch (Exception e) {
            logger.info("Exception in updatemrns Operation Module");
            logger.fatal(e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }

        if (Exceptionmap.size() > 0) {
            return new CompressAPI().compress(object);
        }

        return "No Records";
    }

//    public Object updatemrns(ArrayList mrnbackupdate, String usercr) {
//        Object object = null;
//        HashMap Exceptionmap = new HashMap();
//        Connection con = null;
//
//        try {
//            logger.info("#################updatemrns##################" + mrnbackupdate.size() + "usercr" + usercr);
//
//            con = new ServerObject().getConnection();
//
//            String Sql = "INSERT INTO ops_mrncapture (bookNo,mrnNo,usercr,datecr) values(?,?,?,getdate())";
//            pst = con.prepareStatement(Sql);
//            for (int i = 0; i < mrnbackupdate.size(); i++) {
//                StringTokenizer st1 = new StringTokenizer(checkNull((String) mrnbackupdate.get(i)), "~");
//
//                while (st1.hasMoreTokens()) {
//                    String bookno = st1.nextToken().trim();
//                    String mrnno = st1.nextToken().trim();
//                    pst.setString(1, bookno);
//                    pst.setString(2, mrnno);
//                    pst.setString(3, usercr);
//                    pst.addBatch();
//                }
//
//            }
//            int opsmrncapturedcount[] = pst.executeBatch();
//            logger.info("Updation in opsmrncapturedcount  -->" + opsmrncapturedcount);
//
//            if ((opsmrncapturedcount.length > 0)) {
//                Exceptionmap.put("ReturnString", "Yes");
//            } else {
//                Exceptionmap.put("ReturnString", "No");
//            }
//
//            object = Exceptionmap;
//
//            logger.info("----->ActualContainer End of updatemrns ");
//        } catch (Exception e) {
//            logger.info("Exception in updatemrns Operation Module");
//            logger.fatal(e);
//        } finally {
//            try {
//                if (con != null) {
//                    con.close();
//                }
//            } catch (Exception e) {
//                setErrorInfo(e.getMessage());
//            }
//        }
//
//        if (Exceptionmap.size() > 0) {
//            return new CompressAPI().compress(object);
//        }
//
//        return "No Records";
//    }
    public Object getContainerTypes(AcnActualContainerVo tcvo, String pluginRRRO) {
        Object obj = null;
        Connection con = null;
        String nor = " ";
        ArrayList EquiptypeSttus = new ArrayList();
        try {

            logger.info("---> Start of  getContainerTypes");
            con = new lrp.serverutils.refs.ServerObject().getConnection();

            //  pluginRRRO = getConfiguration();
            HashMap ContainerMap = new HashMap();
            logger.info("tcvo.contno.length------->" + tcvo.contno.length);
            for (int i = 0; i < tcvo.contno.length; i++) {

                if ("true".equalsIgnoreCase(pluginRRRO)) {
                    sql = "select equiptype from equipments where type='r' and status='A'";
                    pst = con.prepareStatement(sql);
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        EquiptypeSttus.add(rs.getString("equiptype"));
                    }
                    if (EquiptypeSttus.contains(tcvo.type[i])) {
                        nor = tcvo.nor[i];

                        nor = nor != null ? nor.trim() : "";
                    } else {
                        nor = "";
                    }
                    if ("Y".equalsIgnoreCase(nor)) {
                        sql = "select distinct c.containerno,e.mappedtype,e.ratesizetype from containerprofile c  (NOLOCK)  inner join eqpsizemapping e  (NOLOCK)  on c.equipmenttype=e.mappedtype  where e.status='A' and ((e.ratenor='Y' and e.mappednor='Y') or ((e.ratenor is null) and (e.mappednor is null)))  and  c.containerno=?";
                    } else if ("N".equalsIgnoreCase(nor)) {
                        sql = "select distinct c.containerno,e.mappedtype,e.ratesizetype from containerprofile c  (NOLOCK)  inner join eqpsizemapping e  (NOLOCK)  on c.equipmenttype=e.mappedtype  where e.status='A' and ((e.ratenor='N' and e.mappednor='N') or ((e.ratenor is null) and (e.mappednor is null)))  and  c.containerno=?";
                    } else {
                        sql = "select distinct c.containerno,e.mappedtype,e.ratesizetype from containerprofile c  (NOLOCK)  inner join eqpsizemapping e  (NOLOCK)  on c.equipmenttype=e.mappedtype  where e.status='A' and  c.containerno=?";
                    }
                } else {

                    sql = "select distinct c.containerno,e.mappedtype,e.ratesizetype from containerprofile c  (NOLOCK)  inner join eqpsizemapping e  (NOLOCK)  on c.equipmenttype=e.mappedtype  where e.status='A' and  c.containerno=?";
                }
                this.pst = con.prepareStatement(sql);

                HashSet MappedTypeSet = new HashSet();
                HashSet RateSizeTypeSet = new HashSet();

                this.pst.setString(1, tcvo.contno[i]);
                this.rs = this.pst.executeQuery();
                while (this.rs.next()) {
                    MappedTypeSet.add(NullCheck(this.rs.getString("mappedtype")));
                    RateSizeTypeSet.add(NullCheck(this.rs.getString("ratesizetype")));
                }

                ContainerMap.put("MappedTypeSet-" + tcvo.contno[i], MappedTypeSet);
                ContainerMap.put("ReateSizeTypeSet-" + tcvo.contno[i], RateSizeTypeSet);
            }

            logger.info("ContainerMap size-->" + ContainerMap.size());
            obj = ContainerMap;

            logger.info("--> End of  getContainerTypes");
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    public Object getContainerWeight(String contno, String plugincwv) {
        Object obj = null;
        Connection con = null;
        ResultSet rsteqp = null;

        try {

            logger.info("---> Start of  getContainerWeight");
            con = new lrp.serverutils.refs.ServerObject().getConnection();

            HashMap ContainerweightMap;
            ContainerweightMap = new HashMap();
            String sqlequipmenttype = " select equipmenttype from containerprofile(nolock) where status='A' and containerno=?";

            PreparedStatement pstequipmenttype = con.prepareStatement(sqlequipmenttype);

            pstequipmenttype.setString(1, contno);
            rsteqp = pstequipmenttype.executeQuery();
            String eqptype = "";
            while (rsteqp.next()) {
                eqptype = NullCheck(rsteqp.getString("equipmenttype"));
            }
            if ("true".equalsIgnoreCase(plugincwv)) {

                sql = "select (payload/1000) as Maxpayload from equipmentpassport   (NOLOCK)    where status='A' and containerno=? AND  payload IS NOT NULL  AND payload>0 ";

                this.pst = con.prepareStatement(sql);

                String maxpayloadweight = "";

                this.pst.setString(1, contno);
                this.rs = this.pst.executeQuery();
                // System.out.println("before while maxpayloadweight1111111111111>>>>>>>>>>>>>>>>>>>");
                if (this.rs.next()) {
                    //  System.out.println("before while maxpayloadweight>>>>>>>>>>>>>>>>>>>");
                    maxpayloadweight = NullCheck(this.rs.getString("Maxpayload"));
                    //  System.out.println("before if maxpayloadweight>>>>>>>>>>>>>>>>>>>" + maxpayloadweight);
                }
                if (maxpayloadweight.isEmpty()) {
                    String sqlems = "select  ((grosskg-tarekg)/1000) as Maxpayloadems  from emsEquipmentMaster  (NOLOCK)   where status='A'   and equiptype =?";

                    PreparedStatement pstems = con.prepareStatement(sqlems);

                    pstems.setString(1, eqptype);
                    ResultSet rst = pstems.executeQuery();

                    while (rst.next()) {
                        maxpayloadweight = NullCheck(rst.getString("Maxpayloadems"));

                    }
                }
                System.out.println("maxpayloadweight>>>>>>>>>>>>>>>>>>>" + maxpayloadweight);
                ContainerweightMap.put("maxpayloadweight-" + contno, maxpayloadweight);
            } else {
                sql = "select  (grosswt/1000) as maxgrossweight  from equipmentpassport   (NOLOCK)    where containerno=? AND  grosswt IS NOT NULL  AND grosswt>0 ";

                this.pst = con.prepareStatement(sql);

                String maxgrossweight = "";

                this.pst.setString(1, contno);
                this.rs = this.pst.executeQuery();
                while (this.rs.next()) {
                    maxgrossweight = NullCheck(this.rs.getString("maxgrossweight"));
                    //      System.out.println("before if maxgrossweight>>>>>>>>>>>>>>>>>>>" + maxgrossweight);

                }
                if (maxgrossweight.isEmpty()) {
                    String sqlems = "select   (ep.grosskg/1000) as maxgrossweightems  from emsEquipmentMaster  ep(NOLOCK)   where status='A'   and equiptype =?";
                    PreparedStatement pstems = con.prepareStatement(sqlems);

                    pstems.setString(1, eqptype);
                    ResultSet rst = pstems.executeQuery();

                    while (rst.next()) {
                        maxgrossweight = NullCheck(rst.getString("maxgrossweightems"));

                    }
                }
                System.out.println("maxgrossweight>>>>>>>>>>>>>>>>>>>" + maxgrossweight);
                ContainerweightMap.put("maxgrossweight-" + contno, maxgrossweight);
            }

            logger.info("ContainerweightMap size-->" + ContainerweightMap.size());
            obj = ContainerweightMap;

            logger.info("--> End of  getContainerTypes");
        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                setErrorInfo(e.getMessage());
            }
        }
        return obj;
    }

    private String NullCheck(String str) {
        return str != null ? str.trim() : "";
    }

    public void ejbCreate() throws EJBException, RemoteException {
    }

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void setSessionContext(SessionContext sessionContext) throws EJBException, RemoteException {
        this.sessionContext = sessionContext;
    }

    public Map getConfigurations(String agencycode) throws RemoteException {
        Map hmap = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rst = null;
        try {
            hmap = new HashMap();
            conn = new ServerObject().getConnection();
            pstmt = conn.prepareStatement("select name,value from ops_template (nolock) where name in ('PLCD','PGCD','ALETALC','AGETDLC')");
            rst = pstmt.executeQuery();
            while (rst.next()) {
                hmap.put(rst.getString("name"), rst.getString("value"));
            }
            pstmt.clearParameters();
            pstmt = conn.prepareStatement("Select attribute,value from bkgagencyconfig(nolock) where attribute in ('GateITN','OPSTDNEDI') and agencycode= ? ");
            pstmt.setString(1, agencycode);
            rst = pstmt.executeQuery();
            while (rst.next()) {
                hmap.put(rst.getString("attribute"), rst.getString("value"));
            }
            pstmt.clearParameters();
            pstmt = conn.prepareStatement("Select attribute,value from bkgagencyconfig(nolock)  where attribute='OPSGATEDVAL' and agencycode= ? ");
            pstmt.setString(1, agencycode);
            rst = pstmt.executeQuery();
            if (rst.next()) {
                hmap.put(rst.getString("attribute"), rst.getString("value"));
            } else {
                pstmt.clearParameters();
                pstmt = conn.prepareStatement("Select attribute,value from bkgConfigur(nolock) where attribute='OPSGATEDVAL' and agencycode= ? ");
                pstmt.setString(1, agencycode);
                rst = pstmt.executeQuery();
                while (rst.next()) {
                    hmap.put(rst.getString("attribute"), rst.getString("value"));
                }
            }
            pstmt.clearParameters();
            String qry = "select enableStatus from pluginconfiguration where moduleid='BKG' and pluginid='VGM'";
            pstmt = conn.prepareStatement(qry);
            rst = pstmt.executeQuery();
            if (rst.next()) {
                String str = rst.getString("enableStatus");
                str = (str != null) ? str.trim() : "";
                if ("True".equalsIgnoreCase(str)) {
                    hmap.put("solasvgm", "Y");
                } else {
                    hmap.put("solasvgm", "N");
                }
            } else {
                hmap.put("solasvgm", "");
            }

            String qrywt = "Select value from bkgagencyconfig(nolock) where  agencycode=?  and Attribute='GWV'";
            pst = conn.prepareStatement(qrywt);
            pst.setString(1, agencycode);
            rs = pst.executeQuery();
            if (rs.next()) {
                logger.info("Value GWV= " + rs.getString("Value"));
                String strwt = rs.getString("Value");
                strwt = (strwt != null) ? strwt.trim() : "";
                hmap.put("GWV", strwt);
            } else {
                hmap.put("GWV", "");
            }

            String qrysoc = "select enableStatus from pluginconfiguration(nolock) where moduleid='LCC' and pluginid='SOC'";
            this.pst = conn.prepareStatement(qrysoc);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                String str = rs.getString("enableStatus");
                str = (str != null) ? str.trim() : "";
                hmap.put("SOC", str);
            } else {
                hmap.put("SOC", "");
            }

            String qrycwv = "select enableStatus from pluginconfiguration(nolock) where moduleid='LCC' and pluginid='CWV'";
            this.pst = conn.prepareStatement(qrycwv);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                String str = rs.getString("enableStatus");
                str = (str != null) ? str.trim() : "";
                hmap.put("CWV", str);
            } else {
                hmap.put("CWV", "");
            }

            String qrylos = "select enableStatus from pluginconfiguration(nolock) where moduleid='LCC' and pluginid='LOS'";
            this.pst = conn.prepareStatement(qrylos);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                String str = rs.getString("enableStatus");
                str = (str != null) ? str.trim() : "";
                hmap.put("LOS", str);
            } else {
                hmap.put("LOS", "");
            }

            String qrytriggeredi = "Select value from bkgagencyconfig(nolock) where  agencycode=?  and Attribute='EDITRI'";
            this.pst = conn.prepareStatement(qrytriggeredi);
            this.pst.setString(1, agencycode);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                this.logger.info("Value Triggeredi= " + this.rs.getString("Value"));
                String triggeredi1 = this.rs.getString("Value");
                triggeredi1 = triggeredi1 != null ? triggeredi1.trim() : "";
                hmap.put("EDITRI", triggeredi1);
            } else {
                String qryglobaleditrigger = "Select value from bkgconfig(nolock) where  Attribute='EDITRI'";
                this.pst = conn.prepareStatement(qryglobaleditrigger);
                this.rs = this.pst.executeQuery();
                if (this.rs.next()) {
                    this.logger.info("Value triggeredi global agency config= " + this.rs.getString("Value"));
                    String triggeredi = this.rs.getString("Value");
                    triggeredi = triggeredi != null ? triggeredi.trim() : "";
                    hmap.put("EDITRI", triggeredi);
                } else {
                    hmap.put("EDITRI", "");
                }
            }
            String qryforcast = "select enableStatus from pluginconfiguration(nolock) where moduleid='ACN' and pluginid='UFC'";
            this.pst = conn.prepareStatement(qryforcast);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                String updateforecastplugin = rs.getString("enableStatus");
                updateforecastplugin = (updateforecastplugin != null) ? updateforecastplugin.trim() : "";
                hmap.put("UFC", updateforecastplugin);

            } else {
                hmap.put("UFC", "");
            }

            String qrywcn = "select enablestatus from pluginconfiguration where moduleid='WOC' and pluginid='WCN' and status='A'";
            this.pst = conn.prepareStatement(qrywcn);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                String strwcn = rs.getString("enableStatus");
                strwcn = (strwcn != null) ? strwcn.trim() : "";
                hmap.put("WCN", strwcn);
            } else {
                hmap.put("WCN", "");
            }

            String qrydg = "select enablestatus from pluginconfiguration where moduleid='ACN' and pluginid='ADGV' and status='A'";
            this.pst = conn.prepareStatement(qrydg);
            this.rs = this.pst.executeQuery();
            if (this.rs.next()) {
                String strADGV = rs.getString("enableStatus");
                strADGV = (strADGV != null) ? strADGV.trim() : "";
                hmap.put("ADGV", strADGV);
            } else {
                hmap.put("ADGV", "");
            }

        } catch (Exception e) {
            logger.info("Exception in Operation Module");
            logger.fatal(e);
        } finally {
            try {
                if (rst != null) {
                    rst.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.info("Exception in Operation Module");
                logger.fatal(e);
            }
        }
        return hmap;
    }

    private String checkNull(String data) {
        if (data != null) {
            return data.trim();
        }
        return "";
    }

    public void getupdateforecastlist(List al) {
        try {

            try {
                this.serviceHelper = ((IEDIServiceHelper) this.vdsInvoker.create(IEDIServiceHelper.class));

            } catch (Exception ee) {
                logger.info("Exception in serviceHelper");
                logger.fatal(ee);
            }
            for (int i = 0; i < al.size(); i++) {

                StringTokenizer st1 = new StringTokenizer(checkNull((String) al.get(i)), "~");

                while (st1.hasMoreTokens()) {
                    String bookno = st1.nextToken().trim();
                    String oldcontno = st1.nextToken().trim();
                    String newcontno = st1.nextToken().trim();
                    String eqptype = st1.nextToken().trim();

                    SvmJsonList alist = new SvmJsonList();
                    alist.add("OPS");
                    alist.add(bookno);
                    alist.add(oldcontno);
                    alist.add(newcontno);
                    alist.add(eqptype);
                    Map updateforecast = this.serviceHelper.updatetshfctcntstatus(alist);
                    this.logger.info("updateforecast >> " + updateforecast);
                }

            }

        } catch (Exception e) {
            logger.info("Exception in updateforecast");
            logger.fatal(e);
        }
    }

    public void triggeredievent(List al) {
        Connection conn = null;
        try {
            conn = new ServerObject().getConnection();
            String Sql = "insert into edievent(refno, doctype, edimode, dateup, pla, pol, pod, pld, editype, bookagency, docagency, plaagency, issueagency,\n"
                    + "status, isProcessed, operatorcode, remarks,newrule,cruser)\n"
                    + "select b.book_no, 'BKG' as doctype, 'U' as edimode, getdate(), b.originportid as pla, b.loadportid as pol, b.dischargeportid as pod,\n"
                    + "b.finalportid as pld, 'OPS' as editype, b.agencyid as bookagency, b.documentalAgencyId as docagency, b.agencyid as plaagency,\n"
                    + "b.agencyIssueId as issueagency, b.status, 'N' as isProcessed, b.thirdPartyOper as operatorcode, 'Booking' as remarks,b.shipment_type as newrule,? \n"
                    + "from booking b(nolock) where book_no = ?";
            pst = conn.prepareStatement(Sql);
            for (int i = 0; i < al.size(); i++) {
                pst.setString(1, usercredievent);
                pst.setString(2, (String) al.get(i));
                pst.addBatch();
            }
            pst.executeBatch();

        } catch (Exception e) {
            logger.info("Exception in triggeredievent");
            logger.fatal(e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                logger.info("Exception in triggeredievent finally block");
                logger.fatal(e);
            }
        }
    }

    public List getRecords(String sql) {
        int row = 0;
        List myVector = new ArrayList();
        Connection conn = null;
        try {
            conn = new ServerObject().getConnection();
            conn.setTransactionIsolation(1);
            this.pst = conn.prepareStatement(sql);
            this.rs = this.pst.executeQuery();
            ResultSetMetaData rsm = this.rs.getMetaData();
            while (this.rs.next()) {
                for (int col = 0; col < rsm.getColumnCount(); col++) {
                    myVector.add(this.rs.getString(col + 1));
                    this.logger.info(new StringBuilder().append("BEAN Values---->>>").append(this.rs.getString(col + 1)).toString());
                }
                row++;
            }
            this.logger.info(new StringBuilder().append("myVector---->>>").append(myVector).toString());
            this.logger.info(new StringBuilder().append("myVector size---->>>").append(myVector.size()).toString());
        } catch (Exception e) {
            this.logger.info("Exception in operation modules");
            this.logger.fatal(e);
        } finally {
            this.logger.info(new StringBuilder().append("Finally Block : Rows Retrieved : ").append(row).toString());
            if (conn != null) {
                try {
                    conn.close();
                    this.rs.close();
                    this.pst.close();
                } catch (Exception e) {
                    this.logger.info("Exception in operation modules");
                    this.logger.fatal(e);
                }
            }
        }
        return myVector;
    }

    public EJBHome getEJBHome() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getPrimaryKey() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void remove() throws RemoteException, RemoveException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Handle getHandle() throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean isIdentical(EJBObject obj) throws RemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
