/*
 * MatchTableVo.java
 *
 * Created on May 20, 2009, 12:38 PM
 */
package com.svm.lrp.ops.acn.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author magesh
 */
public class AcnMatchTableVo implements Serializable {

    /**
     * Creates a new instance of MatchTableVo
     */
    public AcnMatchTableVo() {
    }

    public String bkNo = "";
    public String containerNo = "";
    public String opspod = "";
    public String carrierseal = "";
    public String shipperseal = "";
    public String otherSeal = "";
    public String cargoWeight = "";
    public String stowagePosition = "";
    public String mrnnumber = "";
    public String blno = "";

    public String bgcolor = "";
    public Boolean sselect = new Boolean(false);

    public List grid2collist;

    public String getBlno() {
        return blno;
    }

    public void setBlno(String blno) {
        this.blno = blno;
    }

    public String getMrnnumber() {
        return mrnnumber;
    }

    public void setMrnnumber(String mrnnumber) {
        this.mrnnumber = mrnnumber;
    }

    public List getGrid2collist() {
        return grid2collist;
    }

    public void setGrid2collist(List grid2collist) {
        this.grid2collist = grid2collist;
    }

    public String getBkNo() {
        return bkNo;
    }

    public void setBkNo(String bkNo) {
        this.bkNo = bkNo;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getOpspod() {
        return opspod;
    }

    public void setOpspod(String opspod) {
        this.opspod = opspod;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public Boolean getSselect() {
        return sselect;
    }

    public void setSselect(Boolean sselect) {
        this.sselect = sselect;
    }

    public String getOtherSeal() {
        return otherSeal;
    }

    public void setOtherSeal(String otherSeal) {
        this.otherSeal = otherSeal;
    }

    public String getCarrierseal() {
        return carrierseal;
    }

    public void setCarrierseal(String carrierseal) {
        this.carrierseal = carrierseal;
    }

    public String getShipperseal() {
        return shipperseal;
    }

    public void setShipperseal(String shipperseal) {
        this.shipperseal = shipperseal;
    }

    public String getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(String cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public String getStowagePosition() {
        return stowagePosition;
    }

    public void setStowagePosition(String stowagePosition) {
        this.stowagePosition = stowagePosition;
    }

}
