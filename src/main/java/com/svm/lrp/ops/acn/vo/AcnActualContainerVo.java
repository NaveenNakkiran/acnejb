/**
 * **************************************************************
 * Class : LoadConfVo.java Description :
 *
 * Database Accessed : << LRPDB >> Tables Accessed : <<  Operationdetail >>
 *
 * Author : ram Date : December 31, 2005, 11:50 AM
 *
 * Modification History : Author Date Reason Comment --------- ---------
 * -------- ---------
 *
 *
 *****************************************************************
 */
package com.svm.lrp.ops.acn.vo;

import java.io.Serializable;
import java.util.Date;

public class AcnActualContainerVo implements Serializable {

    public String service = "";
    public String vessel = "";
    public String voyage = "";
    public String vencode = "";
    public String venName = "";
    public String bound = "";
    public String pod = "";
    public String ter = "";
    public String createdby = "";
    public String createDt = "";
    public String updateby = "";
    public String upDt = "";
    public String cmpCode = "";
    public String user = "";
    public String returnString = "";
    public String updationOn = "";
    public String sTDNRefNo = "";
    public String sMultipleVendor;
    public String sInvStatus;
    public String sTdnCallid;
    public String stdnbkgno;
    public String sNextActivity;
    public String sArrDate;
    public String sOpsMode;
    public String sOpsStatus;

    public String operator[];
    public String mt[];
    public String contno[];
    public String type[];
    public String bkgno[];
    public String bltype[];
    public String blpol[];
    public String blpolter[];
    public String opspol[];
    public String opspolter[];
    public String opspod[];
    public String opspodter[];
    public String blpod[];
    public String blpodter[];
    public String orgin[];
    public String delivery[];
    public String vsl[];
    public String voy[];
    public String svc[];
    public String bnd[];

    public String sBound[];
    public String seal[];
    public String otherseal[];

    public String shipperseal[];

    public String wt[];
    public String commodity[];
    public String cargotype[];
    public String dgglass[];
    public String dgapproval[];
    public String tmp[];
    public String oog[];
    public String[] totalOOG;
    public String nor[];
    public String remarks[];
    public String opttype[];
    public String optno[];
    public String[] operationNum;
    public String activeDate[];
    public String prevOperationno[];
    public String mvcode[];
    public String shipOwn[];
    public String tareWt[];
    public String cargoWt[];
    public String weightunit[];
    public String ccont[];
    public String soc[];
    public String priority[];
    public String optopspod[];
    public String optopspodter[];
    public String oldcontno[];
    public String reftype[];
    public String isocode[];
    public String loadingcondition[];
    public String activityFlag[];
    public String[] stowagePosition;
    public String[] actEqpType;
    public String[] actStatus;
    public String[] rowID;
    public String[] vendor;
    public String[] haulagetype;
    public String[] itn;
    public String[] tdnbkgno;
    public String[] tdnRefNo;
    public String[] aOpsSts;
    public String[] aOpsMode;
    public String[] arrDate;

    public String[] gateClearence;
    public String[] gateDocDate;
    public String[] gateInClearenceDate;
    public String[] gateReferenceRemarks;
    public String[] modeOfTransport;
    public String[] amanifestWt;

    public String soperator = "";
    public String smt = "";
    public String sacontno = "";
    public String stcontno = "";
    public String stype = "";
    public String sbkgno = "";
    public String sbltype = "";
    public String sblpol = "";
    public String sblpolter = "";
    public String sopspol = "";
    public String sopspolter = "";
    public String sopspod = "";
    public String sopspodter = "";
    public String sblpod = "";
    public String sblpodter = "";
    public String sorgin = "";
    public String sdelivery = "";
    public String svsl = "";
    public String svoy = "";
    public String ssvc = "";
    public String sbnd = "";
    public String ssBound = "";
    public String sseal = "";
    public String shipseal = "";
    public String sotherseal = "";
    public String swt = "";
    public String scommodity = "";
    public String scargotype = "";
    public String sdgglass = "";
    public String sdgapproval = "";
    public String stmp = "";
    public String soog = "";
    public String stotalOOG = "";
    public String snor = "";
    public String sremarks = "";
    public String sopttype = "";
    public String soptno = "";
    public String sser = "";
    public String soperationNum = "";
    public String sactiveDate = "";
    public String sprevOperationno = "";
    public String smvcode = "";
    public String sshipOwn = "";
    public String stareWt = "";
    public String scargoWt = "";
    public String sweightunit = "";
    public String sccont = "";
    public String ssoc = "";
    public String spriority = "";
    public String soptopspod = "";
    public String soptopspodter = "";
    public String soldcontno = "";
    public String sreftype = "";
    public String sisocode = "";
    public String sloadingcondition = "";
    public String sactivityFlag = "";
    public String sstowagePosition = "";
    public Boolean sselect = new Boolean(false);
    public String sbgColor = "";
    public String sblnumber = "";
    public String sblType = ""; // SBLType
    public String snetWeight = "";
    public String stopspod = "";
    public String sactEqpType = "";
    public String sactStatus = "";
    public String srowID = "";
    public String svendor;
    public String shaulagetype;
    public String sitn = "";
    public String sgateClearence = "";
    public String sgateDocDate = "";
    public String sgateInClearenceDate = "";
    public String sventpercent = "";
    public String shumidity = "";
    public String sdrains = "";
    public String stdnnumber = "";
    public String sbookningETD = "";
    public String sgateReferenceRemarks = "";
    public String smanifestedWeight = "";
    public String sapcode = "";
    public String sapname = "";
    public String smodeOfTransport = "";
    public String sblstatus = "";
    public String soverWtFlag = "";
    public String smaxGrossWt = "";
    public String sisDummy = "";

    public Date sgateDocDateInput;
    public Date sgateInClearenceDateInput;
    public String gridcolor = "";
    public Boolean sworkdisabled = new Boolean(false);

    public String sVGMVeriFlag;
    public String sVGMRefNumber;
    public String sVGMOrderNumber;
    public String sVGMObtDate;
    public String sVGMWeight;
    public String sVGMMeasurement;
    public String sVGMObtainedMet;
    public String sCompanyName;
    public String sNameofAuthorized;
    public String sEDIreference;
    public String sAddress1;
    public String sAddress2;
    public String sZipCode;
    public String sCity;
    public String sCountry;
    public String sPhone;
    public String sFax;
    public String sEmail;
    public String equipmentstype;
    public String booknodialog;
    public String contnodialog;
    public String sOverWtApprovalFlag = "";
    public String svia;
     public String mrnnumber;

    public AcnActualContainerVo() {
    }

    public AcnActualContainerVo(int len) {
        operator = new String[len];
        mt = new String[len];
        contno = new String[len];
        type = new String[len];
        bkgno = new String[len];
        bltype = new String[len];
        blpol = new String[len];
        blpolter = new String[len];
        opspol = new String[len];
        opspolter = new String[len];
        opspod = new String[len];
        opspodter = new String[len];
        blpod = new String[len];
        blpodter = new String[len];
        orgin = new String[len];
        delivery = new String[len];
        vsl = new String[len];
        voy = new String[len];
        svc = new String[len];
        bnd = new String[len];
        sBound = new String[len];
        seal = new String[len];
        shipperseal = new String[len];
        otherseal = new String[len];
        wt = new String[len];
        commodity = new String[len];
        cargotype = new String[len];
        dgglass = new String[len];
        dgapproval = new String[len];
        tmp = new String[len];
        oog = new String[len];
        totalOOG = new String[len];
        nor = new String[len];
        remarks = new String[len];
        opttype = new String[len];
        optno = new String[len];
        // ser=new String[len];
        operationNum = new String[len];
        activeDate = new String[len];
        prevOperationno = new String[len];
        mvcode = new String[len];
        shipOwn = new String[len];
        tareWt = new String[len];
        cargoWt = new String[len];
        weightunit = new String[len];
        ccont = new String[len];
        soc = new String[len];
        priority = new String[len];
        optopspod = new String[len];
        optopspodter = new String[len];
        oldcontno = new String[len];
        reftype = new String[len];
        isocode = new String[len];
        loadingcondition = new String[len];
        activityFlag = new String[len];
        stowagePosition = new String[len];
        actEqpType = new String[len];
        actStatus = new String[len];
        rowID = new String[len];
        vendor = new String[len];
        haulagetype = new String[len];
        itn = new String[len];
        gateClearence = new String[len];
        gateDocDate = new String[len];
        gateInClearenceDate = new String[len];
        gateReferenceRemarks = new String[len];
        modeOfTransport = new String[len];
        amanifestWt = new String[len];

        tdnbkgno = new String[len];
        tdnRefNo = new String[len];
        aOpsSts = new String[len];
        aOpsMode = new String[len];
        arrDate = new String[len];
    }

    public String getMrnnumber() {
        return mrnnumber;
    }

    public void setMrnnumber(String mrnnumber) {
        this.mrnnumber = mrnnumber;
    }
    
    

    public String getSvia() {
        return svia;
    }

    public void setSvia(String svia) {
        this.svia = svia;
    }

    public String getContnodialog() {
        return contnodialog;
    }

    public void setContnodialog(String contnodialog) {
        this.contnodialog = contnodialog;
    }

    public String getsOverWtApprovalFlag() {
        return sOverWtApprovalFlag;
    }

    public void setsOverWtApprovalFlag(String sOverWtApprovalFlag) {
        this.sOverWtApprovalFlag = sOverWtApprovalFlag;
    }

    public String getBooknodialog() {
        return booknodialog;
    }

    public void setBooknodialog(String booknodialog) {
        this.booknodialog = booknodialog;
    }

    public String[] getWeightunit() {
        return weightunit;
    }

    public void setWeightunit(String[] weightunit) {
        this.weightunit = weightunit;
    }

    public String getSweightunit() {
        return sweightunit;
    }

    public void setSweightunit(String sweightunit) {
        this.sweightunit = sweightunit;
    }

    public String getEquipmentstype() {
        return equipmentstype;
    }

    public void setEquipmentstype(String equipmentstype) {
        this.equipmentstype = equipmentstype;
    }

    public String getShipseal() {
        return shipseal;
    }

    public void setShipseal(String shipseal) {
        this.shipseal = shipseal;
    }

    public String getSotherseal() {
        return sotherseal;
    }

    public void setSotherseal(String sotherseal) {
        this.sotherseal = sotherseal;
    }

    public String[] getOtherseal() {
        return otherseal;
    }

    public void setOtherseal(String[] otherseal) {
        this.otherseal = otherseal;
    }

    public String[] getShipperseal() {
        return shipperseal;
    }

    public void setShipperseal(String[] shipperseal) {
        this.shipperseal = shipperseal;
    }

    public Boolean getSworkdisabled() {
        return sworkdisabled;
    }

    public void setSworkdisabled(Boolean sworkdisabled) {
        this.sworkdisabled = sworkdisabled;
    }

    public String getGridcolor() {
        return gridcolor;
    }

    public void setGridcolor(String gridcolor) {
        this.gridcolor = gridcolor;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVencode() {
        return vencode;
    }

    public void setVencode(String vencode) {
        this.vencode = vencode;
    }

    public String getVenName() {
        return venName;
    }

    public void setVenName(String venName) {
        this.venName = venName;
    }

    public String getBound() {
        return bound;
    }

    public void setBound(String bound) {
        this.bound = bound;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getTer() {
        return ter;
    }

    public void setTer(String ter) {
        this.ter = ter;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateby() {
        return updateby;
    }

    public void setUpdateby(String updateby) {
        this.updateby = updateby;
    }

    public String getUpDt() {
        return upDt;
    }

    public void setUpDt(String upDt) {
        this.upDt = upDt;
    }

    public String getCmpCode() {
        return cmpCode;
    }

    public void setCmpCode(String cmpCode) {
        this.cmpCode = cmpCode;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReturnString() {
        return returnString;
    }

    public void setReturnString(String returnString) {
        this.returnString = returnString;
    }

    public String getUpdationOn() {
        return updationOn;
    }

    public void setUpdationOn(String updationOn) {
        this.updationOn = updationOn;
    }

    public String[] getOperator() {
        return operator;
    }

    public void setOperator(String[] operator) {
        this.operator = operator;
    }

    public String[] getMt() {
        return mt;
    }

    public void setMt(String[] mt) {
        this.mt = mt;
    }

    public String[] getContno() {
        return contno;
    }

    public void setContno(String[] contno) {
        this.contno = contno;
    }

    public String[] getType() {
        return type;
    }

    public void setType(String[] type) {
        this.type = type;
    }

    public String[] getBkgno() {
        return bkgno;
    }

    public void setBkgno(String[] bkgno) {
        this.bkgno = bkgno;
    }

    public String[] getBltype() {
        return bltype;
    }

    public void setBltype(String[] bltype) {
        this.bltype = bltype;
    }

    public String[] getBlpol() {
        return blpol;
    }

    public void setBlpol(String[] blpol) {
        this.blpol = blpol;
    }

    public String[] getBlpolter() {
        return blpolter;
    }

    public void setBlpolter(String[] blpolter) {
        this.blpolter = blpolter;
    }

    public String[] getOpspol() {
        return opspol;
    }

    public void setOpspol(String[] opspol) {
        this.opspol = opspol;
    }

    public String[] getOpspolter() {
        return opspolter;
    }

    public void setOpspolter(String[] opspolter) {
        this.opspolter = opspolter;
    }

    public String[] getOpspod() {
        return opspod;
    }

    public void setOpspod(String[] opspod) {
        this.opspod = opspod;
    }

    public String[] getOpspodter() {
        return opspodter;
    }

    public void setOpspodter(String[] opspodter) {
        this.opspodter = opspodter;
    }

    public String[] getBlpod() {
        return blpod;
    }

    public void setBlpod(String[] blpod) {
        this.blpod = blpod;
    }

    public String[] getBlpodter() {
        return blpodter;
    }

    public void setBlpodter(String[] blpodter) {
        this.blpodter = blpodter;
    }

    public String[] getOrgin() {
        return orgin;
    }

    public void setOrgin(String[] orgin) {
        this.orgin = orgin;
    }

    public String[] getDelivery() {
        return delivery;
    }

    public void setDelivery(String[] delivery) {
        this.delivery = delivery;
    }

    public String[] getVsl() {
        return vsl;
    }

    public void setVsl(String[] vsl) {
        this.vsl = vsl;
    }

    public String[] getVoy() {
        return voy;
    }

    public void setVoy(String[] voy) {
        this.voy = voy;
    }

    public String[] getSvc() {
        return svc;
    }

    public void setSvc(String[] svc) {
        this.svc = svc;
    }

    public String[] getBnd() {
        return bnd;
    }

    public void setBnd(String[] bnd) {
        this.bnd = bnd;
    }

    public String[] getsBound() {
        return sBound;
    }

    public void setsBound(String[] sBound) {
        this.sBound = sBound;
    }

    public String[] getSeal() {
        return seal;
    }

    public void setSeal(String[] seal) {
        this.seal = seal;
    }

    public String[] getWt() {
        return wt;
    }

    public void setWt(String[] wt) {
        this.wt = wt;
    }

    public String[] getCommodity() {
        return commodity;
    }

    public void setCommodity(String[] commodity) {
        this.commodity = commodity;
    }

    public String[] getCargotype() {
        return cargotype;
    }

    public void setCargotype(String[] cargotype) {
        this.cargotype = cargotype;
    }

    public String[] getDgglass() {
        return dgglass;
    }

    public void setDgglass(String[] dgglass) {
        this.dgglass = dgglass;
    }

    public String[] getDgapproval() {
        return dgapproval;
    }

    public void setDgapproval(String[] dgapproval) {
        this.dgapproval = dgapproval;
    }

    public String[] getTmp() {
        return tmp;
    }

    public void setTmp(String[] tmp) {
        this.tmp = tmp;
    }

    public String[] getOog() {
        return oog;
    }

    public void setOog(String[] oog) {
        this.oog = oog;
    }

    public String[] getTotalOOG() {
        return totalOOG;
    }

    public void setTotalOOG(String[] totalOOG) {
        this.totalOOG = totalOOG;
    }

    public String[] getNor() {
        return nor;
    }

    public void setNor(String[] nor) {
        this.nor = nor;
    }

    public String[] getRemarks() {
        return remarks;
    }

    public void setRemarks(String[] remarks) {
        this.remarks = remarks;
    }

    public String[] getOpttype() {
        return opttype;
    }

    public void setOpttype(String[] opttype) {
        this.opttype = opttype;
    }

    public String[] getOptno() {
        return optno;
    }

    public void setOptno(String[] optno) {
        this.optno = optno;
    }

    public String[] getOperationNum() {
        return operationNum;
    }

    public void setOperationNum(String[] operationNum) {
        this.operationNum = operationNum;
    }

    public String[] getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(String[] activeDate) {
        this.activeDate = activeDate;
    }

    public String[] getPrevOperationno() {
        return prevOperationno;
    }

    public void setPrevOperationno(String[] prevOperationno) {
        this.prevOperationno = prevOperationno;
    }

    public String[] getMvcode() {
        return mvcode;
    }

    public void setMvcode(String[] mvcode) {
        this.mvcode = mvcode;
    }

    public String[] getShipOwn() {
        return shipOwn;
    }

    public void setShipOwn(String[] shipOwn) {
        this.shipOwn = shipOwn;
    }

    public String[] getTareWt() {
        return tareWt;
    }

    public void setTareWt(String[] tareWt) {
        this.tareWt = tareWt;
    }

    public String[] getCargoWt() {
        return cargoWt;
    }

    public void setCargoWt(String[] cargoWt) {
        this.cargoWt = cargoWt;
    }

    public String[] getCcont() {
        return ccont;
    }

    public void setCcont(String[] ccont) {
        this.ccont = ccont;
    }

    public String[] getSoc() {
        return soc;
    }

    public void setSoc(String[] soc) {
        this.soc = soc;
    }

    public String[] getPriority() {
        return priority;
    }

    public void setPriority(String[] priority) {
        this.priority = priority;
    }

    public String[] getOptopspod() {
        return optopspod;
    }

    public void setOptopspod(String[] optopspod) {
        this.optopspod = optopspod;
    }

    public String[] getOptopspodter() {
        return optopspodter;
    }

    public void setOptopspodter(String[] optopspodter) {
        this.optopspodter = optopspodter;
    }

    public String[] getOldcontno() {
        return oldcontno;
    }

    public void setOldcontno(String[] oldcontno) {
        this.oldcontno = oldcontno;
    }

    public String[] getReftype() {
        return reftype;
    }

    public void setReftype(String[] reftype) {
        this.reftype = reftype;
    }

    public String[] getIsocode() {
        return isocode;
    }

    public void setIsocode(String[] isocode) {
        this.isocode = isocode;
    }

    public String[] getLoadingcondition() {
        return loadingcondition;
    }

    public void setLoadingcondition(String[] loadingcondition) {
        this.loadingcondition = loadingcondition;
    }

    public String[] getActivityFlag() {
        return activityFlag;
    }

    public void setActivityFlag(String[] activityFlag) {
        this.activityFlag = activityFlag;
    }

    public String[] getStowagePosition() {
        return stowagePosition;
    }

    public void setStowagePosition(String[] stowagePosition) {
        this.stowagePosition = stowagePosition;
    }

    public String[] getActEqpType() {
        return actEqpType;
    }

    public void setActEqpType(String[] actEqpType) {
        this.actEqpType = actEqpType;
    }

    public String[] getActStatus() {
        return actStatus;
    }

    public void setActStatus(String[] actStatus) {
        this.actStatus = actStatus;
    }

    public String[] getRowID() {
        return rowID;
    }

    public void setRowID(String[] rowID) {
        this.rowID = rowID;
    }

    public String[] getVendor() {
        return vendor;
    }

    public void setVendor(String[] vendor) {
        this.vendor = vendor;
    }

    public String[] getHaulagetype() {
        return haulagetype;
    }

    public void setHaulagetype(String[] haulagetype) {
        this.haulagetype = haulagetype;
    }

    public String[] getItn() {
        return itn;
    }

    public void setItn(String[] itn) {
        this.itn = itn;
    }

    public String[] getGateClearence() {
        return gateClearence;
    }

    public void setGateClearence(String[] gateClearence) {
        this.gateClearence = gateClearence;
    }

    public String[] getGateDocDate() {
        return gateDocDate;
    }

    public void setGateDocDate(String[] gateDocDate) {
        this.gateDocDate = gateDocDate;
    }

    public String[] getGateInClearenceDate() {
        return gateInClearenceDate;
    }

    public void setGateInClearenceDate(String[] gateInClearenceDate) {
        this.gateInClearenceDate = gateInClearenceDate;
    }

    public String[] getGateReferenceRemarks() {
        return gateReferenceRemarks;
    }

    public void setGateReferenceRemarks(String[] gateReferenceRemarks) {
        this.gateReferenceRemarks = gateReferenceRemarks;
    }

    public String[] getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String[] modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public String[] getAmanifestWt() {
        return amanifestWt;
    }

    public void setAmanifestWt(String[] amanifestWt) {
        this.amanifestWt = amanifestWt;
    }

    public String getSoperator() {
        return soperator;
    }

    public void setSoperator(String soperator) {
        this.soperator = soperator;
    }

    public String getSmt() {
        return smt;
    }

    public void setSmt(String smt) {
        this.smt = smt;
    }

    public String getSacontno() {
        return sacontno;
    }

    public void setSacontno(String sacontno) {
        this.sacontno = sacontno;
    }

    public String getStcontno() {
        return stcontno;
    }

    public void setStcontno(String stcontno) {
        this.stcontno = stcontno;
    }

    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }

    public String getSbkgno() {
        return sbkgno;
    }

    public void setSbkgno(String sbkgno) {
        this.sbkgno = sbkgno;
    }

    public String getSbltype() {
        return sbltype;
    }

    public void setSbltype(String sbltype) {
        this.sbltype = sbltype;
    }

    public String getSblpol() {
        return sblpol;
    }

    public void setSblpol(String sblpol) {
        this.sblpol = sblpol;
    }

    public String getSblpolter() {
        return sblpolter;
    }

    public void setSblpolter(String sblpolter) {
        this.sblpolter = sblpolter;
    }

    public String getSopspol() {
        return sopspol;
    }

    public void setSopspol(String sopspol) {
        this.sopspol = sopspol;
    }

    public String getSopspolter() {
        return sopspolter;
    }

    public void setSopspolter(String sopspolter) {
        this.sopspolter = sopspolter;
    }

    public String getSopspod() {
        return sopspod;
    }

    public void setSopspod(String sopspod) {
        this.sopspod = sopspod;
    }

    public String getSopspodter() {
        return sopspodter;
    }

    public void setSopspodter(String sopspodter) {
        this.sopspodter = sopspodter;
    }

    public String getSblpod() {
        return sblpod;
    }

    public void setSblpod(String sblpod) {
        this.sblpod = sblpod;
    }

    public String getSblpodter() {
        return sblpodter;
    }

    public void setSblpodter(String sblpodter) {
        this.sblpodter = sblpodter;
    }

    public String getSorgin() {
        return sorgin;
    }

    public void setSorgin(String sorgin) {
        this.sorgin = sorgin;
    }

    public String getSdelivery() {
        return sdelivery;
    }

    public void setSdelivery(String sdelivery) {
        this.sdelivery = sdelivery;
    }

    public String getSvsl() {
        return svsl;
    }

    public void setSvsl(String svsl) {
        this.svsl = svsl;
    }

    public String getSvoy() {
        return svoy;
    }

    public void setSvoy(String svoy) {
        this.svoy = svoy;
    }

    public String getSsvc() {
        return ssvc;
    }

    public void setSsvc(String ssvc) {
        this.ssvc = ssvc;
    }

    public String getSbnd() {
        return sbnd;
    }

    public void setSbnd(String sbnd) {
        this.sbnd = sbnd;
    }

    public String getSsBound() {
        return ssBound;
    }

    public void setSsBound(String ssBound) {
        this.ssBound = ssBound;
    }

    public String getSseal() {
        return sseal;
    }

    public void setSseal(String sseal) {
        this.sseal = sseal;
    }

    public String getSwt() {
        return swt;
    }

    public void setSwt(String swt) {
        this.swt = swt;
    }

    public String getScommodity() {
        return scommodity;
    }

    public void setScommodity(String scommodity) {
        this.scommodity = scommodity;
    }

    public String getScargotype() {
        return scargotype;
    }

    public void setScargotype(String scargotype) {
        this.scargotype = scargotype;
    }

    public String getSdgglass() {
        return sdgglass;
    }

    public void setSdgglass(String sdgglass) {
        this.sdgglass = sdgglass;
    }

    public String getSdgapproval() {
        return sdgapproval;
    }

    public void setSdgapproval(String sdgapproval) {
        this.sdgapproval = sdgapproval;
    }

    public String getStmp() {
        return stmp;
    }

    public void setStmp(String stmp) {
        this.stmp = stmp;
    }

    public String getSoog() {
        return soog;
    }

    public void setSoog(String soog) {
        this.soog = soog;
    }

    public String getStotalOOG() {
        return stotalOOG;
    }

    public void setStotalOOG(String stotalOOG) {
        this.stotalOOG = stotalOOG;
    }

    public String getSnor() {
        return snor;
    }

    public void setSnor(String snor) {
        this.snor = snor;
    }

    public String getSremarks() {
        return sremarks;
    }

    public void setSremarks(String sremarks) {
        this.sremarks = sremarks;
    }

    public String getSopttype() {
        return sopttype;
    }

    public void setSopttype(String sopttype) {
        this.sopttype = sopttype;
    }

    public String getSoptno() {
        return soptno;
    }

    public void setSoptno(String soptno) {
        this.soptno = soptno;
    }

    public String getSser() {
        return sser;
    }

    public void setSser(String sser) {
        this.sser = sser;
    }

    public String getSoperationNum() {
        return soperationNum;
    }

    public void setSoperationNum(String soperationNum) {
        this.soperationNum = soperationNum;
    }

    public String getSactiveDate() {
        return sactiveDate;
    }

    public void setSactiveDate(String sactiveDate) {
        this.sactiveDate = sactiveDate;
    }

    public String getSprevOperationno() {
        return sprevOperationno;
    }

    public void setSprevOperationno(String sprevOperationno) {
        this.sprevOperationno = sprevOperationno;
    }

    public String getSmvcode() {
        return smvcode;
    }

    public void setSmvcode(String smvcode) {
        this.smvcode = smvcode;
    }

    public String getSshipOwn() {
        return sshipOwn;
    }

    public void setSshipOwn(String sshipOwn) {
        this.sshipOwn = sshipOwn;
    }

    public String getStareWt() {
        return stareWt;
    }

    public void setStareWt(String stareWt) {
        this.stareWt = stareWt;
    }

    public String getScargoWt() {
        return scargoWt;
    }

    public void setScargoWt(String scargoWt) {
        this.scargoWt = scargoWt;
    }

    public String getSccont() {
        return sccont;
    }

    public void setSccont(String sccont) {
        this.sccont = sccont;
    }

    public String getSsoc() {
        return ssoc;
    }

    public void setSsoc(String ssoc) {
        this.ssoc = ssoc;
    }

    public String getSpriority() {
        return spriority;
    }

    public void setSpriority(String spriority) {
        this.spriority = spriority;
    }

    public String getSoptopspod() {
        return soptopspod;
    }

    public void setSoptopspod(String soptopspod) {
        this.soptopspod = soptopspod;
    }

    public String getSoptopspodter() {
        return soptopspodter;
    }

    public void setSoptopspodter(String soptopspodter) {
        this.soptopspodter = soptopspodter;
    }

    public String getSoldcontno() {
        return soldcontno;
    }

    public void setSoldcontno(String soldcontno) {
        this.soldcontno = soldcontno;
    }

    public String getSreftype() {
        return sreftype;
    }

    public void setSreftype(String sreftype) {
        this.sreftype = sreftype;
    }

    public String getSisocode() {
        return sisocode;
    }

    public void setSisocode(String sisocode) {
        this.sisocode = sisocode;
    }

    public String getSloadingcondition() {
        return sloadingcondition;
    }

    public void setSloadingcondition(String sloadingcondition) {
        this.sloadingcondition = sloadingcondition;
    }

    public String getSactivityFlag() {
        return sactivityFlag;
    }

    public void setSactivityFlag(String sactivityFlag) {
        this.sactivityFlag = sactivityFlag;
    }

    public String getSstowagePosition() {
        return sstowagePosition;
    }

    public void setSstowagePosition(String sstowagePosition) {
        this.sstowagePosition = sstowagePosition;
    }

    public Boolean getSselect() {
        return sselect;
    }

    public void setSselect(Boolean sselect) {
        this.sselect = sselect;
    }

    public String getSbgColor() {
        return sbgColor;
    }

    public void setSbgColor(String sbgColor) {
        this.sbgColor = sbgColor;
    }

    public String getSblnumber() {
        return sblnumber;
    }

    public void setSblnumber(String sblnumber) {
        this.sblnumber = sblnumber;
    }

    public String getSblType() {
        return sblType;
    }

    public void setSblType(String sblType) {
        this.sblType = sblType;
    }

    public String getSnetWeight() {
        return snetWeight;
    }

    public void setSnetWeight(String snetWeight) {
        this.snetWeight = snetWeight;
    }

    public String getStopspod() {
        return stopspod;
    }

    public void setStopspod(String stopspod) {
        this.stopspod = stopspod;
    }

    public String getSactEqpType() {
        return sactEqpType;
    }

    public void setSactEqpType(String sactEqpType) {
        this.sactEqpType = sactEqpType;
    }

    public String getSactStatus() {
        return sactStatus;
    }

    public void setSactStatus(String sactStatus) {
        this.sactStatus = sactStatus;
    }

    public String getSrowID() {
        return srowID;
    }

    public void setSrowID(String srowID) {
        this.srowID = srowID;
    }

    public String getSvendor() {
        return svendor;
    }

    public void setSvendor(String svendor) {
        this.svendor = svendor;
    }

    public String getShaulagetype() {
        return shaulagetype;
    }

    public void setShaulagetype(String shaulagetype) {
        this.shaulagetype = shaulagetype;
    }

    public String getSitn() {
        return sitn;
    }

    public void setSitn(String sitn) {
        this.sitn = sitn;
    }

    public String getSgateClearence() {
        return sgateClearence;
    }

    public void setSgateClearence(String sgateClearence) {
        this.sgateClearence = sgateClearence;
    }

    public String getSgateDocDate() {
        return sgateDocDate;
    }

    public void setSgateDocDate(String sgateDocDate) {
        this.sgateDocDate = sgateDocDate;
    }

    public String getSgateInClearenceDate() {
        return sgateInClearenceDate;
    }

    public void setSgateInClearenceDate(String sgateInClearenceDate) {
        this.sgateInClearenceDate = sgateInClearenceDate;
    }

    public String getSventpercent() {
        return sventpercent;
    }

    public void setSventpercent(String sventpercent) {
        this.sventpercent = sventpercent;
    }

    public String getStdnnumber() {
        return stdnnumber;
    }

    public void setStdnnumber(String stdnnumber) {
        this.stdnnumber = stdnnumber;
    }

    public String getSbookningETD() {
        return sbookningETD;
    }

    public void setSbookningETD(String sbookningETD) {
        this.sbookningETD = sbookningETD;
    }

    public String getSgateReferenceRemarks() {
        return sgateReferenceRemarks;
    }

    public void setSgateReferenceRemarks(String sgateReferenceRemarks) {
        this.sgateReferenceRemarks = sgateReferenceRemarks;
    }

    public String getSmanifestedWeight() {
        return smanifestedWeight;
    }

    public void setSmanifestedWeight(String smanifestedWeight) {
        this.smanifestedWeight = smanifestedWeight;
    }

    public String getSapcode() {
        return sapcode;
    }

    public void setSapcode(String sapcode) {
        this.sapcode = sapcode;
    }

    public String getSapname() {
        return sapname;
    }

    public void setSapname(String sapname) {
        this.sapname = sapname;
    }

    public String getSmodeOfTransport() {
        return smodeOfTransport;
    }

    public void setSmodeOfTransport(String smodeOfTransport) {
        this.smodeOfTransport = smodeOfTransport;
    }

    public String getSblstatus() {
        return sblstatus;
    }

    public void setSblstatus(String sblstatus) {
        this.sblstatus = sblstatus;
    }

    public String getSoverWtFlag() {
        return soverWtFlag;
    }

    public void setSoverWtFlag(String soverWtFlag) {
        this.soverWtFlag = soverWtFlag;
    }

    public String getSmaxGrossWt() {
        return smaxGrossWt;
    }

    public void setSmaxGrossWt(String smaxGrossWt) {
        this.smaxGrossWt = smaxGrossWt;
    }

    public String getSisDummy() {
        return sisDummy;
    }

    public void setSisDummy(String sisDummy) {
        this.sisDummy = sisDummy;
    }

    public Date getSgateDocDateInput() {
        return sgateDocDateInput;
    }

    public void setSgateDocDateInput(Date sgateDocDateInput) {
        this.sgateDocDateInput = sgateDocDateInput;
    }

    public Date getSgateInClearenceDateInput() {
        return sgateInClearenceDateInput;
    }

    public void setSgateInClearenceDateInput(Date sgateInClearenceDateInput) {
        this.sgateInClearenceDateInput = sgateInClearenceDateInput;
    }

    public String getsVGMVeriFlag() {
        return sVGMVeriFlag;
    }

    public void setsVGMVeriFlag(String sVGMVeriFlag) {
        this.sVGMVeriFlag = sVGMVeriFlag;
    }

    public String getsVGMRefNumber() {
        return sVGMRefNumber;
    }

    public void setsVGMRefNumber(String sVGMRefNumber) {
        this.sVGMRefNumber = sVGMRefNumber;
    }

    public String getsVGMOrderNumber() {
        return sVGMOrderNumber;
    }

    public void setsVGMOrderNumber(String sVGMOrderNumber) {
        this.sVGMOrderNumber = sVGMOrderNumber;
    }

    public String getsVGMObtDate() {
        return sVGMObtDate;
    }

    public void setsVGMObtDate(String sVGMObtDate) {
        this.sVGMObtDate = sVGMObtDate;
    }

    public String getsVGMWeight() {
        return sVGMWeight;
    }

    public void setsVGMWeight(String sVGMWeight) {
        this.sVGMWeight = sVGMWeight;
    }

    public String getsVGMMeasurement() {
        return sVGMMeasurement;
    }

    public void setsVGMMeasurement(String sVGMMeasurement) {
        this.sVGMMeasurement = sVGMMeasurement;
    }

    public String getsVGMObtainedMet() {
        return sVGMObtainedMet;
    }

    public void setsVGMObtainedMet(String sVGMObtainedMet) {
        this.sVGMObtainedMet = sVGMObtainedMet;
    }

    public String getsCompanyName() {
        return sCompanyName;
    }

    public void setsCompanyName(String sCompanyName) {
        this.sCompanyName = sCompanyName;
    }

    public String getsNameofAuthorized() {
        return sNameofAuthorized;
    }

    public void setsNameofAuthorized(String sNameofAuthorized) {
        this.sNameofAuthorized = sNameofAuthorized;
    }

    public String getsEDIreference() {
        return sEDIreference;
    }

    public void setsEDIreference(String sEDIreference) {
        this.sEDIreference = sEDIreference;
    }

    public String getsAddress1() {
        return sAddress1;
    }

    public void setsAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    public String getsAddress2() {
        return sAddress2;
    }

    public void setsAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    public String getsZipCode() {
        return sZipCode;
    }

    public void setsZipCode(String sZipCode) {
        this.sZipCode = sZipCode;
    }

    public String getsCity() {
        return sCity;
    }

    public void setsCity(String sCity) {
        this.sCity = sCity;
    }

    public String getsCountry() {
        return sCountry;
    }

    public void setsCountry(String sCountry) {
        this.sCountry = sCountry;
    }

    public String getsPhone() {
        return sPhone;
    }

    public void setsPhone(String sPhone) {
        this.sPhone = sPhone;
    }

    public String getsFax() {
        return sFax;
    }

    public void setsFax(String sFax) {
        this.sFax = sFax;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getShumidity() {
        return shumidity;
    }

    public void setShumidity(String shumidity) {
        this.shumidity = shumidity;
    }

    public String getSdrains() {
        return sdrains;
    }

    public void setSdrains(String sdrains) {
        this.sdrains = sdrains;
    }

}
